<?php

namespace App\Http\Controllers\Admin;

use App\Model\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class ArticleController extends Controller
{
    /**
     * Display a listing of the article posts created.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles=Article::get();
        return view('admin.article.index')
            ->with('articles',$articles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('admin.article.create');
    }

    /**
     * Store a newly created article in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $article=Article::create(Input::all());
        if(Input::hasFile('image')){
            $image=  Input::file('image');
            $imageName=  \Illuminate\Support\Str::random('32').'.'.$image->getClientOriginalExtension();
            //now save the image file
            $image->move('images/article',$imageName);
            $article->image=$imageName;
        }
        $article->save();
        Session()->flash('flash',"Added Article Successfully");
        return redirect('admin/article');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified article content.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $article=Article::where('id',$id)->first();
        return view('admin.article.edit')
            ->with('article',$article);

    }

    /**
     * Update the specified article in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $article=Article::find($id);
        $article->update(Input::all());
        if(Input::hasFile('image')){
            $image=  Input::file('image');
            $imageName=  \Illuminate\Support\Str::random('32').'.'.$image->getClientOriginalExtension();
            //now save the image file
            $image->move('images/article',$imageName);
            $article->image=$imageName;
        }
        $article->save();
        Session()->flash('flash',"Updated Article Successfully");
        return redirect('admin/article');

    }

    /**
     * Remove the specified article from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article=Article::find($id);
        $article->delete();
        Session()->flash('flash',"Deleted Article Successfully");
        return redirect('admin/article');
    }
}
