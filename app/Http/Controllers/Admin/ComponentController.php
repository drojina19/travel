<?php

namespace App\Http\Controllers\Admin;

use App\Model\Component;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class ComponentController extends Controller
{
    public function show($title)
    {
        $t=str_replace('-',' ',$title);
        $component = \App\Model\Component::where('title', $t)->first();
        return view('admin.component.index')->with('component',$component);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $component = \App\Model\Component::find($id);
        return view('admin.component.edit')->with('component',$component);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $component = \App\Model\Component::find($id);
        $component->description = Input::get('description');
        if (Input::hasFile('image')) {
            $imagefile = public_path().'/images/components/'.$component->image;
            $image = Input::file('image');
            $imageName=  \Illuminate\Support\Str::random('32').'.'.$image->getClientOriginalExtension();
            //now save the image file
            $image->move('images/components',$imageName);
            $component->image = $imageName;
        }
        $component->status = Input::get('status');
        $component->save();
        $component = \App\Model\Component::find($id);
        return redirect('admin/component/'.$component->title)->with('component',$component);
    }
}
