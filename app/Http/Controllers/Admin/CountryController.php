<?php

namespace App\Http\Controllers\Admin;

use App\Model\Country;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class CountryController extends Controller
{
    public function index()
    {
        $countries=Country::get();
        return view('admin.country.index')
            ->with('countries',$countries);
    }
    public function create()
    {
        return view('admin.country.create');
    }
    public function store()
    {

        $country=Country::create(Input::all());
        if(Input::hasFile('image')){
            $image=  Input::file('image');
            $imageName=  \Illuminate\Support\Str::random('32').'.'.$image->getClientOriginalExtension();
            //now save the image file
            $image->move('images/country',$imageName);
            $country->image=$imageName;
        }

        Session::flash('flash',"Added Country Successfully");
        return redirect('admin/country');
    }
    public function show($id)
    {
        $country=Country::where('status',1)->where('id',$id)->first();
        return view('admin.country.detail')
            ->with('country',$country);

    }
    public  function edit($id)
    {
        $country=Country::where('id',$id)->first();
        return view('admin.country.edit')
            ->with('country',$country);

    }
    public function update($id)
    {

        $country=Country::find($id);
        $country->update(Input::all());
        if(Input::hasFile('image')){
            $image=  Input::file('image');
            $imageName=  \Illuminate\Support\Str::random('32').'.'.$image->getClientOriginalExtension();
            //now save the image file
            $image->move('images/country',$imageName);
            $country->image=$imageName;
        }
        $country->save();
        Session()->flash('flash',"Updated Country Successfully");
        return redirect('admin/country');
    }
    public  function destroy($id)
    {
        $country=Country::find($id);
        $country->delete();
        Session()->flash('flash',"Deleted Country Successfully");
        return redirect('admin/country');

    }

}
