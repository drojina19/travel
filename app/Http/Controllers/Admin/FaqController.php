<?php

namespace App\Http\Controllers\Admin;

use App\Model\Faq;
use App\Model\Tour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faqs=Faq::get();
        return view('admin.faq.index')
            ->with('faqs',$faqs);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tours=Tour::where('status',1)->get();
        return view('admin.faq.create')
            ->with('tours',$tours);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $faq=Faq::create(Input::all());
        $faq->save();
        Session::flash('flash',"Added Faq Successfully");
        return redirect('admin/faqs/');

    }
    
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faq=Faq::where('id',$id)->first();
        $tours=Tour::where('status',1)->get();
        return view('admin.faq.edit')
            ->with('faq',$faq)
            ->with('tours',$tours);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $faqs=Faq::find($id);
        $faqs->update(Input::all());
        $faqs->save();
        Session()->flash('flash',"Updated Faq Successfully");
        return redirect('admin/faqs');

    }

    /**
     * Remove the specified resource from storage.
     * 
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faq=Faq::find($id);
//        $tour=Tour::where('id',$faq->tour_id)->first();
        $faq->delete();
        Session()->flash('flash',"Deleted Faq Successfully");
        return redirect('admin/faqs');
//        .Str::slug($tour->name));

    }

}
