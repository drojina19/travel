<?php

namespace App\Http\Controllers\Admin;

use App\Model\Gallery;
use App\Model\Tour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries=Gallery::get();
        return view('admin.gallery.index')
            ->with('galleries',$galleries);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tours=Tour::where('status',1)->get();
        return view('admin.gallery.create')
            ->with('tours',$tours);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gallery=Gallery::create(Input::all());
        if(Input::hasFile('image')){
            $image=  Input::file('image');
            $imageName=  \Illuminate\Support\Str::random('32').'.'.$image->getClientOriginalExtension();
            //now save the image file
            $image->move('images/gallery',$imageName);
            $gallery->image=$imageName;
        }
        $gallery->save();
//        dd($gallery);
        Session()->flash('flash', ' Added Gallery Successfully');
        return redirect('admin/gallery');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tours=Tour::where('status',1)->get();
        $gallery=Gallery::where('id',$id)->first();
        return view('admin.gallery.edit')
            ->with('gallery',$gallery)
            ->with('tours',$tours);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gallery=Gallery::find($id);
        if(Input::hasFile('image')){
            $image=  Input::file('image');
            $imageName=  \Illuminate\Support\Str::random('32').'.'.$image->getClientOriginalExtension();
            //now save the image file
            $image->move('images/gallery',$imageName);
            $gallery->image=$imageName;
        }
        $gallery->save();
        Session()->flash('flash','Updated Gallery Successfully');
        return redirect('admin/gallery');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery=Gallery::find($id);
        $gallery->delete();
        Session()->flash('flash',"Deleted Gallery Successfully ");
        return redirect('/admin/gallery');

    }
}
