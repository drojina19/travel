<?php

namespace App\Http\Controllers\Admin;

use App\Model\Country;
use App\Model\Landmark;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class LandmarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLandmarks($country_id)
    {
        $landmarks=Landmark::where('country_id', $country_id)->orderBy('created_at','desc')->get();
        $c=Country::where('id',$country_id)->first();
        return view('admin.landmark.index')
            ->with('landmarks',$landmarks)
            ->with('c',$c);
    }


    public function addLandmark($countryId)
    {
        $c=Country::where('id',$countryId)->first();
        return view('admin.landmark.create')
            ->with('c',$c);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function create()
//    {
//        $countries=Country::where('status',1)->get();
//        return view('admin.landmark.create', compact('countries'));
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $landmark=Landmark::create(Input::all());

        if(Input::hasFile('image')){
            $image=  Input::file('image');
            $imageName=  \Illuminate\Support\Str::random('32').'.'.$image->getClientOriginalExtension();
            //now save the image file
            $image->move('images/landmark',$imageName);
            $landmark->image=$imageName;
        }
        $landmark->save();
        Session::flash('flash',"landmark added");
        return redirect('admin/c/landmark/'.$landmark->country_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $landmark=Landmark::where('status',1)->get();
        return $landmark;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $landmark=Landmark::where('id',$id)->first();
//        $countries=Country::where('status',1)->get();
        return view('admin.landmark.edit')
            ->with('landmark',$landmark);
//        ->with('countries',$countries) ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $landmark=Landmark::find($id);
        $landmark->update(Input::all());
        if(Input::hasFile('image')){
            $image=  Input::file('image');
            $imageName=  \Illuminate\Support\Str::random('32').'.'.$image->getClientOriginalExtension();
            //now save the image file
            $image->move('images/landmark',$imageName);
            $landmark->image=$imageName;
        }
        $landmark->save();
        Session::flash('flash',"Upadated Landmarks");
        return redirect('admin/c/landmark/'.$landmark->country_id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $landmark=Landmark::find($id);
        $c_id=$landmark->country_id;
        $landmark->delete();
        Session::flash('flash',"Deleted Landmarks");
        return redirect('admin/c/landmark/'.$c_id);
    }
}
