<?php

namespace App\Http\Controllers\Admin;

use App\Model\Testemonial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class TestemonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testemonials=Testemonial::get();
        return view('admin.testemonial.index')
            ->with('testemonials',$testemonials);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.testemonial.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $testemonial=Testemonial::create(Input::all());
        if(Input::hasFile('image')){
            $image=  Input::file('image');
            $imageName=  \Illuminate\Support\Str::random('32').'.'.$image->getClientOriginalExtension();
            //now save the image file
            $image->move('images/testemonial',$imageName);
            $testemonial->image=$imageName;
        }
        $testemonial->save();
        Session()->flash('flash',"Added Testemonial Successfully");
        return redirect('admin/testemonial');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $testemonial=Testemonial::where('id',$id)->first();
        return view('admin.testemonial.edit')
            ->with('testemonial',$testemonial);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $testemonial=Testemonial::find($id);
        $testemonial->update(Input::all());
        if(Input::hasFile('image')){
            $image=  Input::file('image');
            $imageName=  \Illuminate\Support\Str::random('32').'.'.$image->getClientOriginalExtension();
            //now save the image file
            $image->move('images/testemonial',$imageName);
            $testemonial->image=$imageName;
        }
        $testemonial->save();
        Session()->flash('flash',"Updated Testemonial Successfully");
        return redirect('admin/testemonial');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $testemonial=Testemonial::find($id);
        $testemonial->delete();
        Session()->flash('flash',"Deleted Testemonial Successfully");
        return redirect('admin/testemonial');
    }
}
