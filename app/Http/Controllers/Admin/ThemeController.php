<?php

namespace App\Http\Controllers\Admin;

use App\Model\Country;
use App\Model\Theme;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class ThemeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $themes=Theme::get();
        return view('admin.theme.index')
            ->with('themes',$themes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.theme.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $theme=Theme::create(Input::all());
        if (Input::hasFile('image')) {
            $image = Input::file('image');
            $imageName = \Illuminate\Support\Str::random('32') . '.' . $image->getClientOriginalExtension();
            //now save the image file
            $image->move('images/theme', $imageName);
            $theme->image = $imageName;
        }
        $theme->save();
       
        Session()->flash('flash',"Added New Theme Successfully");
        return redirect('admin/theme');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $theme=Theme::where('id',$id)->first();
        return view('admin.theme.edit')
            ->with('theme',$theme);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $theme=Theme::find($id);
        $theme->update(Input::all());
        if (Input::hasFile('image')) {
            $image = Input::file('image');
            $imageName = \Illuminate\Support\Str::random('32') . '.' . $image->getClientOriginalExtension();
            //now save the image file
            $image->move('images/theme', $imageName);
            $theme->image = $imageName;
        }
        $theme->save();
        Session()->flash('flash',"Updated Theme Successfully");
        return redirect('admin/theme');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $theme=Theme::find($id);
        $theme->delete();
        Session()->flash('flash',"Deleted Theme Successfully");
        return redirect('admin/theme');
    }
}
