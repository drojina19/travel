<?php

namespace App\Http\Controllers\Admin;

use App\Model\Country;
use App\Model\Theme;
use App\Model\Tour;
use App\Model\TourStyle;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class TourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tours=Tour::orderBy('id',"DESC")->get();
        $countries=Country::get();
        $themes=Theme::get();
        $styles=TourStyle::get();
        return view('admin.tour.index')
            ->with('countries',$countries)
            ->with('themes',$themes)
            ->with('styles',$styles)
            ->with('tours',$tours);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries=Country::where('status',1)->get();
        $themes=Theme::where('status',1)->get();
        $tourstyles=TourStyle::where('status',1)->get();
        return view('admin.tour.create')
            ->with('themes',$themes)
            ->with('countries',$countries)
            ->with('tourstyles',$tourstyles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tour= Tour::create(Input::all());

        if(Input::hasFile('image')){
            $image=  Input::file('image');
            $imageName=  \Illuminate\Support\Str::random('32').'.'.$image->getClientOriginalExtension();
            //now save the image file
            $image->move('images/tour',$imageName);
            $tour->image=$imageName;
        }

        $tour->save();
        dd($tour);
        Session()->flash('flash', "Added Tour Successfully");
        return redirect('admin/tour');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
           

    public function show($id)
    {
        $tour= \App\Model\Tour::where('id',$id)->with('highlights')->with('schedules')
            ->with('inclusions')->with('informations')->with('additionalinfos')->with('prices')->with('equipments')->get();
        return $tour;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $countries=Country::where('status',1)->get();
        $themes=Theme::where('status',1)->get();
        $tourstyles=TourStyle::where('status',1)->get();
        $tour=Tour::where('id',$id)->first();
        return view('admin.tour.edit')
            ->with('tour',$tour)
            ->with('themes',$themes)
            ->with('countries',$countries)
            ->with('tourstyles',$tourstyles);

//
        return view('admin/tour/edit')->with('tour',$tour);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tour= \App\Model\Tour::find($id);
        $tour->update(Input::all());
        
        if(Input::hasFile('image')){
            $image=  Input::file('image');
            $imageName=  \Illuminate\Support\Str::random('32').'.'.$image->getClientOriginalExtension();
            //now save the image file
            $image->move('images/tour',$imageName);
            $tour->image=$imageName;
        }
        $tour->save();
        Session()->flash('flash', ' Updated Tour Successfully');
        return redirect('admin/tour')->with('tour',$tour);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tour= \App\Model\Tour::find($id);
        $tour->delete();
        $tours= \App\Model\Tour::all();
        Session()->flash('flash', 'Deleted successfully');
        return redirect('admin/tour');
    }
}
