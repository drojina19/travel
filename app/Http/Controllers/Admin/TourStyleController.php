<?php

namespace App\Http\Controllers\Admin;

use App\Model\TourStyle;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class TourStyleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $toursyles=TourStyle::get();
        return view('admin.tourstyle.index')
            ->with('tourstyles',$toursyles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tourstyle.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tourstyle=TourStyle::create(Input::all());
        if(Input::hasFile('image')){
            $image=  Input::file('image');
            $imageName=  \Illuminate\Support\Str::random('32').'.'.$image->getClientOriginalExtension();
            //now save the image file
            $image->move('images/tourstyle',$imageName);
            $tourstyle->image=$imageName;
        }
        $tourstyle->save();

        Session()->flash('flash','Added TourStyle Successfully');
        return redirect('admin/tourstyle');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tourstyle=TourStyle::where('id',$id)->first();
        return view('admin.tourstyle.edit')
            ->with('tourstyle',$tourstyle);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tourstyle=TourStyle::find($id);
        $tourstyle->update(Input::all());
        if(Input::hasFile('image'))
        {
            $image=Input::file('image');
            $imageName=\Illuminate\Support\Str::random('32').'.'.$image->getClientOriginalExtension();
            //now save image
            $image->move('images/tourstyle',$imageName);
            $tourstyle->image=$imageName;

        }
        $tourstyle->save();
        Session()->flash('flash','Updated TourStyle Successfully');
        return redirect('admin/tourstyle');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tourstyle=TourStyle::find($id);
        $tourstyle->delete();
        Session()->flash('flash','Deleted TourStyle Successfully');
        return redirect('admin/tourstyle');
    }
}
