<?php

namespace App\Http\Controllers\Admin;

use App\Model\Tour;
use App\Model\TravelDeal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class TravelDealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $traveldeals=TravelDeal::get();
        return view('admin.traveldeal.index')
            ->with('traveldeals',$traveldeals);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tours=Tour::where('status',1)->get();
        return view('admin.traveldeal.create')
            ->with('tours',$tours);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $traveldeal=TravelDeal::create(Input::all());
        if(Input::hasFile('image'))
        {
            $image=Input::file('image');
            $imageName=\Illuminate\Support\Str::random('32').'.'.$image->getClientOriginalExtension();
            //now save the image file
            $image->move('images/traveldeal',$imageName);
            $traveldeal->image=$imageName;
        }

        $traveldeal->save();
        Session::flash('flash',"Added TravelDeal Successfully");
        return redirect('admin/traveldeal');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $traveldeal=TravelDeal::where('id',$id)->first();
        return view('admin.blog.edit')
            ->with('traveldeal',$traveldeal);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $traveldeal=TravelDeal::find($id);
        $traveldeal->update(Input::all());
        if(Input::hasFile('image'))
        {
            $image=Input::file('image');
            $imageName=\Illuminate\Support\Str::random('32').'.'.$image->getClientOriginalExtension();
            //now  save the image file
            $image->move('images/traveldeal',$imageName);
            $traveldeal->image=$imageName;
        }

        $traveldeal->save();
        Session()->flash('flash',"Updated TravelDeal Successfully");
        return redirect('admin/traveldeal');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $traveldeal=TravelDeal::find($id);
        $traveldeal->delete();
        Session()->flash('flash',"Deleted TravelDeal Successfully ");
        return redirect('admin/traveldeal');
    }
}
