<?php

namespace App\Http\Controllers\Api;

use App\Model\AdditionalInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class AdditionalInfoController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $additionalinfo=AdditionalInfo::create(Input::all());
        return $additionalinfo;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $addinfo=AdditionalInfo::find($id);
        $addinfo->delete();
        return 'deleted';
    }

}
