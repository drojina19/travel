<?php

namespace App\Http\Controllers\Api;

use App\Model\Highlight;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class HighlightController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store ()
    {
        $highlight=Highlight::create(Input::all());
        return 'saved';
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $highlight=Highlight::find($id);
        $highlight->delete();
        return "deleted";
    }
}
