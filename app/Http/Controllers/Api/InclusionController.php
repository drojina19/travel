<?php

namespace App\Http\Controllers\Api;

use App\Model\Inclusion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class InclusionController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inclusion=Inclusion::create(Input::all());
        return $inclusion;
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $inclusion=Inclusion::find($id);
        $inclusion->delete();
        return 'deleted';
    }

}
