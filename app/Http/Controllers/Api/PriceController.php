<?php

namespace App\Http\Controllers\Api;

use App\Model\Price;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class PriceController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $price=Price::create(Input::all());
        return 'saved';
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $price=Price::find($id);
        $price->depart_date=date('Y-m-d', strtotime(Input::get('depart_date')));
        $price->arrivedhome_date=date('Y-m-d', strtotime(Input::get('arrivedhome_date')));
        $price->price=Input::get('price');
        $price->saving=Input::get('saving');
        $price->no_of_person=Input::get('no_of_person');
        $price->save();
        return 'updated';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $price=Price::find($id);
        $price->delete();
        return 'deleted';

    }

}