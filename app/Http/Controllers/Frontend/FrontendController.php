<?php

namespace App\Http\Controllers\Frontend;


use App\Model\Article;
use App\Model\Component;
use App\Model\Country;
use App\Model\Gallery;
use App\Model\Slider;
use App\Model\Testemonial;
use App\Model\Theme;
use App\Model\Tour;
use App\Model\TourStyle;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FrontendController extends Controller
{


use App\Model\Article;
use App\Model\Component;
use App\Model\Faq;
use App\Model\Gallery;
use App\Model\Slider;
use App\Model\Testemonial;
use App\Model\Theme;
use App\Model\Tour;
use App\Model\TourStyle;

use App\Model\Country;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class FrontendController extends Controller
{
    public function index()
    {
        $sliders=Slider::where('status',1)->get();
        $tours=Tour::where('status',1)->get();
       
//        $articles=Article::where('status',1)->get();
//        dd($articles);
        return view('frontend.index')
            ->with('sliders',$sliders)
            ->with('tours',$tours);
//            ->with('articles',$articles);
    }

    public function themelist($theme)
    {
        $theme=Theme::where('status',1)->where('name',str_replace('-','',$theme))->first();
        $tours=Tour::where('status',1)->where('theme_id',$theme->id)->get();


//        dd($tours);

        $trs=Tour::where('status',1)->get();
        $destination=Country::where('status',1)->get();

        return view('frontend.themelist')
            ->with('theme',$theme)
            ->with('destination',$destination)
            ->with('tours',$tours)
            ->with('trs',$trs);
    }

//    public function themeDetails($themename){
//        $themes=Theme::where('status',1)->where('name',str_replace('-','',$themename))->first();
//        return view('frontend.themedetails')
//            ->with('themes',$themes);
//    }

    public function about(){
        $about=Component::where('status',1)->first();
        $articles=Article::where('status',1)->get();
        return view('frontend.about')
            ->with('about',$about)
            ->with('articles',$articles);
    }
    public function faqs(){
        $faqs=Faq::where('status',1)->get();
        $testemonials=Testemonial::where('status',1)->get();
        return view('frontend.faq')
            ->with('faqs',$faqs)
            ->with('testemonials',$testemonials);
    }
    public function services()
    {
        return view('frontend.services');

    }
    public function contact()
    {
        return view('frontend.contact');

    }
    public function topdeals()
    {
        $tours=Tour::where('status',1)->get();
        return view('frontend.topdeals')
             ->with('tours',$tours);

    }
    public function gallery()
    {
        $tours=Tour::where('status',1)->get();
//        dd($tours);
        $galleries=Gallery::where('status',1)->get();
        return view('frontend.gallery')
            ->with('galleries',$galleries)
            ->with('tours',$tours);

    }

    public function article($type)
    {
//   dd($type);
        $articles=Article::where('status',1)->where('type',$type)->get();
        return view('frontend.article')
            ->with('articles',$articles)
            ->with('type',$type);
    }

    public function travelStyle($name)
    {   $tourstyles=TourStyle::where('status',1)->get();
        return view('frontend.travelstyle')
            ->with('tourstyles',$tourstyles)
            ->with('name',$name);
    }
    public function destination($country)
    {
        $country=Country::where('status',1)->where('name',str_replace('-','',$country))->first();
//        $c= Country::whereNotIn('id',$country->id)->get();
        $tour=Tour::where('status',1)->where('country_id',$country->id)->get();
//        dd($tour);
        $testimonials=Testemonial::where('status',1)->get();
//        dd($testimonials);
        return view('frontend.destination')
            -> with('country',$country)
            ->with('tours',$tour)
            ->with('testimonials',$testimonials);
    }
    public function tourdetail($theme)
    {
        $theme=Theme::where('status',1)->where('name',str_replace('-','',$theme))->first();
        $tour=Tour::where('status',1)->where('theme_id',$theme->id)->first();
//        dd($tour);
        return view('frontend.tourdetail')
            ->with('tour',$tour)
            ->with('theme',$theme);
    }
    
    
    public function searchTour()
    {

        $country=Country::where('status',1)->where('name', Input::get('country'))->first();

//        dd($country);
        $c=Input::all();

        $t=Tour::where('status',1)->where('country_id',$country->id)->get();






        return view('frontend.searchtour')
            ->with('t',$t)
            ->with('c',$c);
    }

}
