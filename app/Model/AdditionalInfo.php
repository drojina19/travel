<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdditionalInfo extends Model
{
    protected $fillable=['description', 'tour_id','status'];

    public function tour()
    {
        return $this->belongsTo('App\Model\Tour');
    }
}
