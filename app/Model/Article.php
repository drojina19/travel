<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable= ['title','description','image','posted_by','date','type','status'];
}
