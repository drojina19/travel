<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected  $fillable=['title','description','image','posted_by','date','status'];

    
}
