<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable=['name','description','image','status'];

//    public function themes()
//    {
//        return $this->belongsToMany(Theme::class);
//    }

    public function tours()
    {
        return $this->hasMany(Tour::class);
    }

    public function landmarks()
    {
        return $this->hasMany(Landmark::class);
    }
}
