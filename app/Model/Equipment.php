<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    protected $fillable=['title','tour_id','status'];

    public function tour()
    {
        return $this->belongsTo('App\Model\Tour');
    }

}
