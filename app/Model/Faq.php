<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $fillable=['title','description','status','tour_id'];

    public function tour()
    {
        return $this->belongsTo(Tour::class);
    }
}
