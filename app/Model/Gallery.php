<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable=['tour_id','image','status'];

    public  function tour()
    {
        return $this->belongsTo(Tour::class);
    }
}
