<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Inclusion extends Model
{
    protected $fillable =['tour_id','description','status'];

    public function tour()
    {
        return $this->belongsTo(Tour::class);
    }
}
