<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
    protected $fillable=['title','description','tour_id','status'];

    public function tour()
    {
        return $this->belongsTo(Tour::class);
    }
}
