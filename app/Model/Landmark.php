<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Landmark extends Model
{

    protected $fillable=['name','description','tour_id','country_id','image','sites','status'];

    public function tour()
    {
        return $this->belongsToMany('App\Model\Tour');
    }

    public function country()
    {
        return $this->belongsTo('App\Model\Country');
    }
}
