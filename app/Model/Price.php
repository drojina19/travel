<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $fillable=['tour_id','depart_date','arrivedhome_date','price','saving','no_of_person','status'];

    protected $dates = ["depart_date","arrivedhome_date"];

    public function tour()
    {
        return $this->belongsTo(Tour::class);
    }

    public function getDepartDateAttribute()
    {
        $a=$this->attributes['depart_date'];
        $formatted=Carbon::createFromFormat("Y-m-d",$a);
        return $formatted->format("d M, Y");
    }

    public function getArrivedhomeDateAttribute()
    {
        $b=$this->attributes['arrivedhome_date'];
        $formatted=Carbon::createFromFormat("Y-m-d",$b);
        return $formatted->format("d M, Y");
    }
}
