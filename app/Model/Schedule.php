<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable=['tour_id','title','description','status'];

    public function tour()
    {
        return $this->belongsTo(Tour::class);

    }
}
