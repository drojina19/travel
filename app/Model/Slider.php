<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable=['slider_title1','slider_title2','slider_desc','image','status'];
}
