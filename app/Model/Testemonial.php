<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Testemonial extends Model
{
    protected $fillable=['title','description','image','status'];
}
