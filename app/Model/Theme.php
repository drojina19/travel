<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{


    protected $fillable =['name','description','image','status'];


    public function tours()
    {
        return $this->hasMany(Tour::class);
    }
}
