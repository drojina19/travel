<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    protected $fillable =['name','description','image','placesvisited','duration','price','status', 'type','tour_style_id','theme_id','country_id'];


    public function theme()
    {
        return $this->belongsTo(Theme::class);
    }

    public function galleries()
    {
        return $this->hasMany(Gallery::class);
    }

    public function faqs()
    {
        return $this->hasMany(Faq::class);
    }


    public function tourStyle()
    {
        return $this->belongsTo(TourStyle::class);

    }
    public function country()
    {
        return $this->belongsTo(Country::class);
    }


    public function landmarks()
    {
        return $this->belongsToMany('App\Model\Landmark');
    }
    
    public function highlights()
    {
        return $this->hasMany('App\Model\Highlight');
    }
    public function schedules()
    {
        return $this->hasMany('App\Model\Schedule');
    }
    public function inclusions()
    {
        return $this->hasMany('App\Model\Inclusion');
    }

    public function informations()
    {
        return $this->hasMany('App\Model\Information');
    }

    public function  additionalinfos()
    {
        return $this->hasMany('App\Model\AdditionalInfo');
    }

    public  function prices()
    {
        return $this->hasMany('App\Model\Price');
    }
    public  function  equipments()
    {
        return $this->hasMany('App\Model\Equipment');
    }
}


