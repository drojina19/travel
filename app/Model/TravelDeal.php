<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TravelDeal extends Model
{
    protected $fillable=['tour_id','title','description','discount','image','status'];

    public function tours()
    {
        return $this->hasMany(Tour::class);
    }
}
