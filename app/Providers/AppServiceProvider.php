<?php

namespace App\Providers;

use App\Model\Article;
use App\Model\Component;
use App\Model\Country;
use App\Model\Theme;
use App\Model\Tour;
use App\Model\TourStyle;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

//        $components=Component::where('status',1)->get();
//        view()->share('components',$components);
//
//        $countries=Country::where('status',1)->get();
//        view()->share('countries',$countries);
//
//        $themes=Theme::where('status',1)->get();
//        view()->share('themes',$themes);
//
//        $articles=Article::where('status',1)->get();
//        view()->share('articles',$articles);
//
//        $travelstyles=TourStyle::where('status',1)->get();
//        view()->share('travelstyles',$travelstyles);
//
//        $tours=Tour::where('status',1)->get();
//        view()->share('tours',$tours);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
