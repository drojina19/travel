<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTraveldealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_deals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tour_id');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('discount')->nullable();
            $table->string('image')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return voidss
     */
    public function down()
    {
        Schema::dropIfExists('travel_deals');
    }
}
