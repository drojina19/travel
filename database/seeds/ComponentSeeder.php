<?php

use Illuminate\Database\Seeder;

class ComponentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('components')->insert([


            'title'         => 'about',
            'description'   => 'this is about section',
            'status'        => '1'
       

        ]);
    }
}
