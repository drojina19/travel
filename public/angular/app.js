var app = angular.module("scheduleApp", []);

app.directive('datepicker',function(){
    return{
        restrict:'A',
        require:'ngModel',
        link:function(scope,element,attrs,ngModelCtrl){
            element.datepicker({
                dateFormat:'yy-mm-dd',
                todayHighlight:true
            }).on('changeDate',function(e){
                //console.log(e);
                ngModelCtrl.$setViewValue(e.date);
                scope.$apply();
                $(".datepicker").hide();

            });
        }
    };
});


app.directive('ngConfirmClick', [
    function(){
        return {
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmClick || "Are you sure, want to delete?";
                var clickAction = attr.confirmedClick;
                element.bind('click',function (event) {
                    if ( window.confirm(msg) ) {
                        scope.$eval(clickAction)
                    }
                });
            }
        };
    }])




