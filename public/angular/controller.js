//apps=angular.module('highlightsApp',[]);
app.controller('highlightsController',['$scope','$http',function($scope,$http){
     root=$('#tourapi').val();
    console.log(root);
    $scope.fetchTour=function(id) {
        $scope.tour_id = id;
        console.log($scope.tour_id);
        $http.get(root + '/' + id).then(function (res) {
            $scope.tour=res.data[0].name;
            console.log(res);
            $scope.highlights=res.data[0].highlights;
            console.log($scope.highlights);
        });
    }

    $scope.saveHighlight=function(id){
        // var id=$scope.tour_id;
        console.log(id);
        var url=$('#highlightapi').val();
        console.log($scope.highlight_description);
        $http({
            method:'POST',
            url: url,
            data:{'description':$scope.highlight_description,'tour_id':id}

        }).then(function (res) {
            console.log(res);
            $scope.highlight_description='';
            $scope.fetchTour(id);
            alert('saved');
        });
    }
    $scope.deleteHighlight =function(id) {

        var url=$('#highlightapi').val()+'/'+id;
        console.log(url);
        $http({
            method:'DELETE',
            url:url
        }).then(function(res) {
            $scope.fetchTour($scope.tour_id);

        });

    }


    $scope.closeHighlight= function() {
        $scope.highlights =[];
        $scope.tour_id ='';

    }

}]);
//controller for schedule

app.controller('scheduleController',['$scope','$http',function ($scope,$http){

    $scope.popupdata=function(id){
        $scope.schedule={};
        $scope.highlights=[];

        $('#saveSchedule').show();
        $('#updateSchedule').hide();
        $scope.tour_id=id;
        console.log($scope.tour_id);
        var url=$('#tourapi').val();
        $http({
            method:'GET',
            url: url+'/'+ id
        }).then(function(res) {
            $scope.tour=res.data[0].name;
            $scope.schedules=res.data[0].schedules;

        });
    }
    //edit method




    //for save Schedule data
    $scope.saveSchedule= function() {

        // console.log($scope.schedule);
        var url=$('#scheduleapi').val();
        $http({
            method:'POST',
            url:url,
            data:{'title':$scope.schedule.title,'description':$scope.schedule.description,'tour_id':$scope.tour_id}
        }).then(function (res) {
            $scope.schedules.push(res.data);
            $scope.schedule={};
            console.log($scope.schedules);
            alert('saved');

            // $scope.popupdata($scope.tour_id);
        });
    }


    $scope.deleteSchedule= function (id) {

        var url=$('#scheduleapi').val()+'/'+id;
        $http({

            method:'DELETE',
            url: url
        }).then(function(res) {
            console.log(res);
            // $scope.popupdata($scope.tour_id);
         for(var i= 0;i<$scope.schedules.length;i++)
         {
             if($scope.schedules[i].id==res.data)
             {
                 $scope.schedules.splice(i,1);
                 break;
             }
         }

        });

    }
    $scope.editSchedule= function(id) {
        $('#saveSchedule').hide();
        $('#updateSchedule').show();
        $scope.scheduleId=id;
        // console.log(id);
        // console.log($scope.schedules);

        for(var i=0;i<$scope.schedules.length;i++)
        {
            if($scope.schedules[i].id==id)
            {
                $scope.schedule=$scope.schedules[i];
                // $scope.schedule.description=$scope.schedules[i].description;
                break;

            }
        }


    }
    // update
    $scope.updateSchedule = function(id) {
        console.log(id);
        var url=$('#scheduleapi').val()+'/'+id;
        $http({
            method:'PATCH',
            url:url,
            // data:($scope.schedule)
            data:{'title':$scope.schedule.title,'description':$scope.schedule.description}

        }).then(function(res) {
            console.log(res.data);
            alert('upadated');
            $('#saveSchedule').show();
            $('#updateSchedule').hide();
            $scope.schedule={};
            $scope.scheduleId='';
        });


    }

    $scope.closeSchedule= function() {

        $scope.schedules=[];
        $scope.tour_id=[];

    }
}]);

app.controller('inclusionController',['$scope','$http',function ($scope,$http){
    $scope.fetchTourInc =function(id)
    {
        $scope.tour_id=id;
        $http.get($('#tourapi').val()+'/'+id).then(function(res){
            $scope.tour=res.data[0].name;
            console.log(res);
            $scope.inclusions=res.data[0].inclusions;
        });
    }

    $scope.saveInclusion =function()
    {
        var url =$('#inclusionapi').val();
        $http({

            method:'POST',
            url:url,
            data:{'description':$scope.inclusion.description, 'tour_id':$scope.tour_id}

        }).then(function(res) {
            $scope.inclusion={};
            $scope.inclusions.push(res.data);
            alert('saved');
        });
    }
    $scope.deleteInclusion =function(id)
    {
        console.log(id);
        var url=$('#inclusionapi').val()+'/'+id;

        $http({

            method:'DELETE',
            url:url,


        }).then(function(res){
             console.log(res);
            for(var i=0;i<$scope.inclusions.length;i++)
            {
                if($scope.inclusions[i].id==id)
                {
                    $scope.inclusions.splice(i,1);
                    break;
                }
            }

        });

    }
    $scope.closeInclusion = function() {

        $scope.inclusions=[];
        $scope.tour_id= [];
    }


}]);


app.controller('informationController',['$scope','$http',function ($scope,$http){

    $scope.fetchTourInformation=function(id) {
        $scope.information = {};
        $('#saveInformation').show();
        $('#updateInformation').hide();
        $scope.tour_id = id;
        console.log($scope.tour_id);
        var url = $('#tourapi').val();
        $http({
            method: 'GET',
            url: url + '/' + id
        }).then(function (res) {
            console.log(res);
            $scope.tour = res.data[0].name;
            $scope.informations = res.data[0].informations;
        });
    }
        $scope.saveInformation=function()
        {
            var url=$('#informationapi').val();
            $http({
                method:'POST',
                url:url,
                data:{'title':$scope.information.title,'description':$scope.information.description,'tour_id':$scope.tour_id}

            }).then(function(res){
                console.log(res);
                alert('added');
                $scope.information={};
                $scope.informations.push(res.data);

            });
        }
        $scope.editInformation= function(id)
        {

            $('#saveInformation').hide();
            $('#updateInformation').show();
            console.log(id);

            for(var i=0;i<$scope.informations.length;i++)
            {
                if($scope.informations[i].id==id)
                {
                    // console.log($scope.informations[i]);
                    $scope.information=$scope.informations[i];
                    break;
                }
            }
            console.log($scope.information);
        }

        $scope.updateInformation=function(id)
        {
            console.log(id);
            var url=$('#informationapi').val()+'/'+id;
            $http({

                method:'PATCH',
                url:url,
                data:{'title':$scope.information.title,'description':$scope.information.description}
            }).then(function(res) {
                console.log(res);
                $('#saveInformation').show();
                $('#updateInformation').hide();
                $scope.information=null;
                alert('updated');
            });

        }
        $scope.deleteInformation= function(id) {
            var url=$('#informationapi').val()+'/'+id;
            $http({
                method:'DELETE',
                url:url
            }).then(function(res) {
                console.log(res);
                alert('deleted');
                for(var i=0;i<$scope.informations.length;i++)
                {
                    if($scope.informations[i].id==id)
                    {
                        $scope.informations.splice(i,1);
                        break;
                    }
                }

            });


        }
    $scope.closeInformation= function() {

        $scope.informations=[];
        $scope.tour_id =[];

    }




}]);


app.controller('additionalInfoController',['$scope','$http',function($scope, $http) {

    $scope.fetchInfo= function(id) {
        $scope.tour_id = id;
        $http({

            method:'GET',

            url:$('#tourapi').val() + '/' + id


        }).then(function(res){
            $scope.addinfos = res.data[0].additionalinfos;
            console.log($scope.addinfos);
        });
    }

    $scope.saveAddedInfo= function() {
        console.log($('#additionalapi').val());
        console.log($scope.additionalinfo_description);
        $http({

            method:'POST',
            url: $('#additionalapi').val(),
            data:{'description':$scope.additionalinfo_description, 'tour_id':$scope.tour_id}



        }).then(function(res) {

           $scope.addinfos.push(res.data);
            $scope.additionalinfo_description=null;

            alert('added');

        })

    }
    $scope.deleteaddedinfos = function(id) {
        $http({
            method:'DELETE',
            url:$('#additionalapi').val()+'/'+ id
        }).then(function(res) {

            for(var i=0; i<$scope.addinfos.length; i++)
            {
                if($scope.addinfos[i].id==id)
                {
                    $scope.addinfos.splice(i,1);
                    break;
                }
            }
            alert('deleted');
        })

    }

    $scope.closeAdditionalinfo= function() {

        $scope.addinfos =[];
        $scope.tour_id =[];

    }



}]);
app.controller('priceController' ,['$scope','$http', '$filter', function($scope, $http,$filter) {

    $scope.appeardata= function(id) {
        $("#updatePrice").hide();
        $("#savePrice").show();
        $scope.prices=[];
        $scope.price={};
        $scope.tour_id=id;
        console.log($scope.tour_id);
        $http({

            method: 'GET',
            url:$('#tourapi').val() + '/' + id
        }).then(function(res) {


            $scope.prices=res.data[0].prices;
            console.log($scope.prices);

        })

    }

    $scope.savePrice = function(){
        $scope.price['tour_id']=$scope.tour_id;
        $http({
            method:'POST',
            url:$('#priceapi').val(),
            data:$scope.price


        }).then(function(res){
            $scope.price={};
            alert('saved');
            $scope.appeardata($scope.tour_id);

        })

    }
    $scope.editPrice = function(priceid) {
        $("#updatePrice").show();
        $("#savePrice").hide();
        // console.log($scope.prices);
        // console.log($scope.prices.length);
        for(var i=0;i<$scope.prices.length;i++){

            if($scope.prices[i].id == priceid){
                // console.log($scope.prices);
                $scope.price=$scope.prices[i];
                break;
            }
        }
        // var d_d=new Date($scope.price.depart_date);
        // $scope.price['depart_date']=$filter('date')(d_d,"yyyy-mm-dd");
        // var a_d=new Date($scope.price.arrivedhome_date);
        // $scope.price['arrivedhome_date']=$filter('date')(a_d,"yyyy-mm-dd");
        console.log($scope.price);
    }
    $scope.deletePrice =function(priceid){
        $http({

            method:'DELETE',
            url: $('#priceapi').val()+'/'+priceid


        }).then(function(res) {
            console.log(res);
            alert('deleted');
            $scope.appeardata($scope.tour_id);


        });

    }


    $scope.updatePrice=function(priceid){
        console.log(priceid);
        $http({
            method:'PATCH',
            url:$('#priceapi').val() + '/' + priceid,
            data:$scope.price

        }).then(function(res){
            $scope.appeardata($scope.tour_id);
            $("#updatePrice").hide();
            $("#savePrice").show();
            $scope.price={};
            alert('updated');

        });
    }

    $scope.closePrice= function() {

        $scope.prices=[];
        $scope.tour_id=[];

    }


}]);



app.controller('equipmentController' ,['$scope','$http', function($scope, $http) {

$scope.fetchequipment= function (tourId) {

    $scope.tour_id = tourId;
    var rooturl = $('#tourapi').val();
    $http(
        {
            method: 'GET',
            url: rooturl + '/' + tourId

        }
    ).then(function (res) {
        // console.log(res);
        $scope.equipments = res.data[0].equipments;
        // console.log($scope.equipments);
    });
}

$scope.saveEquipment= function(tourId)
{
    var rooturl=$('#equipmentapi').val();
    console.log(rooturl);
    $http({

        method:'POST',
        url:rooturl,
        data: {'title':$scope.equipment_title, 'tour_id':$scope.tour_id}
    }).then(function(res){
        alert('added successully')
        $scope.equipments.push(res.data);
        $scope.equipment_title='';
    });
}


    $scope.deleteEquipment=function(equipmentId)
    {
        var rooturl=$('#equipmentapi').val();
        console.log(rooturl);
        $http({
            method:'DELETE',
            url:rooturl+'/'+equipmentId,

        }).then(function(res){
            alert("deleted successfully")
            console.log(res);
            for(var i=0; i<$scope.equipments.length;i++)
            {
                if ($scope.equipments[i].id==equipmentId)
                {
                    $scope.equipments.splice(i,1);
                    break;
                }


            }


        });

    }

}]);






    