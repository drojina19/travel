// role crud controller
app.controller('roleController', ['$scope', 'configObject', '$http', 'RoleService', function ($scope, configObject, $http, RoleService) {
    $scope.role = {};
    $scope.initFunction=function(){
        $http.post(configObject.getFullBaseUrl() + 'rolelist').then(function(data){
            $scope.roles=data.data.data;
            console.log($scope.roles);
        },function(data){

        });
    }

    $scope.createRole=function () {
        if($scope.roleForm.$valid){
            console.log($scope.role);
            RoleService.save($scope.role, function (data) {
                console.log(data);
                alert('Role is added successfully');
                $('#myRole').modal('hide');
                console.log('new role detail')
                $scope.roles.push(data.data);
                console.log($scope.roles);
                $scope.role={};
            },function (data) {
                $scope.flash ={'has':true,'message': 'Cannot add new role'};
            });
        }else {
            console.log('The form has some errors');
            console.log($scope.roleForm.$error);

        }
    };


    $scope.editRole = function (roleId) {
        console.log('Editing role' + roleId);
        $scope.flash = {};
        RoleService.get({id: roleId}, function (data) {
            console.log(data);
            if (data.status === true) {
                $scope.role = data.data[0];
            }

        }, function (data) {

        });

    };

    $scope.updateRole = function(roleId){
        console.log($scope.role);
        RoleService.update({id:roleId}, $scope.role, function (data) {
            console.log(data);
            if(data.status=='success') {
                var oldItem = $scope.roles.filter(function (e) {
                    return e.id == roleId;
                });
                alert(data.message);
                $('#myRole').modal('hide');
                var orginalIndex = $scope.roles.indexOf(oldItem[0]);
                console.log(orginalIndex);
                $scope.roles.splice(orginalIndex, 1);
                $scope.roles.splice(orginalIndex, 0, data.data);
                $scope.role = {};
                // alert('Role is updated');
                // $('#myRole').modal('hide');
                // $scope.initFunction();
            }
        },function (data) {
            
        });
    };

    $scope.deleteRole = function (roleId) {
        if (confirm('Sure to Delete?')) {
            RoleService.delete({id: roleId}, function (data) {
                console.log(data);
                alert('deleted');
                $scope.roles = $scope.roles.filter(function (e) {
                    return  e.id != roleId;
                });
            }, function (data) {

            });
        }
    };

    $scope.openCreatingForm=function(){
        $scope.role={};
        $scope.flash={};
        $scope.modalData = {'modalHeader': 'Create Role',
            'create': true,
            'edit': false
        }
    };

    $scope.openEditingForm = function () {
        $scope.flash={};
        $scope.modalData={'modalHeader': 'Edit Role',
                            'create': false,
                            'edit': true
                         };
    };

    $scope.isSelected = function (item) {
        return $scope.role.status === item;
    }



    $scope.initFunction();

}]);


//permission crud contoller
app.controller('permissionController', ['$scope', 'configObject', '$http', 'PermissionService', function ($scope, configObject, $http, PermissionService) {
    $scope.permission = {};
    $scope.initFunction=function(){
        $http.post(configObject.getFullBaseUrl() + 'permissionlist').then(function(data){
            $scope.permissions=data.data.data;
            console.log($scope.permissions);
        },function(data){

        });
    }

    $scope.createPermission=function () {
        if($scope.permissionForm.$valid){
            console.log($scope.permission);
            PermissionService.save($scope.permission, function (data) {
                console.log(data);
                alert(data.message);
                $('#myPermission').modal('hide');
                console.log('New permission detail')
                $scope.permissions.push(data.data);
                console.log($scope.permissions);
                $scope.permission={};
            },function (data) {
                $scope.flash ={'has':true,'message': 'Cannot add new permission'};
            });
        }else {
            console.log('The form has some errors');
            console.log($scope.permissionForm.$error);

        }
    };


    $scope.editPermission = function (permissionId) {
        console.log('Editing role' + permissionId);
        $scope.flash = {};
        PermissionService.get({id: permissionId}, function (data) {
            console.log(data);
            if (data.status === true) {
                $scope.permission = data.data[0];
            }

        }, function (data) {

        });

    };

    $scope.updatePermission = function(permissionId){
        console.log($scope.permission);
        PermissionService.update({id:permissionId}, $scope.permission, function (data) {
            console.log(data);
            if(data.status=='success') {
                var oldItem = $scope.permissions.filter(function (e) {
                    return e.id == permissionId;
                });
                alert(data.message);
                $('#myPermission').modal('hide');
                var orginalIndex = $scope.permissions.indexOf(oldItem[0]);
                console.log(orginalIndex);
                $scope.permissions.splice(orginalIndex, 1);
                $scope.permissions.splice(orginalIndex, 0, data.data);
                $scope.permission = {};
                // alert('Role is updated');
                // $('#myRole').modal('hide');
                // $scope.initFunction();
            }
        },function (data) {

        });
    };

    $scope.deletePermission = function (permissionId) {
        console.log(permissionId);
        if (confirm('Sure to Delete?')) {
            PermissionService.delete({id: permissionId}, function (data) {
                console.log(data);
                alert(data.message);
                $scope.permissions = $scope.permissions.filter(function (e) {
                    return  e.id != permissionId;
                });
            }, function (data) {

            });
        }
    };

    $scope.openCreatingForm=function(){
        $scope.permission={};
        $scope.flash={};
        $scope.modalData = {'modalHeader': 'Create Permission',
            'create': true,
            'edit': false
        }
    };

    $scope.openEditingForm = function () {
        $scope.flash={};
        $scope.modalData={'modalHeader': 'Edit Permission',
            'create': false,
            'edit': true
        };
    };

    $scope.isSelected = function (item) {
        return $scope.permission.status === item;
    }

    $scope.initFunction();

}]);



//permission Assignment to role controller
app.controller('permissionsAssignmentController', ['$scope', 'RoleService', 'PermissionService', '$location', 'configObject', '$http', function ($scope, RoleService, PermissionService, $location, configObject, $http) {
    $scope.loaded = {all: false, avail: false};
    var a = $location.absUrl().lastIndexOf('/');
    var b = $location.absUrl().substr(a + 1);//role id
    console.log(b);

    $scope.$watch('loaded', function (n, o) {
        if (n.all === true && n.avail === true) {
            console.log("Yes everything is fully loaded");
            //extract available permissions id into an array
            $scope.availListId = [];
            for (var i = 0; i < $scope.availPermissions.length; i++) {
//                    if ($scope.availPermissions[i].id === id) {
//                        return true;
//                        ;
//                    }
                $scope.availListId.push($scope.availPermissions[i].id);

            }
            console.log("all  avail ids");
            console.log($scope.availListId);
            //finsished extracting available permissions ids
            //now extract all permissions for the current role
            $scope.allPermissionListId = [];
            for (var i = 0; i < $scope.permissions.length; i++) {
//                    if ($scope.availPermissions[i].id === id) {
//                        return true;
//                        ;
//                    }
                $scope.allPermissionListId.push($scope.permissions[i].id);

            }
            console.log("all ids");
            console.log($scope.allPermissionListId);





            $scope.isChecked = function (id) {
                for (var i = 0; i < $scope.availListId.length; i++) {
                    if ($scope.availListId[i] === id) {
                        return true;
                        ;
                    }
                }
                ;
                return false;
//            return true;
            };
        }
    }, true);

    $scope.toggleClick = function (id) {
        if ($scope.availListId.indexOf(id) === -1) {
            $scope.availListId.push(id);
        } else {
            $scope.availListId.splice($scope.availListId.indexOf(id), 1);
        }
        console.log($scope.availListId);
    };
    $scope.updatePermissionAssignment = function () {
        console.log("updating permission for role");
        $http.post(configObject.getFullBaseUrl() + 'updatepermission', {ids: $scope.availListId, roleId: b}).then(function (data) {
            alert('Permission Updated');
            location.href=configObject.getFullBaseUrl()+'roles';
        }, function (data) {

        });

    };


    $http.post(configObject.getFullBaseUrl() + 'permissionlist').then(function(data){
        $scope.permissions=data.data.data;
        console.log($scope.permissions);
        $scope.loaded.avail = true;
    },function(data){

    });
    // PermissionService.query(function (data) {
    //     $scope.permissions = data.data;
    //     console.log($scope.permissions);
    //     $scope.loaded.avail = true;
    // }, function (data) {
    //
    // });


    RoleService.get({id: b}, function (data) {
        console.log("Getting single role");
        console.log(data);
        //$scope.mes = "niraula";
        $scope.role = data.data[0];
        $scope.availPermissions = $scope.role.permissions
        $scope.loaded.all = true;
    });

    //alert(a);
}]);

// user controller
app.controller('userController',['$scope', 'UserService', 'configObject', '$http', function ($scope, UserService, configObject, $http) {
    UserService.query(function(data){
        $scope.users=data;
        console.log($scope.users);
    });

    $scope.deleteUser = function (id) {
        console.log('Deleting user' + id);
        if (confirm('Sure to Delete?')) {
            UserService.delete({id: id}, function (data) {
                console.log(data);
                alert('deleted');
                $scope.users = $scope.users.filter(function (e) {
                    return  e.id != id;
                });
            }, function (data) {

            });
        }
    };


    //Reject user
    $scope.rejectUser = function (id) {
//            alert(id);
        if (confirm('Sure want to Reject User?')) {
            $http.post(configObject.getFullBaseUrl() + 'rejectuser', {id: id}).then(function (data) {
                console.log(data);
                if (data.data.status === true) {
                    alert('User Rejected.');
                    $scope.users.forEach(function (el) {
                        if (el.id === id) {
                            el.status = 2;
                        }
                    });
                }
            }, function (data) {
                alert('Network Error');
            });
        }
    };


    //De-activate user
    $scope.deactivateuser = function (id) {
//            alert(id);
        if (confirm('Sure want to De-Activate User?')) {
            $http.post(configObject.getFullBaseUrl() + 'deactivateuser', {id: id}).then(function (data) {
                console.log(data);
                if (data.data.status === true) {
                    alert('User De-Activated.');
                    $scope.users.forEach(function (el) {
                        if (el.id === id) {
                            el.status = 0;
                        }
                    });
                }
            }, function (data) {
                alert('Network Error');
            });
        }
    };

    // approve/activate user
    $scope.approveUser = function (id) {
//            alert(id);
        if (confirm('Sure want to Activate User?')) {
            $http.post(configObject.getFullBaseUrl() + 'acceptuser', {id: id}).then(function (data) {
                console.log(data);
                if (data.data.status === true) {
                    alert('User Accepted.');
                    $scope.users.forEach(function (el) {
                        if (el.id === id) {
                            el.status = 1;
                        }
                    });
                }
            }, function (data) {
                alert('Network Error');
            });
        }
    };


    //update password for user

    $scope.updateUserPsd=function(userId){
        console.log(userId);
        console.log($scope.user);
        UserService.update({id:userId}, $scope.user, function (data) {
            console.log(data);
            if(data.status==='success'){
                alert(data.message);
                $('#chgPsd').modal('hide');
            }
        });
    }
    //change the password of user
    $scope.openChangePsdForm=function (userId, userName) {
        console.log(userId);
        console.log(userName);
        $scope.modalData={'modalHeader': 'Change Password',
            'edit':true,
            'user_id': userId,
            'user_name': userName
        };
    }
}]);

//role assignment to user controller
app.controller('rolesAssignmentController', ['$scope', 'RoleService', 'UserService', '$location', 'configObject', '$http', function ($scope, RoleService, UserService, $location, configObject, $http) {
    $scope.loaded = {all: false, avail: false};
    var a = $location.absUrl().lastIndexOf('/');
    var b = $location.absUrl().substr(a + 1);
    console.log(b);

    $scope.$watch('loaded', function (n, o) {
        if (n.all === true && n.avail === true) {
            console.log("Yes everything is fully loaded");
            //extract available permissions id into an array
            $scope.availListId = [];
            for (var i = 0; i < $scope.availRoles.length; i++) {
//                    if ($scope.availPermissions[i].id === id) {
//                        return true;
//                        ;
//                    }
                $scope.availListId.push($scope.availRoles[i].id);

            }
            console.log("all  avail ids");
            console.log($scope.availListId);
            //finsished extracting available permissions ids
            //now extract all permissions for the current role
            $scope.allRoleListId = [];
            for (var i = 0; i < $scope.roles.length; i++) {
//                    if ($scope.availPermissions[i].id === id) {
//                        return true;
//                        ;
//                    }
                $scope.allRoleListId.push($scope.roles[i].id);

            }
            console.log("all ids");
            console.log($scope.allRoleListId);





            $scope.isChecked = function (id) {
                for (var i = 0; i < $scope.availListId.length; i++) {
                    if ($scope.availListId[i] === id) {
                        return true;
                        ;
                    }
                }
                ;
                return false;
//            return true;
            };
        }
    }, true);

    $scope.toggleClick = function (id) {
        if ($scope.availListId.indexOf(id) === -1) {
            $scope.availListId.push(id);
        } else {
            $scope.availListId.splice($scope.availListId.indexOf(id), 1);
        }
        console.log($scope.availListId);
    };
    $scope.updateRoleAssignment = function () {
        console.log("updating role for user");
        $http.post(configObject.getFullBaseUrl() + 'updaterole', {ids: $scope.availListId, userId: b}).then(function (data) {
            alert('Roles Updated');
            location.href = configObject.getFullBaseUrl() + 'users';
        }, function (data) {

        });

    };


    $http.post(configObject.getFullBaseUrl() + 'rolelist').then(function(data){
        $scope.roles=data.data.data;
        console.log($scope.roles);
        $scope.loaded.avail = true;
    },function(data){

    });


    UserService.get({id: b}, function (data) {
        console.log("Getting single user");
        console.log(data);
        //$scope.mes = "niraula";
        $scope.user = data.data[0];
        $scope.availRoles = $scope.user.roles
        $scope.loaded.all = true;
    });



    //alert(a);
}]);