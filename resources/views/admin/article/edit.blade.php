@extends('admin.baselayout')
@section('main-section')
    <div class="containers">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Edit Article
                    <a href="{{'admin/article'}}" class="btn btn-primary pull-right">List Articles</a></h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="row" id="content-margin-btn">

            <div class="col-md-12">
                <form action="{{url('admin/article/'.$article->id)}}" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <input type="hidden" name="_method" value="PATCH">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="col-md-6 form-group">
                            <label for="title" class="control-label">Title *</label>
                            <input type="text"  class="form-control" name="title" id="title" value="{{$article->title}}" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label  class="col-md-12 control-label">Posted By *</label>
                            <input type="text"  class="form-control" name="posted_by" id="posted_by" value="{{$article->posted_by}}" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-10 form-group">
                            <label class="col-md-12 control-label">Description *</label>
                            <textarea rows="3" class="col-md-12 form-control" name="description" id="description"  required>{{$article->description}}</textarea>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-6 form-group">
                            <label  class="col-md-12 control-label">Date *</label>
                            <input type="text"  class="form-control" name="date" id="date" value="{{$article->date}}" required>
                        </div>

                        <div class="col-md-4 form-group">
                            <label  class="col-md-12 control-label">Type *</label>
                            <input type="text"  class="form-control" name="type" id="type" value="{{$article->type}}" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="image" class="control-label">Image</label>
                            <input type="file" name="image" id="image" class="form-control">
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <hr>
                    <div class="col-md-12">
                        <input type="submit" value="Edit Article" class="btn btn-success">
                        <input type="reset" value="Reset" class="btn btn-default">
                        <a href="{{url('admin/article')}}" class="btn btn-danger">Cancel</a>
                    </div>

                </form>
            </div>
        </div>

    </div>
@section('scripts')
    <script>
        $(document).ready(function() {
            $('textarea').jqte({});

            $('#date').datepicker({
                dateFormat: 'yy-mm-dd'
            });
        });
    </script>
@stop
@stop

