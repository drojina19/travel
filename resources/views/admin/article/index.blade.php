@extends('admin.baselayout')
@section('main-section')
    {{--Coded by: Rojina--}}


    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">List Articles
                <a href="{{url('admin/article/create')}}" class="btn btn-primary pull-right">Create New Article</a></h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row" id="content-margin-btn">
        @if(Session::has('flash'))
            <div class="alert alert-success text-center" id="status">
                {{Session::get('flash')}}
            </div>
        @endif

    <?php $a = 1 ?>

            <table id="datatables" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th style="width: 10%">Image</th>
                    <th>Posted By</th>
                    <th>Date</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th>Action</th>

                </tr>
                </thead>


                <tbody>
                <?php $a = 1 ?>
                @foreach($articles as $article)
                    <tr>
                        <td><?= $a ?></td>
                        <td>{{$article->title}}</td>
                        <td>{{$article->description}}</td>
                        <td><img src="{{asset('images/article/'.$article->image)}}" class="img-responsive img"></td>
                        <td>{{$article->posted_by}}</td>
                        <td>{{$article->date}}</td>
                        <td>{{$article->type}}</td>
                        <td>{{$article->status==1?"active":"inactive"}}</td>
                        <td>

                            <a href="{{url('admin/article/'.$article->id.'/edit')}}" class="btn btn-info btn-xs" title="Edit"> <i class="fa fa-edit"></i></a>

                            <form action={{url('admin/article/'.$article->id)}} method="POST">

                                <input type="hidden" name="_token" value="{{csrf_token()}}">

                                <input type="hidden" name="_method" value="DELETE">

                                <button type="submit" class="btn btn-xs btn-danger" onclick="return confirm('Are You Sure??')"><i class="fa fa-trash-o" title="Delete"></i></button>

                            </form>
                        </td>


                    </tr>
                    <?php $a = 1 + $a ?>
                @endforeach
                </tbody>
            </table>


    </div>

@stop