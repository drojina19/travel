<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{config('app.name')}}</title>


    <!-- Bootstrap Core CSS -->
    <link href={{asset('jquery-ui/jquery-ui.min.css')}} rel="stylesheet">
    <link href={{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}} rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href={{asset("bower_components/metisMenu/dist/metisMenu.min.css")}} rel="stylesheet">

    <!-- DataTables CSS -->
    <link href={{asset("bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css")}} rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href={{asset("bower_components/datatables-responsive/css/dataTables.responsive.css")}} rel="stylesheet">

    <!-- Custom CSS -->
    <link href={{asset("dist/css/sb-admin-2.css")}} rel="stylesheet">

    <link href="{{asset("dist/css/custom.css")}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href={{asset("bower_components/font-awesome/css/font-awesome.min.css")}} rel="stylesheet" type="text/css">





    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href={{asset('jqte/jquery-te-1.4.0.css')}} rel="stylesheet">

    <link rel="shortcut icon" href="{{asset('frontend/img/icons/favicon.png')}}">
    <style>
        #content-margin-btn{
            margin-bottom: 90px;
            padding-bottom: 50px;
        }
        .jqte{
            margin-top: 2px;
        }
    </style>
    @yield('css')
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{url('/admin')}}">{{config('app.name')}}</a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">

            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">

                    <li class="divider"></li>
                    <li>

                        <a href="{{ url('/logout')}}" onclick="event.preventDefault();
                                                       document.getElementById('logout-form').submit();">
                            <i class="fa fa-user"> </i>

                            <span class="hidden-xs"> Logout</span>
                        </a>
                        <form id="logout-form" action="{{ url('/logout')}}" method="POST" style="display: none;">
                            {{ csrf_field()}}
                        </form>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li>
                        <a href="{{url('/admin')}}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>


                    <li>
                        <a href=><i class="fa fa-wrench fa-fw"></i> Packages Infos<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{url('admin/country')}}"><i class="fa fa-wrench fa-fw"></i> Country</a>
                            </li>
                            <li>
                                <a href=" {{url('admin/theme')}}"><i class="fa fa-wrench fa-fw"></i> Theme</a>
                            </li>
                            <li>
                                <a href="{{url('admin/tourstyle')}}"><i class="fa fa-wrench fa-fw"></i> TourStyle</a>
                            </li>
                            <li>
                                <a href="{{url('admin/tour')}}"><i class="fa fa-wrench fa-fw"></i> Tour </a>
                            </li>
                        </ul>

                    </li>
                    <li>
                        <a href="{{url('admin/slider')}}"><i class="fa fa-wrench fa-fw"></i> Slider</a>
                    </li>
                    <li>
                        <a href=><i class="fa fa-wrench fa-fw"></i> Component<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            @foreach($components as $component)
                                <li>
                                    <a href={{url('admin/component/'.\Illuminate\Support\Str::slug($component->title))}}><i class="fa fa-table fa-fw"></i> {{$component->title}}</a>
                                </li>
                            @endforeach

                        </ul>

                    </li>








                    <li>
                        <a href="{{url('admin/gallery')}}"><i class="fa fa-wrench fa-fw"></i> Gallery</a>
                    </li>

                    <li>

                    <a href="{{url('admin/article')}}"><i class="fa fa-wrench fa-fw"></i> Article</a>

                    </li>


                    <li>
                        <a href="{{url('admin/faqs')}}"><i class="fa fa-wrench fa-fw"></i> Faqs</a>
                    </li>


                    <li>
                        <a href="{{url('admin/testemonial')}}"><i class="fa fa-wrench fa-fw"></i> Testimonial</a>
                    </li>
                    {{--<li>--}}
                        {{--<a href=><i class="fa fa-wrench fa-fw"></i> Enquiry<span class="fa arrow"></span></a>--}}
                        {{--<ul class="nav nav-second-level">--}}
                            {{--<li>--}}
                                {{--<a href="{{url('admin/enquiry/'.\Illuminate\Support\Str::slug('air-tickets'))}}"><i class="fa fa-table fa-fw"></i>Air Ticket Enquiries</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="{{url('admin/enquiry/'.\Illuminate\Support\Str::slug('tour'))}}"><i class="fa fa-table fa-fw"></i>Tour Enquiries</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="{{url('admin/enquiry/'.\Illuminate\Support\Str::slug('trekking'))}}"><i class="fa fa-table fa-fw"></i>Trekking Enquiries</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        {{--<!-- /.nav-second-level -->--}}
                    {{--</li>--}}

                    {{--<li>--}}
                        {{--<a href="{{url('admin/newsletter')}}"><i class="fa fa-wrench fa-fw"></i> Newletter Suscriber</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href=><i class="fa fa-wrench fa-fw"></i> Component<span class="fa arrow"></span></a>--}}
                        {{--<ul class="nav nav-second-level">--}}
                            {{--@foreach($components as $component)--}}
                                {{--<li>--}}
                                    {{--<a href={{url('admin/component/'.\Illuminate\Support\Str::slug($component->title))}}><i class="fa fa-table fa-fw"></i> {{$component->title}}</a>--}}
                                {{--</li>--}}
                            {{--@endforeach--}}
                            {{--<li>--}}
                            {{--<a href={{url('admin/component/our-philoshopy')}}><i class="fa fa-table fa-fw"></i>Our Philosophy</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        {{--<!-- /.nav-second-level -->--}}
                    {{--</li>--}}

                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper">
        @yield('main-section')
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src={!!asset("bower_components/jquery/dist/jquery.min.js")!!}></script>
<script src={!!asset("jquery-ui/jquery-ui.min.js")!!}></script>

<!-- Bootstrap Core JavaScript -->
<script src={{asset("bower_components/bootstrap/dist/js/bootstrap.min.js")}}></script>

<!-- Metis Menu Plugin JavaScript -->
<script src={{asset("bower_components/metisMenu/dist/metisMenu.min.js")}}></script>

<!-- DataTables JavaScript -->
<script src={{asset("bower_components/datatables/media/js/jquery.dataTables.min.js")}}></script>
<script src={{asset("bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js")}}></script>


<!-- Custom Theme JavaScript -->
<script src={{asset("dist/js/sb-admin-2.js")}}></script>
<script src="{{asset('jqte/jquery-te-1.4.0.min.js')}}"></script>
{{--<script src="{{url('ckeditor/ckeditor.js')}}"></script>--}}
        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script src={{asset("angular/angular.min.js")}}> </script>

<script src="{{asset('angular/app.js')}}"> </script>
<script src="{{asset('angular/controller.js')}}"> </script>



<script>
    $('#datatables').dataTable({
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
    });

    $("#status").delay(3000).slideUp(500);



</script>
@yield('scripts')
</body>

</html>

