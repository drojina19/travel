@extends('admin.baselayout')
@section('main-section')
    {{--Coded by: Rojina--}}


    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">List Blog
                <a href="{{url('admin/blog/create')}}" class="btn btn-primary pull-right">Create New Blog</a></h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row" id="content-margin-btn">
        @if(Session::has('flash'))
            <div class="alert alert-success text-center" id="status">
                {{Session::get('flash')}}
            </div>
        @endif

        <?php $a = 1 ?>

        <table id="datatables" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Description</th>
                <th style="width: 10%">Image</th>
                <th>Posted By</th>
                <th>Date</th>
                <th>Status</th>
                <th>Action</th>

            </tr>
            </thead>


            <tbody>
            <?php $a = 1 ?>
            @foreach($blogs as $blog)
                <tr>
                    <td><?= $a ?></td>
                    <td>{{$blog->title}}</td>
                    <td>{{$blog->description}}</td>
                    <td><img src="{{asset('images/blog/'.$blog->image)}}" class="img-responsive img"></td>
                    <td>{{$blog->posted_by}}</td>
                    <td>{{$blog->date}}</td>
                    <td>{{$blog->status==1?"active":"inactive"}}</td>
                    <td>

                        <a href="{{url('admin/blog/'.$blog->id.'/edit')}}" class="btn btn-info btn-xs" title="Edit"> <i class="fa fa-edit"></i></a>

                        <form action={{url('admin/blog/'.$blog->id)}} method="POST">

                            <input type="hidden" name="_token" value="{{csrf_token()}}">

                            <input type="hidden" name="_method" value="DELETE">

                            <button type="submit" class="btn btn-xs btn-danger" onclick="return confirm('Are You Sure??')"><i class="fa fa-trash-o" title="Delete"></i></button>

                        </form>
                    </td>


                </tr>
                <?php $a = 1 + $a ?>
            @endforeach
            </tbody>
        </table>


    </div>

@stop