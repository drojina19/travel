@extends('admin.baselayout')
@section('main-section')
<div class="containers">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Edit {{ucWords($component->title)}} </h2>

        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row" id="content-margin-btn">
        <div class="col-md-12">
            <form action="{{url('admin/component/'.$component->id)}}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="PATCH">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="row">
                    <div class="col-md-8">
                        <div class="col-md-12 form-group">
                            <label for="description" class="col-md-12 control-label">Description</label>
                            <textarea rows="3" class="col-md-12 form-control" name="description" id="description">{{$component->description}}</textarea>
                        </div>


                        <div class="col-md-6">
                            <label for="" class="control-label"> Image <small style="color: red">(Please upload the image of size 400 x 252px)</small></label>
                            <input type="file" name="image" id="image" class="form-control">
                        </div>
                        <div class="col-md-6" id="">
                            <img id="image-preview" alt="image" class="img-thumbnail" title="image preview" src="{{asset('images/components/'.$component->image)}}">
                        </div>


                        <div class="col-md-12 form-group">
                            <label for="status" class="col-md-12 control-label">Component Status:</label>
                            <select class="form-control" name="status">
                                @if($component->status=='1')
                                <option value="1" selected="true">Active</option>
                                <option value="0">Inactive</option>
                                @else
                                <option value="1">Active</option>
                                <option value="0" selected="true">Inactive</option>
                                @endif

                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    
                    <div class="col-md-12">
                        <input type="submit" value="Update" class="btn btn-success">
                        <input type="reset" value="Reset" class="btn btn-default">
                        <a href="{{url('/admin/component')}}" class="btn btn-danger">Cancel</a>
                    </div>                
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('scripts')

<script src="{{asset('frontend/js/custom.js')}}" type="text/javascript"></script>

    <script>
        $(document).ready(function() {
            $('textarea').jqte({

            });
        });
    </script>


@stop