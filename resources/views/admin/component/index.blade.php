@extends('admin.baselayout')
@section('main-section')
<style>
    table tr:first-child{
        width:1px;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">{{ucWords($component->title)}}</h2>
            <!--<a href={{url('admin/component/create')}} class="btn btn-primary pull-right">Create new Landmark</a></h2>-->
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row" id="content-margin-btn">
    @if(Session::has('flash'))
    <div class="alert alert-success text-center" id="status">
        {{Session::get('flash')}}
    </div>
    @endif

    <?php $a = 1 ?>
    <table class="table table-striped table-bordered" id="datatables">
        <thead>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Description</th>
            <th>Image</th>

            <th>Status</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>



            <tr>
                <td><?= $a ?></td>
                <td>{{$component->title}}</td>
                <td>{!!$component->description!!}</td>

                <td><img src="{{asset('images/components/'.$component->image)}}" class="img img-responsive" style="width: 20em"></td>

                <td>{{$component->status}}</td>
                <td><a href="{{url('admin/component/'.$component->id.'/edit')}}" class="btn btn-default"><i class="fa fa-edit" title=""></i></a>
<!--                    <form action={{url('admin/component/'.$component->id)}} method="POST">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="submit" value="Delete">
                        <button class="btn btn-danger" value="Delete"><i class="fa fa-trash"></i></button>
                    </form>-->
                </td>
            </tr>
        </tbody>

    </table>
</div>

@stop