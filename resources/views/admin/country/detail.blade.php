@extends('admin.baselayout')
@section('main-section')
    <style>
        table tr:first-child{
            width:1px;
        }
    </style>
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">{{$country->name}} Details
                </h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row" id="content-margin-btn">
        <div class="col-md-12">
            <table class="table table-striped table-bordered">
                <tr>

                    <th>Currency</th>

                    <th>Visa Required</th>

                    <th>Telephone</th>

                    <th>Population</th>

                    <th>Language</th>

                </tr>
                <tr>
                    <td>{{$country->currency}}</td>
                    <td>{{$country->visa_req}}</td>
                    <td>{{$country->tele_code}}</td>
                    <td>{{$country->population}}</td>
                    <td>{{$country->language}}</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">

        <?php $a = 1 ?>


            <div class="col-md-6 form-group">
                <label for="history" class="col-md-12 control-label">History:</label>
                <textarea rows="9" class="col-md-12 form-control" name="history" id="history" required>{{$country->history}}</textarea>
            </div>

            <div class="col-md-6 form-group">
                <label for="geography" class="col-md-12 control-label">Geography:</label>
                <textarea rows="9" class="col-md-12 form-control" name="geography" id="geography" required>{{$country->geography}}</textarea>
            </div>
            <div class="col-md-6 form-group">
                <label for="people" class="col-md-12 control-label">People:</label>
                <textarea rows="9" class="col-md-12 form-control" name="people" id="people" required>{{$country->people}}</textarea>
            </div>
            <div class="col-md-6 form-group">
                <label for="religion" class="col-md-12 control-label">Religion:</label>
                <textarea rows="9" class="col-md-12 form-control" name="religion" id="religion" required>{{$country->religion}}</textarea>
            </div>

            <div class="col-md-6 form-group">
                <label for="culture" class="col-md-12 control-label">Culture:</label>
                <textarea rows="9" class="col-md-12 form-control" name="culture" id="culture" required>{{$country->culture}}</textarea>
            </div>


    </div>

@stop