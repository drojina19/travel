@extends('admin.baselayout')
@section('main-section')
    <div class="containers">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Edit Country
                    <a href="{{url('admin/country')}}" class="btn btn-primary pull-right">List Country</a></h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row" id="content-margin-btn">
            <div class="col-md-12">
                <form action="{{url('admin/country/'.$country->id)}}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PATCH">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label for="name" class="control-label">Name *</label>
                            <input type="text"  class="form-control" name="name" id="name" value="{{$country->name}}" required>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6 form-group">
                                    <label for="description" class="col-md-12 control-label">Description *</label>
                                    <textarea rows="9" class="col-md-12 form-control" name="description" id="description" required>{{$country->description}}</textarea>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6">

                            <div class="col-md-12 form-group">

                                <label for="image" class="control-label">Image:</label>

                                <input type="file"  class="form-control" name="image" id="image"  required="required">
                                <div class="alert alert-info" style="color: red; font-size: 15px"> Please upload the image of size 269px X 269px</div>

                            </div>

                        </div>

                        <div class="col-md-3 form-group">
                            <label for="status" class="col-md-12 control-label">Status *</label>
                            <select class="form-control" name="status" required>
                                @if($country->status=='1')
                                    <option value="1" selected="true">Active</option>
                                    <option value="0">Inactive</option>
                                @else
                                    <option value="1">Active</option>
                                    <option value="0" selected="true">Inactive</option>
                                @endif

                            </select>
                        </div>
                        <div class="clearfix"></div>
                        <hr>

                        <div class="col-md-12">
                            <input type="submit" value="Update Country" class="btn btn-success">
                            <input type="reset" value="Reset" class="btn btn-default">
                            <a href="{{url('admin/country')}}" class="btn btn-danger">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@section('scripts')
    <script>
        $(document).ready(function() {
            $('textarea').jqte({

            });
        });
    </script>
@stop
@stop

