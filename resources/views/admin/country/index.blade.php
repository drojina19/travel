@extends('admin.baselayout')
@section('main-section')
    <style>
        table tr:first-child{
            width:1px;
        }
        #coun{
            border-bottom:1px solid grey;
        }
    </style>
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">List Countries
                <a href="{{url('admin/country/create')}}" class="btn btn-primary pull-right">Create new Country</a></h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row" id="content-margin-btn">
        @if(Session::has('flash'))
            <div class="alert alert-success text-center" id="status">
                {{Session::get('flash')}}
            </div>
        @endif

        <?php $a = 1 ?>
        <table class="table table-striped table-bordered" id="datatables">
            <thead>
            <tr>
                <th>SN</th>
                <th>Name</th>
                <th style="width: 20%">Description</th>

                <th  style="width: 10%">Image</th>

                <th>Landmark</th>

                <th>Status</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>

            @foreach($countries as $country)

                    <tr>
                        <td><?= $a ?></td>
                        <td>{{$country->name}}</td>
                        <td>{!! $country->description !!}</td>
                        <td><img src="{{asset('images/country/'.$country->image)}}" class="img-responsive img"></td>

                        <td>

                            <div class="col-md-12">
                                <a href="{{url('admin/c/landmark/add/'.\Illuminate\Support\Str::slug($country->id))}}"><i class="fa fa-plus" title="Add Landmark"></i></a> |
                                <a href="{{url('admin/c/landmark/'.\Illuminate\Support\Str::slug($country->id))}}"><i class="fa fa-table" title="View Landmark"></i></a>

                            </div>

                        </td>

                        <td>@if($country->status==1)
                                <span class="label label-success">Active</span>
                            @else
                                <span class="label label-danger"></span>Inactive</span>
                            @endif
                        </td>
                        <td><a href="{{url('admin/country/'.$country->id.'/edit')}}" class="btn btn-info"><i class="fa fa-edit" title=""></i></a>
                            <a href="{{url('admin/country/'.$country->id)}}" class="btn btn-success"><i class="fa fa-search" title=""></i></a>
                            <form action={{url('admin/country/'.$country->id)}} method="POST">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="DELETE">
                                <!--<input type="submit" value="Delete">-->
                                <button class="btn btn-danger" value="Delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>



                    </tr>

                    <?php $a = 1 + $a ?>


            @endforeach
            </tbody>
        </table>
    </div>

@stop