@extends('admin.baselayout')
@section('main-section')
    <div class="containers">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Create  Faqs
                    <a href="{{url('admin/faqs')}}" class="btn btn-primary pull-right">List Faq</a></h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row" id="content-margin-btn">

            <div class="col-md-12">
                <form action="{{url('admin/faqs')}}" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">

                        <div class="col-md-6 form-group">
                            <label for="tour" class="control-label"> Tour:</label>
                            <select class="form-control" name="tour_id" required>
                                <option value="" selected disabled>Choose Tour Name</option>
                                @foreach($tours as $tour)
                                    <option value="{{$tour->id}}">{{$tour->name}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="col-md-6 form-group">
                            <label for="title" class="col-md-4 control-label"> Title:</label>
                            <input type="text"  class="col-md-8 form-control" name="title" id="title" required>
                        </div>


                        <div class="col-md-6 form-group" >

                                <label for="description" class="col-md-12 control-label">Description:</label>
                                <textarea rows="3" class="col-md-12 form-control" name="description" id="description" required></textarea>

                        </div>




                        <div class="col-md-6 form-group">
                            <label for="status" class="col-md-4 control-label">Status:</label>
                            <select class="form-control" name="status">
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <input type="submit" value="Create Faqs" class="btn btn-success">
                            <input type="reset" value="Reset" class="btn btn-default">
                            <a href="{{url('admin/faqs')}}" class="btn btn-danger">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('scripts')
@section('scripts')
    <script>
        $(document).ready(function() {
            $('textarea').jqte({

            });
        });
    </script>
@stop
@stop