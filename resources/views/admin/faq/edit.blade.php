@extends('admin.baselayout')
@section('main-section')
    <div class="containers">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Edit  Faqs
                    <a href="{{url('admin/faqs')}}" class="btn btn-primary pull-right">List Faqs </a></h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row" id="content-margin-btn">
            <div class="col-md-12">
                <form action="{{url('admin/faqs/'.$faq->id)}}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PATCH">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                    <div class="row">
                        <div class="col-md-6">
                            <label class="col-md-7 form-group"> Tour</label>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <select class="form-control" name="tour_id" required>
                                    @foreach($tours as $tour)
                                        <option value="{{$tour->id}}" {{$faq->tour_id==$tour->id?"selected":""}}>{{$tour->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="col-md-12 form-group">
                                <label for="name" class="col-md-4 control-label"> Name:</label>
                                <input type="text"  class="col-md-8 form-control" name="name" id="name" value="{{$faq->name}}">
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="col-md-12 form-group">
                                <label for="description" class="col-md-12 control-label">Description:</label>
                                <textarea rows="3" class="col-md-12 form-control" name="description" id="description">{{$faq->description}}</textarea>
                            </div>
                        </div>




                        <div class="col-md-12 form-group">
                            <label for="status" class="col-md-12 control-label">Faqs Status:</label>
                            <select class="form-control" name="status">
                                @if($faq->status=='1')
                                    <option value="1" selected="true">Active</option>
                                    <option value="0">Inactive</option>
                                @else
                                    <option value="1">Active</option>
                                    <option value="0" selected="true">Inactive</option>
                                @endif

                            </select>
                        </div>

                        <div class="col-md-12">
                            <input type="submit" value="Update Faqs" class="btn btn-success">
                            <input type="reset" value="Reset" class="btn btn-default">
                            <a href="{{url('admin/faqs')}}" class="btn btn-danger">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('scripts')

    <script>
        $(document).ready(function() {
            $('textarea').jqte({

            });
        });
    </script>


@stop