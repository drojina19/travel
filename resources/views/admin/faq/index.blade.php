@extends('admin.baselayout')
@section('main-section')
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">List Faqs
                <a href="{{url('admin/faqs/create')}}" class="btn btn-primary pull-right">Create New Faq</a></h2>
        </div>

    </div>

    <div class="row" id="content-margin-btn">
        @if(Session::has('flash'))
            <div class="alert alert-success text-center" id="status">
                {{Session::get('flash')}}
            </div>
        @endif

        <?php $a = 1 ?>


           <table class="table table-striped table-bordered" id="datatables">
                <thead>
                <tr>

                    <th>ID</th>

                    <th>Tour</th>

                    <th>Name</th>

                    <th>Description</th>

                    <th>Status</th>

                    <th>Action</th>



                </tr>
                </thead>

                <?php $a = 1 ?>
                <tbody>
                @foreach($faqs as $faq)
                    <tr>

                        <td><?= $a ?></td>

                        <td>{{$faq->tour->name}}</td>

                        <td>{{$faq->title}}</td>

                        <td>{{$faq->description}}</td>

                        <td>{{$faq->status==1?"active":"inactive"}}</td>

                        <td> <a href="{{url('admin/faqs/'.$faq->id.'/edit')}}" class="btn btn-info btn-block">Edit</a>

                            <form action={{url('admin/faqs/'.$faq->id)}} method="POST">

                                <input type="hidden" name="_token" value="{{csrf_token()}}">

                                <input type="hidden" name="_method" value="DELETE">

                                <input type="submit" class="btn btn-danger btn-block" value="Delete" onclick="return confirm('are you sure to delete')">

                            </form>

                        </td>

                    </tr>

                    <?php $a = 1 + $a ?>



                @endforeach
                </tbody>

            </table>




    </div>




@stop