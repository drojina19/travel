@extends('admin.baselayout')
@section('main-section')
    <div class="containers">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Create New Gallery
                    <a href="{{'admin/gallery'}}" class="btn btn-primary pull-right">List Gallery</a></h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row" id="content-margin-btn">

            <div class="col-md-12">
                <form action="{{url('admin/gallery')}}" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">

                        <label class="col-md-7 form-group"> Tour Name</label>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <select class="form-control" name="tour_id" required>
                                <option value="" selected disabled>Choose Appropriate Tour </option>
                                @foreach($tours as $tour)
                                    <option value="{{$tour->id}}">{{$tour->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-md-6 form-group">
                            <label for="image" class="control-label">Image *</label>
                            <input type="file" name="image" id="image" class="form-control" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 form-group">
                            <label for="status" class="control-label">Status *</label>
                            <select class="form-control" name="status" required>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>

                        <div class="clearfix"></div>
                        <hr>
                        <div class="col-md-12">
                            <input type="submit" value="Create " class="btn btn-success">
                            <input type="reset" value="Reset" class="btn btn-default">
                            <a href="{{url('admin/tour')}}" class="btn btn-danger">Cancel</a>
                        </div>
                </form>
            </div>
        </div>
    </div>
@section('scripts')
    <script>
        $(document).ready(function() {
            $('textarea').jqte({

            });
        });
    </script>
@stop
@stop

