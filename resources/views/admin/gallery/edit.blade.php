@extends('admin.baselayout')
@section('main-section')
    <div class="containers">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Edit Gallery
                    <a href="{{url('/admin/gallery')}}" class="btn btn-primary pull-right">
                        List
                    </a>
                </h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row" id="content-margin-btn">
            <div class="col-md-12">
                <form action="{{url('admin/gallery/'.$gallery->id)}}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PATCH">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="row">

                        <label class="col-md-7 form-group"> Tour Name</label>
                        <div class="col-md-6 col-sm-6 col-xs-6">

                            <select class="form-control" name="tour_id" required>
                                @foreach($tours as $tour)
                                    <option value="{{$tour->id}}" {{$gallery->tour_id==$tour->id?"selected":""}}>{{$tour->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-6 form-group">
                            <label for="image" class="control-label">Image</label>
                            <input type="file" name="image" id="image" class="form-control">
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label for="status" class="control-label">Status</label>
                            <select class="form-control" name="status">
                                @if($gallery->status=='1')
                                    <option value="1" selected="true">Active</option>
                                    <option value="0">Inactive</option>
                                @else
                                    <option value="1">Active</option>
                                    <option value="0" selected="true">Inactive</option>
                                @endif

                            </select>
                        </div>
                        <div class="clearfix"></div>
                        <hr>
                        <div class="col-md-12">
                            <input type="submit" value="Update Gallery" class="btn btn-success">
                            <input type="reset" value="Reset" class="btn btn-default">
                            <a href="{{url('admin/gallery')}}" class="btn btn-danger">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@section('scripts')
    <script>
        $(document).ready(function() {
            $('textarea').jqte({

            });
        });
    </script>
@stop
@stop

