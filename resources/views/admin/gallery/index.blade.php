@extends('admin.baselayout')
@section('main-section')

    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">All Recent Galleries
                <a href="{{url('admin/gallery/create')}}" class="btn btn-primary pull-right">Add New Galley</a></h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row" id="content-margin-btn" >

        @if(Session::has('flash'))
            <div class="alert alert-success text-center" id="status">
                {{Session::get('flash')}}
            </div>
        @endif

        <table id="datatables" class="table table-striped table-bordered">

            <tr>

                <th>ID</th>

               <th>Tour Name</th>
                

                <th  style="width: 10%">Image</th>


                <th>Status</th>

                <th>Action</th>

            </tr>

            <?php $a = 1 ?>

            @foreach($galleries as $gallery)


                <tr>

                    <td><?= $a ?></td>


                   <td>{{$gallery->tour->name}}</td>


                    <td><img src="{{asset('images/gallery/'.$gallery->image)}}" class="img-responsive img"></td>

                    <td>{{$gallery->status==1?"active":"inactive"}}</td>

                    <td><a href="{{url('admin/gallery/'.$gallery->id.'/edit')}}" class="btn btn-info btn-block">Edit</a>

                        <form action={{url('admin/gallery/'.$gallery->id)}} method="POST">

                            <input type="hidden" name="_token" value="{{csrf_token()}}">

                            <input type="hidden" name="_method" value="DELETE">

                            <input type="submit" class="btn btn-danger btn-block" value="Delete" onclick="return confirm('are you sure to delete??')">

                        </form>

                    </td>

                </tr>

                <?php $a = 1 + $a ?>

            @endforeach

        </table>

    </div>




@stop

    