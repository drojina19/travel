@extends('admin.baselayout')
@section('main-section')
<div class="containers">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Create  Landmark
                <a href="{{url('admin/c/landmark/'.$c->id)}}" class="btn btn-primary pull-right">List {{$c->name}} Landmarks</a></h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row" id="content-margin-btn">

        <div class="col-md-12">
            <form action="{{url('admin/landmark')}}" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="col-md-6">
                        {{--<div class="col-md-12 form-group">--}}
                            {{--<label for="city_id" class="col-md-4 control-label">City:</label>--}}
                            {{--<select name="city_id" class="col-md-8 form-control" required>--}}
                                {{--<option selected disabled value="">Choose City</option>--}}
                                {{--@foreach($cities as $city)--}}
                                {{--<option value="{{$city->id}}">{{$city->name}}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        <div class=" form-group">
                            <label for="city_id" class="col-md-4 control-label">Country:</label>
                            <input type="text"  class="col-md-8 form-control" id="country_name" value="{{$c->name}}" disabled>
                            <input type="hidden"  class="col-md-8 form-control" name="country_id" value="{{$c->id}}" >
                        </div>
                        <div class=" form-group">
                            <label for="name" class="col-md-4 control-label">Landmark Name *</label>
                            <input type="text"  class="col-md-8 form-control" name="name" id="name" required>
                        </div>  
                        <div class=" form-group">
                            <label for="image" class="col-md-4 control-label">Image *</label>
                            <input type="file"  class="col-md-8 form-control" name="image" id="image" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12 form-group">
                            <label for="description" class="col-md-12 control-label">Description *</label>
                            <textarea rows="3" class="col-md-12 form-control" name="description" id="description" required></textarea>
                        </div>  
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-md-6 form-group">
                        <label for="status" class="col-md-4 control-label">Site Type *</label>
                        <select class="form-control" name="sites" required>
                            <option value="" disabled selected>Choose Site Type</option>
                            <option value="cultural">Cultural Sites</option>
                            <option value="natural">Natural Sites</option>
                        </select>
                    </div>
                    
                    
                    <div class="col-md-6 form-group">
                        <label for="status" class="col-md-4 control-label">Status *</label>
                        <select class="form-control" name="status" required>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <input type="submit" value="Create Landmark" class="btn btn-success">
                        <input type="reset" value="Reset" class="btn btn-default">
                        <a href="{{url('admin/c/landmark/'.$c->id)}}" class="btn btn-danger">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('scripts')
@section('scripts')
    <script>
        $(document).ready(function() {
            $('textarea').jqte({

            });
        });
    </script>
@stop
@stop