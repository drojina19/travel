@extends('admin.baselayout')
@section('main-section')
<div class="containers">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Edit  Landmark
                <a href="{{url('admin/c/landmark/'.$landmark->country->id)}}" class="btn btn-primary pull-right">List {{$landmark->country->name}} Landmarks</a></h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-md-12" id="content-margin-btn">
            <form action="{{url('admin/landmark/'.$landmark->id)}}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="PATCH">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                
                <div class="row">
                    <div class="col-md-6">
                        <div class=" form-group">
                            <label for="country_id" class="col-md-4 control-label">Country:</label>
                            <input type="text" class="form-control" value="{{$landmark->country->name}}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Landmark Name *</label>
                            <input type="text"  class="col-md-8 form-control" name="name" id="name" value="{{$landmark->name}}" required>
                        </div>
                        <div class="form-group">
                            <label for="image" class="col-md-4 control-label">Image</label>
                            <input type="file"  class="col-md-8 form-control" name="image" id="image">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="description" class="col-md-12 control-label">Description *</label>
                            <textarea rows="3" class="col-md-12 form-control" name="description" id="description" required>{{$landmark->description}}</textarea>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-6 form-group">
                        <label for="status" class="col-md-12 control-label">Site Type *</label>
                        <select class="form-control" name="sites" required>
                            @if($landmark->status=='1')
                                <option value="cultural" selected="true">Cultural Sites</option>
                                <option value="natural">Natural Sites</option>
                            @else
                                <option value="cultural">Cultural Sites</option>
                                <option value="natural" selected="true">Natural Sites</option>
                            @endif

                        </select>
                    </div>


                    <div class="col-md-6 form-group">
                        <label for="status" class="col-md-12 control-label">Status *</label>
                        <select class="form-control" name="status" required>
                            @if($landmark->status=='1')
                            <option value="1" selected="true">Active</option>
                            <option value="0">Inactive</option>
                            @else
                            <option value="1">Active</option>
                            <option value="0" selected="true">Inactive</option>
                            @endif

                        </select>
                    </div>
                    
                    <div class="col-md-12">
                        <input type="submit" value="Update Landmark" class="btn btn-success">
                        <input type="reset" value="Reset" class="btn btn-default">
                        <a href="{{url('admin/c/landmark/'.$landmark->country->id)}}" class="btn btn-danger">Cancel</a>
                    </div>                
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('scripts')
@section('scripts')
    <script>
        $(document).ready(function() {
            $('textarea').jqte({

            });
        });
    </script>
@stop

@stop