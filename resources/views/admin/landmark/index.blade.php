@extends('admin.baselayout')
@section('main-section')
<style>
    table tr:first-child{
        width:1px;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">List {{$c->name}} Landmarks
            <a href="{{url('admin/c/landmark/add/'.$c->id)}}" class="btn btn-primary pull-right">Create new Landmark</a></h2>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row" id="content-margin-btn">
    <div class="col-md-12">
        @if(Session::has('flash'))
        <div class="alert alert-success text-center" id="status">
            {{Session::get('flash')}}
        </div>
        @endif

        <?php $a = 1 ?>
        <table id="datatables" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    {{--<th>City Name</th>--}}
                    {{--<th>Country Name</th>--}}
                    <th>Landmark Name</th>
                    <th>Description</th>
                    <th>Image</th>
                    <th>Sites</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($landmarks as $landmark)
                    <tr>
                        <td><?= $a ?></td>
                        {{--<td>{{$landmark->city->name}}</td>--}}
{{--                        <td>{{$landmark->country->name}}</td>--}}
                        <td>{{$landmark->name}}</td>
                        <td>{!!$landmark->description!!}</td>
                        <td><img src="{{asset('images/landmark/'.$landmark->image)}}" class="img img-responsive" style="width: 10em"></td>
                        <td>{{$landmark->sites}}</td>
                        <td>
                            @if($landmark->status==1)
                                <span class="label label-success">Active</span>
                            @else
                                <span class="label label-success">Inactive</span>
                            @endif
                        </td>
                        <td><a href="{{url('admin/landmark/'.$landmark->id.'/edit')}}" class="btn btn-default"><i class="fa fa-edit" title=""></i></a>
                            <form action={{url('admin/landmark/'.$landmark->id)}} method="POST">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="DELETE">
                                <!--<input type="submit" value="Delete">-->
                                <button class="btn btn-danger" value="Delete" onclick="return confirm('are you sure to delete??')"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                <?php $a = 1 + $a ?>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@stop