<div id="myAddInfo" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Additional Informations for @{{tour_id}}</h4>
            </div>
            <div class="modal-body">
                <form name="addinfoForm">
                    <!--<input type="text" ng-model="tourid">-->
                    <input class="form-control" placeholder="Description" ng-model="additionalinfo_description" required>
                    <input id="saveAdditionalInfo"  type="button" value="Add" class="btn btn-primary pull-right" ng-click="saveAddedInfo()" ng-disabled="addinfoForm.$invalid">
                    <!--                                    <input id="updateHighlight" type="button" value="Update Highlight" class="btn btn-primary pull-right" ng-click="updateHighlight(highlightId)">-->
                    <input type="hidden" value='{{url("/api/tour")}}' id="tourapi" >
                    <input type="hidden" value='{{url("api/additionalinfo")}}' id="additionalapi" >


                </form>
                <br>
                <hr>
                <h4>Additional Informations</h4>
                <ul>
                    <li ng-repeat="addinfo in addinfos">
                        @{{addinfo.description}}
                        <a href="" class="fa fa-times pull-right" confirmed-click="deleteaddedinfos(addinfo.id)" ng-confirm-click></a>
                        <!--<i class="fa fa-edit pull-right" ng-click="editHighlight(highlight.id)"></i>-->
                    </li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"  ng-click="closeAdditionalinfo()">Close</button>
            </div>
        </div>

    </div>
</div>