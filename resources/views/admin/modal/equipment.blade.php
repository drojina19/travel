<div id="equipments" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
                <h4 class="modal-title">Equipment for @{{tour_id}}</h4>
            </div>
            <div class="modal-body">
                <form name="equipmentForm">
                    <!--<input type="text" ng-model="tourid">-->
                    <input class="form-control" placeholder="Title" ng-model="equipment_title" required>
                    <input ng-disabled="equipmentForm.$invalid" id="saveEquipment" type="button" value="Create Equipment" class="btn btn-primary pull-right" ng-click="saveEquipment(tour_id)">
                    <input type="hidden" value='{{url("/api/tour")}}' id="tourapi" >
                    <input type="hidden" value='{{url("api/equipment")}}' id="equipmentapi" >


                </form>
                <br>
                <hr>
                <h4>Listing Equipment.</h4>
                <ul>
                    <li ng-repeat="equipment in equipments">
                        @{{equipment.title}}
                        <a href="" class="fa fa-times pull-right" confirmed-click="deleteEquipment(equipment.id)" ng-confirm-click></a>|

                    </li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" ng-click="closeEquipment()">Close</button>
            </div>
        </div>

    </div>
</div>