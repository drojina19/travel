<div id="inclusionModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Inclusion for @{{tour_id}}</h4>
            </div>
            <div class="modal-body">
                <form name="inclusionForm">
                    <!--<input type="text" ng-model="tourid">-->
                    <input class="form-control" placeholder="Description" ng-model="inclusion.description" required>
                    <input id="saveInclusion" type="button" value="Create Inclusion" class="btn btn-primary pull-right" ng-click="saveInclusion()"  ng-disabled="inclusionForm.$invalid">
                    <!--                                    <input id="updateHighlight" type="button" value="Update Highlight" class="btn btn-primary pull-right" ng-click="updateHighlight(highlightId)">-->
                    <input type="hidden" value='{{url("/api/tour")}}' id="tourapi" >
                    <input type="hidden" value='{{url("api/inclusion")}}' id="inclusionapi" >


                </form>
                <br>
                <hr>
                <h4>Listing Inclusions</h4>
                <ul>
                    <li ng-repeat="inclusion in  inclusions">
                        @{{inclusion.description}}
                        <a href="" class="fa fa-times pull-right" confirmed-click="deleteInclusion(inclusion.id)" ng-confirm-click></a>
                        <!--<i class="fa fa-edit pull-right" ng-click="editHighlight(highlight.id)"></i>-->
                    </li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" ng-click="closeInclusion()">Close</button>
            </div>
        </div>

    </div>
</div>