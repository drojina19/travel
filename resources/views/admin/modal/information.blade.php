<div id="informationModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tour Information for @{{tour_id}}</h4>
            </div>
            <div class="modal-body">
                <form name="informationForm">

                    <input type="text" placeholder="Information Title" class="form-control" ng-model="information.title" required>
                    <textarea class="form-control" placeholder="Description" ng-model="information.description" required></textarea>
                    <input id="saveInformation" type="button" value="Create Information" class="btn btn-primary pull-right" ng-click="saveInformation()" ng-disabled="informationForm.$invalid">
                    <input id="updateInformation" type="button" value="Update Information" class="btn btn-primary pull-right" ng-click="updateInformation(information.id)">
                    <input type="hidden" value='{{url("/api/tour")}}' id="tourapi" >
                    <input type="hidden" value='{{url("api/information")}}' id="informationapi" >


                </form>
                <br>
                <hr>
                <h4><u>Listing Information</u></h4>
                <div ng-repeat='info in informations'>
                    <div class="row">
                        <div class="col-md-10">
                            <h4>@{{info.title}}</h4>
                            <p>@{{ info.description }}</p>
                        </div>
                        <div class="col-md-2">
                            <a href="" class="fa fa-times" confirmed-click="deleteInformation(info.id)" ng-confirm-click></a>|
                            <a href="" class="fa fa-edit" ng-click="editInformation(info.id)"></a>
                        </div>
                    </div>
                    <hr>


                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" ng-click="closeInformation()">Close</button>
            </div>
        </div>

    </div>
</div>