

<div id="myPrice" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tour Prices for @{{tour_id}}</h4>
            </div>
            <div class="modal-body">
                <form name="priceForm">
                    <div class="col-sm-12">
                        <div class="form-group col-md-6">
                            <input type="text" placeholder="departure date"   class="form-control datepicker" datepicker ng-model="price.depart_date" required>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="text" placeholder="arrival date"  class="form-control datepicker" datepicker ng-model=" price.arrivedhome_date" required>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="text" placeholder="total price" class="form-control"  ng-model ="price.price" required>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="text" placeholder="savings" class="form-control"  ng-model="price.saving">
                        </div>
                        <div class="form-group col-md-6">
                            <input type="number" placeholder="number of person" class="form-control" ng-model="price.no_of_person" required>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <input id="savePrice" type="button" value="Add Price" class="btn btn-primary pull-right" ng-click="savePrice()" ng-disabled="priceForm.$invalid">
                            <input id="updatePrice" type="button" value="Update Price" class="btn btn-primary pull-right" ng-click="updatePrice(price.id)">
                        </div>
                        <input type="hidden" value='{{url("/api/tour")}}' id="tourapi">
                        <input type="hidden" value='{{url("api/price")}}' id="priceapi">
                    </div>

                </form>
                <br>
                <hr>
                <h4><u>Listing Price</u></h4>
                <div >
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <tr>
                                    <th> S.N</th>
                                    <th> Depart Date</th>
                                    <th> Arrival Date</th>
                                    <th>Total Prices</th>
                                    <th> Savings</th>
                                    <th> Number of People</th>
                                    <th>  Action</th>

                                </tr>

                                <tr ng-repeat='price in prices track by $index'>
                                    <td>@{{ $index +1}}</td>
                                    <td>@{{ price.depart_date | date: "d M, Y" }}</td>
                                    <td>@{{ price.arrivedhome_date }}</td>
                                    <td>@{{ price.price }}</td>
                                    <td>@{{ price.saving }}</td>
                                    <td>@{{ price.no_of_person }}</td>
                                    <td> <a href="" ng-click="editPrice(price.id)" value="Edit"> <i class="fa fa-edit"></i>  </a>
                                        <a href="" confirmed-click="deletePrice(price.id)" ng-confirm-click> <i class="fa fa-trash-o"> </i></a>


                                    </td>



                                </tr>

                            </table>

                        </div>

                    </div>



                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" ng-click="closePrice()">Close</button>
            </div>
        </div>

    </div>
</div>


