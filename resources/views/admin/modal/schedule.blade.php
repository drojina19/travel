<div id="mySchedule" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tour Schedule for @{{tour_id}}</h4>
            </div>
            <div class="modal-body">
                <form name="scheduleForm">

                    <input type="text" placeholder="Schedule Title" class="form-control" ng-model="schedule.title" required>
                    <textarea class="form-control" placeholder="Description" ng-model="schedule.description" required></textarea>
                    <input id="saveSchedule" type="button" value="Create Schedule" class="btn btn-primary pull-right" ng-click="saveSchedule()" ng-disabled="scheduleForm.$invalid">
                    <input id="updateSchedule" type="button" value="Update Schedule" class="btn btn-primary pull-right" ng-click="updateSchedule(scheduleId)">
                    <input type="hidden" value='{{url("/api/tour")}}' id="tourapi" >
                    <input type="hidden" value='{{url("api/schedule")}}' id="scheduleapi" >


                </form>
                <br>
                <hr>
                <h4><u>Listing Schedule</u></h4>
                <div ng-repeat='schedule in schedules'>
                    <div class="row">
                        <div class="col-md-10">
                            <h4>@{{schedule.title}}</h4>
                            <p>@{{ schedule.description }}</p>
                        </div>
                        <div class="col-md-2">
                            <a href="" class="fa fa-times" confirmed-click="deleteSchedule(schedule.id)" ng-confirm-click></a>|
                            <a href="" class="fa fa-edit" ng-click="editSchedule(schedule.id)"></a>



                        </div>
                    </div>
                    <hr>


                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"  ng-click="closeSchedule()">Close</button>
            </div>
        </div>

    </div>
</div>