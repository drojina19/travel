@extends('admin.baselayout')
@section('main-section')

<div class="containers">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Slider</h1>
        </div>
    </div>
    <div class="row" id="content-margin-btn">

        <div class="col-md-12">

            <form action="{{url('admin/slider/'.$slider->id)}}" method="POST" enctype="multipart/form-data">



                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="_method" value="PATCH">

                <div class="row">

                    <div class="col-md-6">

                        <div class="col-md-12 form-group">

                            <label for="name" class="col-md-4 control-label"> Slider_title1:</label>

                            <input type="text"  class="col-md-8 form-control" name="slider_title1" id="name" value="{{$slider->slider_title1}}" required>

                        </div>
                        <div class="col-md-12 form-group">

                            <label for="name" class="col-md-4 control-label"> Slider_title2:</label>

                            <input type="text"  class="col-md-8 form-control" name="slider_title2" value="{{$slider->slider_title2}}" id="name" required>

                        </div>

                    </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="description" class="control-label col-md-12">Description</label>
                            <textarea name="description" id="description"  class="form-control" rows="10" cols="">{!! $slider->slider_desc !!}</textarea>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-md-6">

                        <div class="col-md-12 form-group">

                            <label for="image" class="control-label">Image:</label>

                            <input type="file"  class="form-control" name="image" id="image">
                            <div class="alert alert-info" style="color: red; font-size: 15px"> Please upload the image of size 269px X 269px</div>

                        </div>

                    </div>

                    <div class="col-md-6">

                        <div class="col-md-12 form-group">

                            <label for="status" class="col-md-4 control-label">Status:</label>

                            <select class="form-control" name="status">

                                <option value="1">Active</option>

                                <option value="0">Inactive</option>

                            </select>

                        </div>

                    </div>



                </div>
                <div class="">
                    <input type="submit" value="Update Slider" class="btn btn-success">
                    <input type="reset" class="btn btn-default">
                    <a href="{{url('admin')}}" type="submit" class="btn btn-danger">Cancel</a>
                </div>








                

            </form>

        </div>

    </div>

</div>
@section('scripts')
    <script>
        $(document).ready(function() {
            $('textarea').jqte({

            });
        });
    </script>
@stop

@stop

