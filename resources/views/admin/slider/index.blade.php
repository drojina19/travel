@extends('admin.baselayout')
@section('main-section')

    <style>
        table tr:first-child{
            width:1px;
        }
    </style>
<div class="row">

    <div class="col-lg-12">
        <h1 class="page-header">List Sliders
            <a href="{{url('admin/slider/create')}}" class="btn btn-primary pull-right">Add New Slider</a>
        </h1>
    </div>
</div>
    <div class="row" id="content-margin-btn">

        @if(Session::has('flash'))
        <div class="alert alert-success text-center" id="status">
            {{Session::get('flash')}}
        </div>
        @endif

    <table class="table table-striped table-bordered" id="datatables">

        <tr>

            <th>ID</th>

            <th>Slider_title1</th>

            <th>Slider_title2</th>
            
            <th>Description</th>

            <th  style="width: 10%">Image</th>


            

            <th>Status</th>

            <th>Action</th>



        </tr>

        <?php $a = 1 ?>

        @foreach($sliders as $slider)





        <tr>

            <td><?= $a ?></td>

            <td>{{$slider->slider_title1}}</td>

            <td>{{$slider->slider_title2}}</td>
            
            <td> {!! $slider->slider_desc !!}</td>

            <td><img src="{{asset('images/slider/'.$slider->image)}}" class="img-responsive img"></td>
           

            <td>{{$slider->status==1?"active":"inactive"}}</td>

            <td><a href="{{url('admin/slider/'.$slider->id.'/edit')}}" class="btn btn-info btn-block">Edit</a>

                <form action={{url('admin/slider/'.$slider->id)}} method="POST">

                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                    <input type="hidden" name="_method" value="DELETE">

                    <input type="submit" class="btn btn-danger btn-block" value="Delete" onclick="return confirm('are you sure to delete')">

                </form>

            </td>

        </tr>

        <?php $a = 1 + $a ?>



        @endforeach

    </table>

</div>

@stop