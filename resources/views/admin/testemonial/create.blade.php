@extends('admin.baselayout')
@section('main-section')

    <div class="containers">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Add New Testimonials</h1>
            </div>
        </div>
        <div class="row" id="content-margin-btn">

            <div class="col-md-12">

                <form action="{{url('admin/testemonial')}}" method="POST" enctype="multipart/form-data">



                    <input type="hidden" name="_token" value="{{csrf_token()}}">


                    <div class="row">

                        <div class="col-md-6">

                            <div class="col-md-12 form-group">

                                <label for="name" class="col-md-4 control-label"> Testimonial title:</label>

                                <input type="text"  class="col-md-8 form-control" name="title" id="name" required="required">

                            </div>


                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="description" class="control-label col-md-12">Description</label>
                                <textarea name="description" id="description" class="form-control" rows="10" cols=""></textarea>
                            </div>
                        </div>





                        <div class="col-md-6">

                            <div class="col-md-12 form-group">

                                <label for="image" class="control-label">Image:</label>

                                <input type="file"  class="form-control" name="image" id="image"  required="required">
                                <div class="alert alert-info" style="color: red; font-size: 15px"> Please upload the image of size 269px X 269px</div>

                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="col-md-12 form-group">

                                <label for="status" class="col-md-4 control-label">Status:</label>

                                <select class="form-control" name="status">

                                    <option value="1">Active</option>

                                    <option value="0">Inactive</option>

                                </select>

                            </div>

                        </div>



                    </div>
                    <div class="">
                        <input type="submit" value="Add Testimonial" class="btn btn-success">
                        <input type="reset" class="btn btn-default">
                        <a href="{{url('admin/testemonial')}}" type="submit" class="btn btn-danger">Cancel</a>
                    </div>


                </form>

            </div>

        </div>

    </div>
@section('scripts')
    <script>
        $(document).ready(function() {
            $('textarea').jqte({

            });
        });
    </script>
@stop

@stop

