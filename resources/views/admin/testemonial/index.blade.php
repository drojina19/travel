@extends('admin.baselayout')
@section('main-section')
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">List all Testimonials
                <a href="{{url('admin/testemonial/create')}}" class="btn btn-primary pull-right">Create New Testimonials</a></h2>
        </div>

    </div>

    <div class="row" id="content-margin-btn">
        @if(Session::has('flash'))
            <div class="alert alert-success text-center" id="status">
                {{Session::get('flash')}}
            </div>
        @endif

        <?php $a = 1 ?>


        <table class="table table-striped table-bordered" id="datatables">
            <thead>
            <tr>

                <th>ID</th>

                <th>Title</th>

                <th>Description</th>

                <th>Image</th>

                <th>Status</th>

                <th>Action</th>



            </tr>
            </thead>

            <?php $a = 1 ?>
            <tbody>
            @foreach($testemonials as $testemonial)
                <tr>

                    <td><?= $a ?></td>

                    <td>{{$testemonial->title}}</td>

                    <td>{{$testemonial->description}}</td>

                    <td><img src="{{asset('images/testemonial/'.$testemonial->image)}}" class="img-responsive img"></td>

                    <td>{{$testemonial->status==1?"active":"inactive"}}</td>

                    <td> <a href="{{url('admin/testemonial/'.$testemonial->id.'/edit')}}" class="btn btn-info btn-block">Edit</a>

                        <form action={{url('admin/testemonial/'.$testemonial->id)}} method="POST">

                            <input type="hidden" name="_token" value="{{csrf_token()}}">

                            <input type="hidden" name="_method" value="DELETE">

                            <input type="submit" class="btn btn-danger btn-block" value="Delete" onclick="return confirm('are you sure to delete')">

                        </form>

                    </td>

                </tr>

                <?php $a = 1 + $a ?>



            @endforeach
            </tbody>

        </table>




    </div>




@stop