@extends('admin.baselayout')
@section('main-section')
    <div class="containers">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Create
                    <a href="{{'/admin/theme'}}" class="btn btn-primary pull-right">List Theme</a></h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row" id="content-margin-btn">

            <div class="col-md-12">
                <form action="{{url('admin/theme')}}" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">

                        <div class="col-md-6 form-group">
                            <label for="name" class="control-label">Name *</label>
                            <input type="text"  class="form-control" name="name" id="name" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="description" class="control-label">Description *</label>
                            <textarea rows="3" class="form-control" name="description" id="description" required></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="image" class="control-label">Image *</label>
                            <input type="file" name="image" id="image" class="form-control" required>
                        </div>

                        <div class="col-md-3 form-group">
                            <label for="status" class="control-label">Status *</label>
                            <select class="form-control" name="status" required>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>



                        <div class="clearfix"></div>
                        <hr>


                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <input type="submit" value="Create " class="btn btn-success">
                            <input type="reset" value="Reset" class="btn btn-default">
                            <a href="{{url('admin/tour')}}" class="btn btn-danger">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@section('scripts')
    <script>
        $(document).ready(function() {
            $('textarea').jqte({

            });
        });
    </script>
@stop
@stop

