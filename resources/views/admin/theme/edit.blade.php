@extends('admin.baselayout')
@section('main-section')
    <div class="containers">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Edit  for
                    <a href="{{url('/admin/theme')}}" class="btn btn-primary pull-right">
                        List
                    </a>
                </h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row" id="content-margin-btn">
            <div class="col-md-12">
                <form action="{{url('admin/theme/'.$theme->id)}}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PATCH">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                    <div class="row">
                        <div class="col-md-5 form-group">
                            <label for="name" class="control-label">Name *</label>
                            <input type="text"  class="form-control" name="name" id="name" value="{{$theme->name}}" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="description" class="control-label">Description *</label>
                            <textarea rows="3" class="form-control" name="description" id="description" required>{{$theme->description}}</textarea>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-8 form-group">
                            <label for="image" class="control-label">Image</label>
                            <input type="file" name="image" id="image" class="form-control">
                        </div>




                        <div class="col-md-4 form-group">
                            <label for="status" class="control-label">Status</label>
                            <select class="form-control" name="status">
                                @if($theme->status=='1')
                                    <option value="1" selected="true">Active</option>
                                    <option value="0">Inactive</option>
                                @else
                                    <option value="1">Active</option>
                                    <option value="0" selected="true">Inactive</option>
                                @endif

                            </select>
                        </div>
                        <div class="clearfix"></div>
                        <hr>
                        <div class="col-md-12">
                            <input type="submit" value="Update Theme" class="btn btn-success">
                            <input type="reset" value="Reset" class="btn btn-default">
                            <a href="{{url('admin/theme')}}" class="btn btn-danger">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@section('scripts')
    <script>
        $(document).ready(function() {
            $('textarea').jqte({

            });
        });
    </script>
@stop
@stop

