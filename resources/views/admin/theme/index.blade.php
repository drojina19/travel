@extends('admin.baselayout')
@section('main-section')

    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">List Themes
                <a href="{{url('admin/theme/create')}}" class="btn btn-primary pull-right">Create New Theme</a></h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row" id="content-margin-btn" >

        @if(Session::has('flash'))
            <div class="alert alert-success text-center" id="status">
                {{Session::get('flash')}}
            </div>
        @endif

        <table class="table table-striped table-bordered" id="datatables">

            <tr>

                <th>ID</th>

                <th>Name</th>

                <th>Description</th>

                <th  style="width: 10%">Image</th>

               
                <th>Status</th>

                <th>Action</th>



            </tr>

            <?php $a = 1 ?>

            @foreach($themes as $theme)


                <tr>

                    <td><?= $a ?></td>


                    <td>{{$theme->name}}</td>

                    <td> {!! $theme->description !!}</td>

                    <td><img src="{{asset('images/theme/'.$theme->image)}}" class="img-responsive img"></td>
                    


                    <td>{{$theme->status==1?"active":"inactive"}}</td>

                    <td><a href="{{url('admin/theme/'.$theme->id.'/edit')}}" class="btn btn-info btn-block">Edit</a>

                        <form action={{url('admin/theme/'.$theme->id)}} method="POST">

                            <input type="hidden" name="_token" value="{{csrf_token()}}">

                            <input type="hidden" name="_method" value="DELETE">

                            <input type="submit" class="btn btn-danger btn-block" value="Delete" onclick="return confirm('are you sure to delete')">

                        </form>

                    </td>

                </tr>

                <?php $a = 1 + $a ?>



            @endforeach

        </table>

    </div>




@stop

    