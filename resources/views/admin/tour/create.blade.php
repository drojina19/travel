@extends('admin.baselayout')
@section('main-section')
<div class="containers">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Create Tour
                <a href="{{'admin/tour'}}" class="btn btn-primary pull-right">List Tour</a></h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row" id="content-margin-btn">

        <div class="col-md-12">
            <form action="{{url('admin/tour')}}" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                    <div class="col-md-4 form-group">
                        <label for="theme" class="control-label">Theme </label>
                        <select class="form-control" name="theme_id" required>
                            <option value="" selected disabled>Choose Appropriate Theme</option>
                            @foreach($themes as $theme)
                                <option value="{{$theme->id}}">{{$theme->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-4 form-group">
                        <label for="country" class="control-label">Country </label>
                        <select class="form-control" name="country_id" required>
                            <option value="" selected disabled>Choose Appropriate Country</option>
                            @foreach($countries as $country)
                                <option value="{{$country->id}}">{{$country->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-3 form-group">
                        <label for="tourstyle" class="col-md-12 control-label">Trip Style:</label>
                        <select class="form-control"  name="tour_style_id" required>
                            <option selected disabled value="">Choose Appropriate Trip Style</option>
                            @foreach($tourstyles as $tourstyle)
                                <option value="{{$tourstyle->id}}">{{$tourstyle->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-5 form-group">
                        <label for="name" class="control-label">Name *</label>
                        <input type="text"  class="form-control" name="name" id="name" required>
                    </div>

                    <div class="col-md-3 form-group">
                        <label for="duration" class="control-label">Duration *</label>
                        <input type="text"  class="form-control" name="duration" id="duration" required>
                         <div class="alert alert-danger">Please do not enter days </div>
                    </div>
                    <div class="col-md-3 form-group">
                        <label for="price" class="control-label">Price *</label>
                        <input type="text"  class="form-control" name="price" id="price" required>
                        <div class="alert alert-danger">Please do not enter dollar symbol'$'</div>
                    </div>

                   




                    <div class="clearfix"></div>
                    

                    <div class="col-md-6 form-group">
                        <label for="description" class="control-label">Description *</label>
                        <textarea rows="3" class="form-control" name="description" id="description" required></textarea>
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="placesvisited" class="control-label">Places Visited *</label>
                        <textarea rows="3" class="form-control" name="placesvisited" id="placesvisited" required></textarea>
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="image" class="control-label">Image *</label>
                        <input type="file" name="image" id="image" class="form-control" required>
                    </div>





                    <div class="col-md-3 form-group">
                        <label for="status" class="control-label">Status *</label>
                        <select class="form-control" name="status" required>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                        </select>
                    </div>



                    <div class="clearfix"></div>
                    <hr>

                    
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <input type="submit" value="Create " class="btn btn-success">
                        <input type="reset" value="Reset" class="btn btn-default">
                        <a href="{{url('admin/tour')}}" class="btn btn-danger">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@section('scripts')
    <script>
        $(document).ready(function() {
            $('textarea').jqte({

            });
        });
    </script>
@stop
@stop

