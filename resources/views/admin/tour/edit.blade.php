@extends('admin.baselayout')
@section('main-section')
<div class="containers">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Edit  for {{$tour->name}}
                <a href="{{url('/admin/tour')}}" class="btn btn-primary pull-right">
                    List
                </a>
            </h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row" id="content-margin-btn">
        <div class="col-md-12">
            <form action="{{url('admin/tour/'.$tour->id)}}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="PATCH">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="row">

                     <div class="col-md-4 form-group">
                        <label for="theme" class="control-label">Theme </label>
                        <select class="form-control" name="theme_id" required>
                            @foreach($themes as $theme)
                                <option value="{{$theme->id}}" {{$tour->theme_id==$theme->id?"selected":""}}>
                                    {{$theme->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    
                    <div class="col-md-4 form-group">
                        <label for="country" class="control-label">Country </label>
                        <select class="form-control" name="country_id" required>
                            <option value="" selected disabled>Choose Appropriate Theme</option>
                            @foreach($countries as $country)
                                <option value="{{$country->id}}" {{$tour->country_id==$country->id?"selected":""}}>{{$country->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-3 form-group">
                        <label for="tourstyle" class="col-md-12 control-label">Trip Style:</label>
                        <select class="form-control"  name="tour_style_id" required>
                            @foreach($tourstyles as $tourstyle)
                                <option value="{{$tourstyle->id}}" {{$tour->tour_style_id==$tourstyle->id?"selected":""}}>{{$tourstyle->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-5 form-group">
                        <label for="name" class="control-label">Name *</label>
                        <input type="text"  class="form-control" name="name" id="name" value="{{$tour->name}}" required>
                    </div>

                    <div class="col-md-3 form-group">
                        <label for="duration" class="control-label">Duration *</label>
                        <input type="text"  class="form-control" name="duration" id="duration" value="{{$tour->duration}}" required>
                         <div class="alert alert-danger">Please do not enter days</div>
                    </div>
                    <div class="col-md-3 form-group">
                        <label for="price" class="control-label">Price *</label>
                        <input type="text"  class="form-control" name="price" id="price" value="{{$tour->price}}" required>
                        <div class="alert alert-danger">Please do not enter dollar symbol'$'</div>
                    </div>

                    
                    <div class="col-md-6 form-group">
                        <label for="description" class="control-label">Description *</label>
                        <textarea rows="3" class="form-control" name="description" id="description" required>{{$tour->description}}</textarea>
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="placesvisited" class="control-label">Places Visited *</label>
                        <textarea rows="3" class="form-control" name="placesvisited" id="placesvisited" required>{{$tour->placesvisited}}</textarea>
                    </div>


                    <div class="col-md-8 form-group">
                        <label for="image" class="control-label">Image</label>
                        <input type="file" name="image" id="image" class="form-control">
                    </div>

                    <div class="col-md-4 form-group">
                        <label for="status" class="control-label">Status</label>
                        <select class="form-control" name="status">
                            @if($tour->status==1)
                            <option value="1" selected="true">Active</option>
                            <option value="0">Inactive</option>
                            @else
                            <option value="1">Active</option>
                            <option value="0" selected="true">Inactive</option>
                            @endif

                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <hr>
                    <div class="col-md-12">
                        <input type="submit" value="Update" class="btn btn-success">
                        <input type="reset" value="Reset" class="btn btn-default">
                        <a href="{{url('admin/tour')}}" class="btn btn-danger">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@section('scripts')
    <script>
        $(document).ready(function() {
            $('textarea').jqte({

            });
        });
    </script>
@stop
@stop

