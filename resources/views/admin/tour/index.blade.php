@extends('admin.baselayout')
@section('main-section')
<style>
    table tr:first-child{
        width:1px;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">List Tour
            <a href="{{url('admin/tour/create')}}" class="btn btn-primary pull-right">Add New Tour</a></h2>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row" id="content-margin-btn" >
    <div class="col-md-12">
        @if(Session::has('flash'))
            <div class="alert alert-success text-center" id="status">
                {{Session::get('flash')}}
            </div>
        @endif
        <div class="row" ng-app="scheduleApp" id="content-margin-btn">
            <div  ng-controller="highlightsController">
                @include('admin.modal.highlight')
                <div ng-controller="inclusionController">
                    @include('admin.modal.inclusion')
                    <div ng-controller="informationController">
                        @include('admin.modal.information')
                        <div  ng-controller="scheduleController">
                            @include('admin.modal.schedule')
                            <div  ng-controller="additionalInfoController">
                                @include('admin.modal.additionalinfo')
                                <div  ng-controller="priceController">
                                    @include('admin.modal.price')
                                    <div ng-controller="equipmentController">
                                        @include('admin.modal.equipment')
                                        <div class="tab-content" >
                                            <div class="tab-pane fade in active">
                                                <div >
                                                    <div class="x_title">
                                                        <h3>Choose Filter Options</h3>
                                                        <div class="pull-left">
                                                            <select id="sel-country" class="form-control">
                                                                <option value="" selected disabled>Choose Country</option>
                                                                <option value="">All Country</option>
                                                                @foreach($countries as $country)
                                                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <div class="clearfix"></div>

                                                        </div>
                                                        <div class="pull-left">
                                                            <select id="sel-theme" class="form-control">
                                                                <option value="" selected disabled>Choose Theme</option>
                                                                <option value="">All Themes</option>
                                                                @foreach($themes as $theme)
                                                                    <option value="{{$theme->id}}">{{$theme->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <div class="clearfix"></div>

                                                        </div>
                                                        <div class="pull-left">
                                                            <select id="sel-style" class="form-control">
                                                                <option value="" selected disabled> Choose Tour Style</option>
                                                                <option value="">All Themes</option>
                                                                @foreach($styles as $style)
                                                                    <option value="{{$style->id}}">{{$style->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <div class="clearfix"></div>

                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <hr>
                                                    <?php $a = 1 ?>
                                                    <table class="table table-striped table-bordered" id="datatables">
                                                        <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Name</th>
                                                            <th>Country</th>
                                                            <th>Theme</th>
                                                            <th>Tour Style</th>
                                                            <th>Description</th>
                                                            <th>Places Visited</th>
                                                            <th>Duration</th>
                                                            <th>Price</th>
                                                            <th> Image</th>
                                                            <th>Other Infos</th>

                                                            <th>Status</th>

                                                            <th>Action</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($tours as $tour)
                                                            <tr>
                                                                <td><?= $a ?></td>
                                                                <td>
                                                                    {{$tour->name}}
                                                                </td>
                                                                <td>
                                                                    {{$tour->country->name}}
                                                                    <div class="hidden">{{$tour->country_id}}</div>
                                                                </td>
                                                                <td>
                                                                    {{$tour->theme->name}}
                                                                    <div class="hidden">{{$tour->theme_id}}</div>
                                                                </td>
                                                                <td>
                                                                    {{$tour->tourStyle->name}}
                                                                    <div class="hidden">{{$tour->tour_style_id}}</div>
                                                                </td>
                                                                <td>
                                                                    <center>
                                                                        <a href="" data-toggle="modal" data-target="#descriptionModal-{{$tour->id}}">
                                                                            <i class="fa fa-search fa-2x" title="View Tour Description"></i>
                                                                        </a>
                                                                    </center>
                                                                    @include('admin.tour.modal._descmodal')
                                                                </td>
                                                                <td>
                                                                    <center>
                                                                        <a href="" data-toggle="modal" data-target="#placeModal-{{$tour->id}}">
                                                                            <i class="fa fa-search fa-2x" title="View Plae Visited"></i>
                                                                        </a>
                                                                    </center>
                                                                    @include('admin.tour.modal._placemodal')
                                                                </td>
                                                                <td>{{$tour->duration}} Days</td>
                                                                <td>$ {{$tour->price}}</td>
                                                                <td style="width:10%" > <img src="{{asset('images/tour/'.$tour->image)}}" class="img-responsive"></td>
                                                                <td>
<<<<<<< HEAD
                                                                    <p>
                                                                        <a ng-click="fetchTour({{$tour -> id}})" class="btn btn-info btn-sm btn-block" data-toggle="modal" data-target="#highlightsModal" data-controls-modal="#landmarkTourModal" data-backdrop="static" data-keyboard="false"><span class="" title="Manage Highlights">Highlights</span></a>
                                                                    </p>
                                                                    <p>
                                                                        <a  ng-click="fetchTourInc({{$tour -> id}})" class="btn btn-info btn-sm btn-block" data-toggle="modal" data-target="#inclusionModal" ><span class="" title="Manage Inclusions">Inclusions</span></a>
                                                                    </p>
=======
                                                                    <a href="" id="showthis-{{$tour->id}}" onclick="showdiv({{$tour->id}})" style="text-decoration: underline">Show</a>
                                                                    <a href="" class="hide" id="hidethis-{{$tour->id}}" onclick="hidediv({{$tour->id}})" style="text-decoration: underline">Hide</a>
                                                                    <div id="add_links-{{$tour->id}}" class="hide">
                                                                        <p>
                                                                            <a ng-click="fetchTour({{$tour -> id}})" class="btn btn-info btn-sm btn-block" data-toggle="modal" data-target="#highlightsModal" data-controls-modal="#landmarkTourModal" data-backdrop="static" data-keyboard="false"><span class="" title="Manage Highlights">highlights</span></a>
                                                                        </p>
                                                                        <p>
                                                                            <a  ng-click="fetchTourInc({{$tour -> id}})" class="btn btn-info btn-sm btn-block" data-toggle="modal" data-target="#inclusionModal" ><span class="" title="Manage Inclusions">Inclusions</span></a>
                                                                        </p>
>>>>>>> 2ee1dca842b7ca1c79e7b43e7e1e61de37b5cb05

                                                                        <p>
                                                                            <a   ng-click="fetchTourInformation({{$tour -> id}})" class="btn btn-info btn-sm btn-block" data-toggle="modal" data-target="#informationModal" ><span class="" title="Manage Information">Information</span></a>
                                                                        </p>


                                                                        <p>
                                                                            <a  class="" data-toggle="modal" data-target="#mySchedule" ng-click="popupdata({{$tour -> id}})"><button type="button" class="btn btn-info btn-sm btn-block">Schedule</button></a>
                                                                        </p>


                                                                        <p>
                                                                            <a  class="" data-toggle="modal" data-target="#myPrice" ng-click="appeardata({{$tour -> id}})"><button type="button" class="btn btn-info btn-sm btn-block">Price</button></a>
                                                                        </p>
                                                                        <p>
                                                                            <a class="" data-toggle="modal" data-target="#equipments" ng-click="fetchequipment({{$tour->id}})"> <button type="button" class="btn btn-info btn-sm btn-block"> Equipment</button></a>
                                                                        </p>

                                                                        <p>
                                                                             <a  class="" data-toggle="modal" data-target="#myAddInfo" ng-click="fetchInfo({{$tour -> id}})"><button type="button" class="btn btn-info btn-sm btn-block">Additional Info</button></a>
                                                                         </p>
                                                                    </div>



                                                                </td>


                                                                <td>
                                                                    <?php
                                                                    if($tour -> status==1){
                                                                        echo '<span class="label label-success">Active</span>';
                                                                    }else{
                                                                        echo '<span class="label label-danger">Inactive</span>';
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td ><a href="{{url('admin/tour/'.$tour->id.'/edit')}}" class="btn btn-default"><i class="fa fa-edit" title=""></i></a>
                                                                    <form action={{url('admin/tour/'.$tour->id)}} method="POST">
                                                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                                        <input type="hidden" name="_method" value="DELETE">
                                                                        <!--<input type="submit" value="Delete">-->
                                                                        <button class="btn btn-danger" value="Delete" onclick="return confirm('are you sure to delete')"><i class="fa fa-trash"></i></button>
                                                                    </form>


                                                                </td>
                                                            </tr>

                                                            <?php $a = 1 + $a; ?>
                                                        @endforeach
                                                        </tbody>

                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
    <script>
        var table =  $('#datatables').DataTable();
        $('#sel-country').on('change', function () {
            table.columns(2).search(this.value).draw();
        } );
        $('#sel-theme').on('change', function () {
            table.columns(3).search(this.value).draw();
        } );
        $('#sel-style').on('change', function () {
            table.columns(4).search(this.value).draw();
        } );


        function showdiv(id) {
            $("#add_links-"+id).removeClass("hide");
            $("#hidethis-"+id).removeClass("hide");
            $("#showthis-"+id).addClass("hide");
        }

        function hidediv(id) {
            $("#add_links-"+id).addClass("hide");
            $("#hidethis-"+id).addClass("hide");
            $("#showthis-"+id).removeClass("hide");
        }
    </script>
@stop

