@extends('admin.baselayout')
@section('main-section')
    <div class="containers">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Create  Tour Style
                    <a href="{{url('admin/tourstyle')}}" class="btn btn-primary pull-right">List Tour Style</a></h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">

            <div class="col-md-12" >
                <form action="{{url('admin/tourstyle')}}" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="col-md-6 form-group">
                            <label for="name" class="col-md-4 control-label">Name:</label>
                            <input type="text"  class="col-md-8 form-control" name="name" id="name" required>
                        </div>

                        <div class="col-md-6 form-group">
                            <label for="description" class="col-md-12 control-label">Description:</label>
                            <textarea rows="3" class="col-md-12 form-control" name="description" id="description" required></textarea>
                        </div>

                        <div class="col-md-6">

                            <div class="col-md-12 form-group">

                                <label for="image" class="control-label">Image:</label>

                                <input type="file"  class="form-control" name="image" id="image"  required="required">
                                <div class="alert alert-info" style="color: red; font-size: 15px"> Please upload the image of size 269px X 269px</div>

                            </div>
                        </div>


                        <div class="col-md-6 form-group">
                            <label for="status" class="col-md-4 control-label">Status:</label>
                            <select class="form-control" name="status">
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>

                    </div>
                    <div class="row">
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <input type="submit" value="Create Tour Style" class="btn btn-success">
                            <input type="reset" value="Reset" class="btn btn-default">
                            <a href="{{url('admin/tourstyle')}}" class="btn btn-danger">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@section('scripts')
    <script>
        $(document).ready(function() {
            $('textarea').jqte({

            });
        });
    </script>
@stop
@stop
