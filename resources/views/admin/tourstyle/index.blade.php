@extends('admin.baselayout')
@section('main-section')
    <style>
        table tr:first-child{
            width:1px;
        }
    </style>
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">List Tour Styles
                <a href="{{url('admin/tourstyle/create')}}" class="btn btn-primary pull-right">Create New Tour Style</a></h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        @if(Session::has('flash'))
            <div class="alert alert-success text-center" id="status">
                {{Session::get('flash')}}
            </div>
        @endif

        <?php $a = 1 ?>
        <table class="table table-striped table-bordered">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th style="width: 20%">Description</th>
                <th  style="width: 10%">Image</th>
                <th>Status</th>
                <th>Action</th>
            </tr>

            @foreach($tourstyles as $tourstyle)
                <div class="col-md-6">

                    <tr>
                        <td><?= $a ?></td>
                        <td>{{$tourstyle->name}}</td>

                        <td style="width:50%"> {!!$tourstyle->description!!}</td>

                        <td><img src="{{asset('images/tourstyle/'.$tourstyle->image)}}"  class="img-responsive img"></td>

                        <td>{{$tourstyle->status}}</td>

                        <td><a href="{{url('admin/tourstyle/'.$tourstyle->id.'/edit')}}" class="btn btn-default"><i class="fa fa-edit" title=""></i></a>
                            <form action={{url('admin/tourstyle/'.$tourstyle->id)}} method="POST">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="DELETE">
                                <!--<input type="submit" value="Delete">-->
                                <button class="btn btn-danger" value="Delete" onclick=" return confirm('are you sure to delete??')"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>

                    <?php $a = 1 + $a ?>
                </div>
            @endforeach
        </table>
    </div>

@stop