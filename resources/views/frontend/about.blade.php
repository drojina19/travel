@extends('frontend.baselayout')
@section('main-section')

    <div class="inner-banner style-6">
        <img class="center-image" src="{{asset('frontend/img/inner/bg_5.jpg')}}" alt="">
        <div class="vertical-align">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <ul class="banner-breadcrumb color-white clearfix">
                            <li><a class="link-blue-2" href="{{asset('/')}}">home</a> /</li>
                            <li><span>About</span></li>
                        </ul>
                        <p class="color-white-light">About us</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-wraper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <div class="second-title">
                        <h4 class="subtitle color-dr-blue-2 underline">about us</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h3>About us?</h3>
                    <br>
                    <div class="text-justify">{{$about->description}}</div>
                </div>

                <div class="col-md-6">
                    <img class="img-responsive img-full" src="{{asset('images/components/'.$about->image)}}" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="main-wraper bg-grey-2 padd-90">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <div class="second-title">
                        <h4 class="subtitle color-dr-blue-2 underline">our Articles</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="swiper-container" data-autoplay="0" data-loop="0" data-speed="1000" data-slides-per-view="responsive" data-mob-slides="1" data-xs-slides="2" data-sm-slides="2" data-md-slides="3" data-lg-slides="4" data-add-slides="4">
                    <div class="swiper-wrapper">
                        @foreach( $articles as $article)
                        <div class="swiper-slide">
                            <div class="icon-block style-2 bg-white">
                                <img class="icon-img bg-dr-blue-2 border-grey-2" src="{{asset('images/article/'.$article->image)}}" alt="" style="height: 100px;width:100px;">
                                <h5 class="icon-title color-dark-2">{{ $article->title }}</h5>
                                <div class="icon-text color-dark-2-light">{{ str_limit($article->description, 100) }}.</div>
                                <a href="{{url("/article/".$article->type)}}" class="c-button small bg-dr-blue-2 hv-dr-blue-2-o"><span>view more</span></a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="pagination poin-style-2"></div>
                </div>
            </div>
        </div>
    </div>


@stop