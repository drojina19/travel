@extends('frontend.baselayout')
@section('main-section')



<div class="inner-banner style-5">
    <img class="center-image" src="{{asset('frontend/img/detail/bg_5.jpg')}}" alt="{{config('app.name')}}">
    <div class="vertical-align">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <ul class="banner-breadcrumb color-white clearfix">
                        <li><a class="link-blue-2" href="#">home</a> /</li>
                        <li><a class="link-blue-2" href="#">article </a> / </li>
                        <li><span >{{$type}}</span></li>

                    </ul>

                    <h2 class="color-white">{{$type}}</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="detail-wrapper">
    <div class="container">
        <div class="row padd-90">
            <div class="col-xs-12 col-md-8">
                <div class="blog-list">
                    @foreach($articles as $article)
                    <div class="blog-list-entry">
                        <div class="blog-list-top">
                        <img class="img-responsive" src="{{asset('images/article/'.$article->image)}}" alt="{{config('app.name')}}">
                        </div>
                        <h4 class="blog-list-title"><a class="color-dark-2 link-dr-blue-2" href="#">{{$article->title}}</a></h4>
                        <div class="tour-info-line clearfix">
                            <div class="tour-info fl">
                                <img src="img/calendar_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">{{\Carbon\Carbon::parse($article->date)->format('d-m-Y')}}</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/people_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">By {{$article->posted_by}}</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/comment_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">10 comments</span>
                            </div>
                        </div>
                        <div class="blog-list-text color-grey-3 text-justify">{!! $article->description !!}</div>
                        <a href="#" class="c-button small bg-dr-blue-2 hv-dr-blue-2-o"><span>read more</span></a>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="right-sidebar">
                    {{--<div class="sidebar-block type-2">--}}
                        {{--<div class="widget-search clearfix">--}}
                            {{--<form>--}}
                                {{--<div class="input-style-1 b-50 brd-0 type-2 color-3">--}}
                                    {{--<input type="text" placeholder="Enter what you want to find">--}}
                                {{--</div>--}}
                                {{--<input class="widget-submit" type="submit" value="">--}}
                            {{--</form>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="sidebar-block type-2">
                        <h4 class="sidebar-title color-dark-2">categories</h4>
                        <ul class="sidebar-category color-5">
                            <li>
                                <a href="#">all <span class="fr">(125)</span></a>
                            </li>
                            @foreach($articles as $article)
                            <li>
                                <a href="#">{{$article->type}} <span class="fr">(26)</span></a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    {{--<div class="sidebar-block type-2">--}}
                        {{--<h4 class="sidebar-title color-dark-2">popular posts</h4>--}}
                        {{--<div class="widget-popular">--}}
                            {{--<div class="hotel-small style-2 clearfix">--}}
                                {{--<a class="hotel-img black-hover" href="#">--}}
                                    {{--<img class="img-responsive radius-0" src="img/home_7/small_hotel_5.jpg" alt="">--}}
                                    {{--<div class="tour-layer delay-1"></div>--}}
                                {{--</a>--}}
                                {{--<div class="hotel-desc">--}}
                                    {{--<div class="tour-info-line">--}}
                                        {{--<div class="tour-info">--}}
                                            {{--<img src="img/calendar_icon_grey.png" alt="">--}}
                                            {{--<span class="font-style-2 color-dark-2">03/07/2015</span>--}}
                                        {{--</div>--}}
                                        {{--<div class="tour-info">--}}
                                            {{--<img src="img/people_icon_grey.png" alt="">--}}
                                            {{--<span class="font-style-2 color-dark-2">By Emma Stone</span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<h4>history of mauritius</h4>--}}
                                    {{--<div class="tour-info-line clearfix">--}}
                                        {{--<div class="tour-info">--}}
                                            {{--<img src="img/comment_icon_grey.png" alt="">--}}
                                            {{--<span class="font-style-2 color-dark-2">10 comments</span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="hotel-small style-2 clearfix">--}}
                                {{--<a class="hotel-img black-hover" href="#">--}}
                                    {{--<img class="img-responsive radius-0" src="img/home_7/small_hotel_6.jpg" alt="">--}}
                                    {{--<div class="tour-layer delay-1"></div>--}}
                                {{--</a>--}}
                                {{--<div class="hotel-desc">--}}
                                    {{--<div class="tour-info-line">--}}
                                        {{--<div class="tour-info">--}}
                                            {{--<img src="img/calendar_icon_grey.png" alt="">--}}
                                            {{--<span class="font-style-2 color-dark-2">03/07/2015</span>--}}
                                        {{--</div>--}}
                                        {{--<div class="tour-info">--}}
                                            {{--<img src="img/people_icon_grey.png" alt="">--}}
                                            {{--<span class="font-style-2 color-dark-2">By Emma Stone</span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<h4>mauritius from 5 days</h4>--}}
                                    {{--<div class="tour-info-line clearfix">--}}
                                        {{--<div class="tour-info">--}}
                                            {{--<img src="img/comment_icon_grey.png" alt="">--}}
                                            {{--<span class="font-style-2 color-dark-2">10 comments</span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="hotel-small style-2 clearfix">--}}
                                {{--<a class="hotel-img black-hover" href="#">--}}
                                    {{--<img class="img-responsive radius-0" src="img/home_7/small_hotel_7.jpg" alt="">--}}
                                    {{--<div class="tour-layer delay-1"></div>--}}
                                {{--</a>--}}
                                {{--<div class="hotel-desc">--}}
                                    {{--<div class="tour-info-line">--}}
                                        {{--<div class="tour-info">--}}
                                            {{--<img src="img/calendar_icon_grey.png" alt="">--}}
                                            {{--<span class="font-style-2 color-dark-2">03/07/2015</span>--}}
                                        {{--</div>--}}
                                        {{--<div class="tour-info">--}}
                                            {{--<img src="img/people_icon_grey.png" alt="">--}}
                                            {{--<span class="font-style-2 color-dark-2">By Emma Stone</span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<h4>mauritius from 5 days</h4>--}}
                                    {{--<div class="tour-info-line clearfix">--}}
                                        {{--<div class="tour-info">--}}
                                            {{--<img src="img/comment_icon_grey.png" alt="">--}}
                                            {{--<span class="font-style-2 color-dark-2">10 comments</span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="sidebar-block type-2">--}}
                        {{--<div class="simple-tab tab-3 color-1 tab-wrapper">--}}
                            {{--<div class="tab-nav-wrapper">--}}
                                {{--<div class="nav-tab  clearfix">--}}
                                    {{--<div class="nav-tab-item active">--}}
                                        {{--commented--}}
                                    {{--</div>--}}
                                    {{--<div class="nav-tab-item">--}}
                                        {{--popular--}}
                                    {{--</div>--}}
                                    {{--<div class="nav-tab-item">--}}
                                        {{--new--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="tabs-content clearfix">--}}
                                {{--<div class="tab-info active">--}}
                                    {{--<div class="hotel-small style-2 clearfix">--}}
                                        {{--<a class="hotel-img black-hover" href="#">--}}
                                            {{--<img class="img-responsive radius-0" src="img/home_9/cruise_1.jpg" alt="">--}}
                                            {{--<div class="tour-layer delay-1"></div>--}}
                                        {{--</a>--}}
                                        {{--<div class="hotel-desc">--}}
                                            {{--<div class="tour-info-line">--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/calendar_icon_grey.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-dark-2">03/07/2015</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<h4>cruises reviews</h4>--}}
                                            {{--<div class="tour-info-line clearfix">--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/people_icon_grey.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-dark-2">By Emma Stone</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="hotel-small style-2 clearfix">--}}
                                        {{--<a class="hotel-img black-hover" href="#">--}}
                                            {{--<img class="img-responsive radius-0" src="img/detail/popular_1.jpg" alt="">--}}
                                            {{--<div class="tour-layer delay-1"></div>--}}
                                        {{--</a>--}}
                                        {{--<div class="hotel-desc">--}}
                                            {{--<div class="tour-info-line">--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/calendar_icon_grey.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-dark-2">03/07/2015</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<h4>cruises reviews</h4>--}}
                                            {{--<div class="tour-info-line clearfix">--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/people_icon_grey.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-dark-2">By Emma Stone</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="hotel-small style-2 clearfix">--}}
                                        {{--<a class="hotel-img black-hover" href="#">--}}
                                            {{--<img class="img-responsive radius-0" src="img/detail/popular_2.jpg" alt="">--}}
                                            {{--<div class="tour-layer delay-1"></div>--}}
                                        {{--</a>--}}
                                        {{--<div class="hotel-desc">--}}
                                            {{--<div class="tour-info-line">--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/calendar_icon_grey.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-dark-2">03/07/2015</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<h4>cruises reviews</h4>--}}
                                            {{--<div class="tour-info-line clearfix">--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/people_icon_grey.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-dark-2">By Emma Stone</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="tab-info">--}}
                                    {{--<div class="hotel-small style-2 clearfix">--}}
                                        {{--<a class="hotel-img black-hover" href="#">--}}
                                            {{--<img class="img-responsive radius-0" src="img/detail/popular_2.jpg" alt="">--}}
                                            {{--<div class="tour-layer delay-1"></div>--}}
                                        {{--</a>--}}
                                        {{--<div class="hotel-desc">--}}
                                            {{--<div class="tour-info-line">--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/calendar_icon_grey.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-dark-2">03/07/2015</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<h4>cruises reviews</h4>--}}
                                            {{--<div class="tour-info-line clearfix">--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/people_icon_grey.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-dark-2">By Emma Stone</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="hotel-small style-2 clearfix">--}}
                                        {{--<a class="hotel-img black-hover" href="#">--}}
                                            {{--<img class="img-responsive radius-0" src="img/home_9/cruise_1.jpg" alt="">--}}
                                            {{--<div class="tour-layer delay-1"></div>--}}
                                        {{--</a>--}}
                                        {{--<div class="hotel-desc">--}}
                                            {{--<div class="tour-info-line">--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/calendar_icon_grey.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-dark-2">03/07/2015</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<h4>cruises reviews</h4>--}}
                                            {{--<div class="tour-info-line clearfix">--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/people_icon_grey.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-dark-2">By Emma Stone</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="hotel-small style-2 clearfix">--}}
                                        {{--<a class="hotel-img black-hover" href="#">--}}
                                            {{--<img class="img-responsive radius-0" src="img/detail/popular_1.jpg" alt="">--}}
                                            {{--<div class="tour-layer delay-1"></div>--}}
                                        {{--</a>--}}
                                        {{--<div class="hotel-desc">--}}
                                            {{--<div class="tour-info-line">--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/calendar_icon_grey.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-dark-2">03/07/2015</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<h4>cruises reviews</h4>--}}
                                            {{--<div class="tour-info-line clearfix">--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/people_icon_grey.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-dark-2">By Emma Stone</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="tab-info">--}}
                                    {{--<div class="hotel-small style-2 clearfix">--}}
                                        {{--<a class="hotel-img black-hover" href="#">--}}
                                            {{--<img class="img-responsive radius-0" src="img/detail/popular_1.jpg" alt="">--}}
                                            {{--<div class="tour-layer delay-1"></div>--}}
                                        {{--</a>--}}
                                        {{--<div class="hotel-desc">--}}
                                            {{--<div class="tour-info-line">--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/calendar_icon_grey.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-dark-2">03/07/2015</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<h4>cruises reviews</h4>--}}
                                            {{--<div class="tour-info-line clearfix">--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/people_icon_grey.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-dark-2">By Emma Stone</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="hotel-small style-2 clearfix">--}}
                                        {{--<a class="hotel-img black-hover" href="#">--}}
                                            {{--<img class="img-responsive radius-0" src="img/home_9/cruise_1.jpg" alt="">--}}
                                            {{--<div class="tour-layer delay-1"></div>--}}
                                        {{--</a>--}}
                                        {{--<div class="hotel-desc">--}}
                                            {{--<div class="tour-info-line">--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/calendar_icon_grey.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-dark-2">03/07/2015</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<h4>cruises reviews</h4>--}}
                                            {{--<div class="tour-info-line clearfix">--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/people_icon_grey.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-dark-2">By Emma Stone</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="hotel-small style-2 clearfix">--}}
                                        {{--<a class="hotel-img black-hover" href="#">--}}
                                            {{--<img class="img-responsive radius-0" src="img/detail/popular_2.jpg" alt="">--}}
                                            {{--<div class="tour-layer delay-1"></div>--}}
                                        {{--</a>--}}
                                        {{--<div class="hotel-desc">--}}
                                            {{--<div class="tour-info-line">--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/calendar_icon_grey.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-dark-2">03/07/2015</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<h4>cruises reviews</h4>--}}
                                            {{--<div class="tour-info-line clearfix">--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/people_icon_grey.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-dark-2">By Emma Stone</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="sidebar-block type-2">--}}
                        {{--<div class="widget-slider arrows">--}}
                            {{--<div class="swiper-container" data-autoplay="0" data-loop="1" data-speed="900" data-center="0" data-slides-per-view="1">--}}
                                {{--<div class="swiper-wrapper">--}}
                                    {{--<div class="swiper-slide radius-4 active" data-val="0">--}}
                                        {{--<img class="center-image" src="img/detail/widget_s.jpg" alt="">--}}
                                        {{--<div class="vertical-bottom">--}}
                                            {{--<h4 class="color-white">best hotels reviews</h4>--}}
                                            {{--<div class="tour-info-line clearfix">--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/calendar_icon.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-white">03/07/2015</span>--}}
                                                {{--</div>--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/people_icon.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-white">By Emma Stone</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="swiper-slide radius-4" data-val="1">--}}
                                        {{--<img class="center-image" src="img/home_9/f_slide.jpg" alt="">--}}
                                        {{--<div class="vertical-bottom">--}}
                                            {{--<h4 class="color-white">royal Hotel</h4>--}}
                                            {{--<div class="tour-info-line clearfix">--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/calendar_icon.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-white">03/07/2015</span>--}}
                                                {{--</div>--}}
                                                {{--<div class="tour-info">--}}
                                                    {{--<img src="img/people_icon.png" alt="">--}}
                                                    {{--<span class="font-style-2 color-white">By Emma Stone</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="pagination pagination-hidden poin-style-1"></div>--}}
                                {{--<div class="arr-t-3">--}}
                                    {{--<div class="swiper-arrow-left sw-arrow"><span class="fa fa-angle-left"></span></div>--}}
                                    {{--<div class="swiper-arrow-right sw-arrow"><span class="fa fa-angle-right"></span></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="sidebar-block type-2">--}}
                        {{--<h4 class="sidebar-title color-dark-2">latest comments</h4>--}}
                        {{--<div class="widget-comment">--}}
                            {{--<div class="w-comment-entry">--}}
                                {{--<div class="w-comment-date"><img src="img/calendar_icon_grey.png" alt=""> july <strong>19th 2015</strong></div>--}}
                                {{--<div class="w-comment-title color-grey-3"><a class="color-dark-2" href="#">BEST HOTELS REVIEWS</a> by <span class="color-dark-2">Emma Stone</span></div>--}}
                                {{--<div class="w-comment-text color-grey-3">Lorem ipsum dolor sit amet, eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>--}}
                            {{--</div>--}}
                            {{--<div class="w-comment-entry">--}}
                                {{--<div class="w-comment-date"><img src="img/calendar_icon_grey.png" alt=""> july <strong>21th 2015</strong></div>--}}
                                {{--<div class="w-comment-title color-grey-3"><a class="color-dark-2" href="#">TOP BEST HOTELS AND TOURS</a> by <span class="color-dark-2">Emma Stone</span></div>--}}
                                {{--<div class="w-comment-text color-grey-3">Lorem ipsum dolor sit amet, eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>--}}
                            {{--</div>--}}
                            {{--<div class="w-comment-entry">--}}
                                {{--<div class="w-comment-date"><img src="img/calendar_icon_grey.png" alt=""> july <strong>29th 2015</strong></div>--}}
                                {{--<div class="w-comment-title color-grey-3"><a class="color-dark-2" href="#">TOP BEST HOTELS AND TOURS</a> by <span class="color-dark-2">Emma Stone</span></div>--}}
                                {{--<div class="w-comment-text color-grey-3">Lorem ipsum dolor sit amet, eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="sidebar-block type-2">--}}
                        {{--<h4 class="sidebar-title color-dark-2">popular tags</h4>--}}
                        {{--<ul class="widget-tags clearfix">--}}
                            {{--<li><a class="c-button b-30 b-1 bg-grey-2 hv-dr-blue-2" href="#">flights</a></li>--}}
                            {{--<li><a class="c-button b-30 b-1 bg-grey-2 hv-dr-blue-2" href="#">travelling</a></li>--}}
                            {{--<li><a class="c-button b-30 b-1 bg-grey-2 hv-dr-blue-2" href="#">Sale</a></li>--}}
                            {{--<li><a class="c-button b-30 b-1 bg-grey-2 hv-dr-blue-2" href="#">cruises</a></li>--}}
                            {{--<li><a class="c-button b-30 b-1 bg-grey-2 hv-dr-blue-2" href="#">Sale</a></li>--}}
                            {{--<li><a class="c-button b-30 b-1 bg-grey-2 hv-dr-blue-2" href="#">travelling</a></li>--}}
                            {{--<li><a class="c-button b-30 b-1 bg-grey-2 hv-dr-blue-2" href="#">travelling</a></li>--}}
                            {{--<li><a class="c-button b-30 b-1 bg-grey-2 hv-dr-blue-2" href="#">Illegal</a></li>--}}
                            {{--<li><a class="c-button b-30 b-1 bg-grey-2 hv-dr-blue-2" href="#">flights</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                    {{--<div class="sidebar-block type-2">--}}
                        {{--<h4 class="sidebar-title color-dark-2">popular tags</h4>--}}
                        {{--<div class="widget-gallery clearfix">--}}
                            {{--<a href="#"><img class="img-responsive" src="img/detail/w_gal_1.jpg" alt=""></a>--}}
                            {{--<a href="#"><img class="img-responsive" src="img/detail/w_gal_2.jpg" alt=""></a>--}}
                            {{--<a href="#"><img class="img-responsive" src="img/detail/w_gal_3.jpg" alt=""></a>--}}
                            {{--<a href="#"><img class="img-responsive" src="img/detail/w_gal_4.jpg" alt=""></a>--}}
                            {{--<a href="#"><img class="img-responsive" src="img/detail/w_gal_5.jpg" alt=""></a>--}}
                            {{--<a href="#"><img class="img-responsive" src="img/detail/w_gal_6.jpg" alt=""></a>--}}
                            {{--<a href="#"><img class="img-responsive" src="img/detail/w_gal_7.jpg" alt=""></a>--}}
                            {{--<a href="#"><img class="img-responsive" src="img/detail/w_gal_8.jpg" alt=""></a>--}}
                            {{--<a href="#"><img class="img-responsive" src="img/detail/w_gal_9.jpg" alt=""></a>--}}
                            {{--<a href="#"><img class="img-responsive" src="img/detail/w_gal_10.jpg" alt=""></a>--}}
                            {{--<a href="#"><img class="img-responsive" src="img/detail/w_gal_11.jpg" alt=""></a>--}}
                            {{--<a href="#"><img class="img-responsive" src="img/detail/w_gal_12.jpg" alt=""></a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
</div>









@stop
