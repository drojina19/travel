@extends('frontend.baselayout')
@section('main-section')

    <div class="inner-banner style-5">
        <img class="center-image" src="{{asset('frontend/img/inner/bg_3.jpg')}}" alt="">
        <div class="vertical-align">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <ul class="banner-breadcrumb color-white clearfix">
                            <li><a class="link-blue-2" href="{{asset('/')}}">home</a> /</li>
                            <li><a class="link-blue-2" href="{{asset('/about')}}">about</a> /</li>

                            <li><span>Contact</span></li>
                        </ul>
                        <h2 class="color-white">Get in Touch</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="detail-wrapper">

        <div class="main-wraper padd-40">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-8">
                        <form class="contact-form js-contact-form" action="{{url('/contact')}}" method="POST">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="input-style-1 type-2 color-2">
                                        <input type="text" name="name" required="" placeholder="Enter your name">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="input-style-1 type-2 color-2">
                                        <input type="email" name="email" required="" placeholder="Enter your email">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="input-style-1 type-2 color-2">
                                        <input type="text" name="phone" required="" placeholder="Enter your phone number">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="drop-wrap drop-wrap-s-3 color-2">
                                        <div class="drop">
                                            <b>Enquiry Type</b>
                                            <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
                                                <span>

                                                <a href="#">03 kids</a>
                                                <a href="#">04 kids</a>
                                                <a href="#">05 kids</a>
                                                </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <textarea class="area-style-1 color-1" name="message" required="" placeholder="Enter your message"></textarea>
                                    <button type="submit" class="c-button bg-dr-blue-2 hv-dr-blue-2-o"><span>Submit</span></button>
                                </div>

                            </div>
                        </form>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="contact-about">
                            <h4 class="color-dark-2"><strong>Contact Info</strong></h4>
                            <p class="color-grey-3">Donec gravida euismod felis, quis dictum justo scelerisque in. Aenean nec lacus ipsum. Suspendisse vel lobortis libero, eu imperdiet purus. Aenean nec lacus ipsum.</p>
                        </div>
                        <div class="contact-info">
                            <h4 class="color-dark-2"><strong>contact info</strong></h4>
                            <div class="contact-line color-grey-3"><img src="{{asset('frontend/img/phone_icon_2_dark.png')}}" alt="">Phone: <a class="color-dark-2" href="tel:93123456789">+93 123 456 789</a></div>
                            <div class="contact-line color-grey-3"><img src="{{asset('frontend/img/mail_icon_b_dark.png')}}" alt="">Email us: <a class="color-dark-2 tt" href="#">let’<span class="__cf_email__" data-cfemail="aad9f5ded8cbdccfc6eaddc5d8c6ce84c9c5c7">[email&#160;protected]</span></a></div>
                            <div class="contact-line color-grey-3"><img src="{{asset('frontend/img/loc_icon_dark.png')}}" alt="">Address: <span class="color-dark-2 tt">Aenean vulputate porttitor</span></div>
                        </div>
                        <div class="contact-socail">
                            <a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-facebook"></i></a>
                            <a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-twitter"></i></a>
                            <a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-skype"></i></a>
                            <a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-google-plus"></i></a>
                            <a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-pinterest-p"></i></a>
                            <a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-instagram"></i></a>
                            <a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-behance"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="map-block">
        <div id="map-canvas" class="style-2" data-lat="33.664467" data-lng="-117.601586" data-zoom="10" data-style="2"></div>
        <div class="addresses-block">
            <a data-lat="33.664467" data-lng="-117.601586" data-string="Santa Monica Hotel"></a>
        </div>
    </div>


@stop


