@extends('frontend.baselayout')
@section('main-section')



<div class="inner-banner style-6">
    <img class="center-image" src="{{url('frontend/img/inner/bg_1.jpg')}}" alt="{{config('app.name')}}">
    <div class="vertical-align">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <ul class="banner-breadcrumb color-white clearfix">
                    <li><a class="link-blue-2" href="#">home</a> /</li>
                    <li><a class="link-blue-2" href="#">country</a> /</li>
                    <li><span>{{$country->name}}</span></li>
                    </ul>
                    <h2 class="color-white">{{$country->name}} </h2>

                </div>
            </div>
        </div>
    </div>
</div>




<div class="detail-wrapper">
    <div class="container">
        <div class="row padd-90">
            <div class="col-xs-12 col-md-8">

                <div class="detail-content  text-justify">
                    <div class="detail-content-block">
                        <div class="embed-responsive embed-responsive-16by9">
                         <img src="{{asset('images/country/'.$country->image)}}">
                        </div>
                        <h3>General Information About {{$country->name}}</h3>
                        <p>{!! $country-> description!!}</p>

                    </div>
                </div>

            </div>

            <div class="col-xs-12 col-md-4">

                <div class="right-sidebar">

                            <div class="sidebar-block">


                                <div class="sidebar-block">
                                    <h4 class="sidebar-title color-dark-2"> other popular destinations</h4>
                                    <div class="widget-popular">
                                        @foreach($countries->where('id','!=', $country->id) as $coun)


                                        <div class="hotel-small style-2 clearfix">
                                            <a class="hotel-img black-hover" href="{{url('/destination/'.Illuminate\Support\Str::slug($coun->name))}}">
                                                <img class="img-responsive radius-0" src="{{asset('images/country/'.$coun->image)}}" alt="">
                                                <div class="tour-layer delay-1"></div>
                                            </a>
                                            <div class="hotel-desc">

                                                <h4>{{$coun->name}}</h4>

                                            </div>
                                        </div>

                                        @endforeach

                                    </div>
                                </div>


                            </div>
                        </div>

            </div>
        </div>
    </div>

</div>

<div class="main-wraper bg-grey-2 padd-90">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="second-title">
                    <h4 class="subtitle color-dr-blue-2 underline">our most popular tour</h4>
                    <h2>we proposed to you</h2>
                </div>
            </div>
        </div>
        <div class="grid-content clearfix">
            @foreach($tours as $tour)
                <div class="list-item-entry">
                    <div class="hotel-item style-9 bg-white">
                        <div class="table-view">
                            <div class="radius-top cell-view">
                                <img src="{{url('images/tour/'.$tour->image)}}" alt="{{config('app.name')}}">
                                <div class="price price-s-4">${{$tour->price}}</div>
                            </div>
                            <div class="title hotel-middle cell-view">
                                <div class="tour-info-line clearfix">
                                    <div class="tour-info fl">
                                        <img src="img/calendar_icon_grey.png" alt="">
                                        <span class="font-style-2 color-grey-3"> <strong>{{$tour->duration}} days</strong></span>
                                    </div>
                                    <div class="tour-info fl">
                                        <img src="img/loc_icon_small_grey.png" alt="">
                                        <span class="font-style-2 color-grey-3">{{$tour->country->name}}</span>
                                    </div>
                                </div>
                                <h4><b>{{$tour->name}}</b></h4>
                                <div class="rate-wrap list-hidden">
                                <div class="rate">
                                <span class="fa fa-star color-yellow"></span>
                                <span class="fa fa-star color-yellow"></span>
                                <span class="fa fa-star color-yellow"></span>
                                <span class="fa fa-star color-yellow"></span>
                                <span class="fa fa-star color-yellow"></span>
                                </div>
                                <i>485 rewies</i>
                                </div>
                                <p class="f-14 color-grey-3">{!! $tour->description !!}</p>

                                <div class="buttons-block bg-dr-blue-2">
                                    <a href="#" class="c-button b-40 bg-grey-3-t hv-grey-3-t b-1">detail</a>
                                    <a href="#" class="c-button b-40 bg-white hv-transparent fr">book now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            @endforeach

        </div>

</div>
</div>
<div class="main-wraper padd-120">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="second-title">
                    <h2>what our client say about us</h2>
                    {{--<p class="color-grey">Curabitur nunc erat, consequat in erat ut, congue bibendum nulla.</p>--}}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="top-baner arrows">
                <div class="swiper-container offers-slider" data-autoplay="5000" data-loop="1" data-speed="500" data-slides-per-view="responsive" data-mob-slides="1" data-xs-slides="2" data-sm-slides="2" data-md-slides="3" data-lg-slides="4" data-add-slides="4">
                    <div class="swiper-wrapper">
                        @foreach($testimonials as $testimonial)
                        <div class="swiper-slide" data-val="0">
                            <div class="offers-block style-2 radius-mask underline-block hover-blue">
                                <div class="clip">
                                    <div class="bg bg-bg-chrome act" >
                                        <img src="{{asset('images/testemonial/'.$testimonial->image)}}">
                                    </div>
                                </div>
                                <div class="tour-layer delay-1"></div>

                                <div class="vertical-bottom">

                                    <h4 class="offet-title underline hover-it m94">{{$testimonial->title}}</h4>
                                    <p>{!! $testimonial->description!!}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        {{--<div class="swiper-slide" data-val="1">--}}
                            {{--<div class="offers-block style-2 radius-mask underline-block hover-blue">--}}
                                {{--<div class="clip">--}}
                                    {{--<div class="bg bg-bg-chrome act" style="background-image:url(frontend/img/home_4/offers-block_2.jpg)"></div>--}}
                                {{--</div>--}}
                                {{--<div class="tour-layer delay-1"></div>--}}
                                {{--<div class="vertical-top">--}}
                                    {{--<div class="rate">--}}
                                        {{--<span class="fa fa-heart color-dr-blue-2 hover-it"></span>--}}
                                        {{--<span class="fa fa-heart color-dr-blue-2 hover-it"></span>--}}
                                        {{--<span class="fa fa-heart color-dr-blue-2 hover-it"></span>--}}
                                        {{--<span class="fa fa-heart color-dr-blue-2 hover-it"></span>--}}
                                        {{--<span class="fa fa-heart color-dr-blue-2 hover-it"></span>--}}
                                    {{--</div>--}}
                                    {{--<h3 class="hover-it">cruise tour</h3>--}}
                                {{--</div>--}}
                                {{--<div class="vertical-bottom">--}}
                                    {{--<div class="tour-info">--}}
                                        {{--<img src="img/calendar_icon.png" alt="">--}}
                                        {{--<span class="font-style-2">03/07/2015</span>--}}
                                    {{--</div>--}}
                                    {{--<h4 class="offet-title underline hover-it m94">anna, mark</h4>--}}
                                    {{--<p>Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam.</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="swiper-slide" data-val="2">--}}
                            {{--<div class="offers-block style-2 radius-mask underline-block hover-blue">--}}
                                {{--<div class="clip">--}}
                                    {{--<div class="bg bg-bg-chrome act" style="background-image:url(frontend/img/home_4/offers-block_3.jpg)"></div>--}}
                                {{--</div>--}}
                                {{--<div class="tour-layer delay-1"></div>--}}
                                {{--<div class="vertical-top">--}}
                                    {{--<div class="rate">--}}
                                        {{--<span class="fa fa-heart color-dr-blue-2 hover-it"></span>--}}
                                        {{--<span class="fa fa-heart color-dr-blue-2 hover-it"></span>--}}
                                        {{--<span class="fa fa-heart color-dr-blue-2 hover-it"></span>--}}
                                        {{--<span class="fa fa-heart color-dr-blue-2 hover-it"></span>--}}
                                        {{--<span class="fa fa-heart color-dr-blue-2 hover-it"></span>--}}
                                    {{--</div>--}}
                                    {{--<h3 class="hover-it">food tour</h3>--}}
                                {{--</div>--}}
                                {{--<div class="vertical-bottom">--}}
                                    {{--<div class="tour-info">--}}
                                        {{--<img src="img/calendar_icon.png" alt="">--}}
                                        {{--<span class="font-style-2">03/07/2015</span>--}}
                                    {{--</div>--}}
                                    {{--<h4 class="offet-title underline hover-it m94">elison lloyds</h4>--}}
                                    {{--<p>Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam.</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="swiper-slide" data-val="4">--}}
                            {{--<div class="offers-block style-2 radius-mask underline-block hover-blue">--}}
                                {{--<div class="clip">--}}
                                    {{--<div class="bg bg-bg-chrome act" style="background-image:url(frontend/img/home_4/offers-block_4.jpg)"></div>--}}
                                {{--</div>--}}
                                {{--<div class="tour-layer delay-1"></div>--}}
                                {{--<div class="vertical-top">--}}
                                    {{--<div class="rate">--}}
                                        {{--<span class="fa fa-heart color-dr-blue-2 hover-ite"></span>--}}
                                        {{--<span class="fa fa-heart color-dr-blue-2 hover-it"></span>--}}
                                        {{--<span class="fa fa-heart color-dr-blue-2 hover-it"></span>--}}
                                        {{--<span class="fa fa-heart color-dr-blue-2 hover-it"></span>--}}
                                        {{--<span class="fa fa-heart color-dr-blue-2 hover-it"></span>--}}
                                    {{--</div>--}}
                                    {{--<h3 class="hover-it">sea tour</h3>--}}
                                {{--</div>--}}
                                {{--<div class="vertical-bottom">--}}
                                    {{--<div class="tour-info">--}}
                                        {{--<img src="{{asset('frontend/img/calendar_icon.png')}}" alt="">--}}
                                        {{--<span class="font-style-2">03/07/2015</span>--}}
                                    {{--</div>--}}
                                    {{--<h4 class="offet-title underline hover-it m94">elison lloyds</h4>--}}
                                    {{--<p>Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam.</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>
                    <div class="pagination  poin-style-1 pagination-hidden"></div>
                </div>
                <div class="swiper-arrow-left offers-arrow color-2"><span class="fa fa-angle-left"></span>
                </div>
                <div class="swiper-arrow-right offers-arrow color-2"><span class="fa fa-angle-right"></span>
                </div>
            </div>
        </div>
    </div>
</div>

@stop