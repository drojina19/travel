@extends('frontend.baselayout')
@section('main-section')


    <div class="inner-banner style-5">
        <img class="center-image" src="{{asset('frontend/img/detail/bg_5.jpg')}}" alt="">
        <div class="vertical-align">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <ul class="banner-breadcrumb color-white clearfix">
                            <li><a class="link-blue-2" href="{{asset('/')}}">home</a> /</li>
                            <li><span>faq</span></li>
                        </ul>
                        <h2 class="color-white">Informations</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="main-wraper bg-dr-blue-2 padd-90">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <div class="second-title">
                        <h2 class="color-white">Geneneral Faqs:</h2>
                    </div>
                </div>
            </div>
            <div class="accordion style-6">
                @foreach($faqs as $faqs)
                <div class="acc-panel">
                    <div class="acc-title"><span class="acc-icon"></span>{{$faqs->title}}</div>
                    <div class="acc-body">
                        <div class="text-justify">{{$faqs->description}}</div>
                    </div>
                </div>
                @endforeach


            </div>
        </div>
    </div>
    <div class="main-wraper bg-grey-2 padd-90">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <div class="second-title">
                        <h4 class="color-dark-2-light">testimonials</h4>
                        <h2>what our clients say</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="top-baner arrows">
                    <div class="swiper-container" data-autoplay="0" data-loop="1" data-speed="500" data-slides-per-view="responsive" data-mob-slides="1" data-xs-slides="2" data-sm-slides="2" data-md-slides="3" data-lg-slides="4" data-add-slides="4">
                        <div class="swiper-wrapper">
                            @foreach($testemonials as $test)
                            <div class="swiper-slide">
                                <div class="tour-item style-3">
                                    <div class="radius-top">
                                        <img src="{{asset('images/testemonial/'.$test->image)}}" alt="">
                                        <div class="tour-weather">sea tour</div>
                                    </div>
                                    <div class="tour-desc bg-white">
                                        <div class="rate">
                                            <span class="fa fa-heart color-green"></span>
                                            <span class="fa fa-heart color-green"></span>
                                            <span class="fa fa-heart color-green"></span>
                                            <span class="fa fa-heart color-green"></span>
                                            <span class="fa fa-heart color-green"></span>
                                        </div>

                                        <h4><a class="tour-title color-dark-2 link-green" href="#">{{ $test->title }}</a></h4>
                                        <div class="tour-text color-grey-3 text-justify">{!! $test->description !!}</div>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>
                        <div class="pagination  poin-style-1 pagination-hidden"></div>
                    </div>
                    <div class="swiper-arrow-left offers-arrow color-3"><span class="fa fa-angle-left"></span></div>
                    <div class="swiper-arrow-right offers-arrow color-3"><span class="fa fa-angle-right"></span></div>
                </div>
            </div>
        </div>
    </div>

@stop