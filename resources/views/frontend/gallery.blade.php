@extends('frontend.baselayout')
@section('main-section')


    <div class="inner-banner style-6">
        <img class="center-image" src="{{asset('frontend/img/gallery/bg_1.jpg')}}" alt="">
        <div class="vertical-align">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <ul class="banner-breadcrumb color-white clearfix">
                            <li><a class="link-blue-2" href="{{asset('/')}}">home</a> /</li>
                            <li><a class="link-blue-2" href="{{asset('/about')}}">about</a> /</li>
                            <li><span>Gallery</span></li>
                        </ul>
                        <h2 class="color-white">Gallery</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--<div class="main-wraper padd-70-70">--}}
        {{--<div class="container">--}}
            {{--<div class="filter style-2">--}}
                {{--<ul class="filter-nav">--}}
                    {{--<li class="selected"><a href="#all" data-filter="*">all</a></li>--}}
                    {{--@foreach($tours as $tour)--}}
                    {{--<li><a href="{{url('/themelist/'.Illuminate\Support\Str::slug($tour->theme->name))}}" data-filter=".tours">{{$tour->theme->name}}</a></li>--}}
                    {{--@endforeach--}}

                {{--</ul>--}}
            {{--</div>--}}
            {{--<div class="filter-content row">--}}
                {{--<div class="grid-sizer col-mob-12 col-xs-6 col-sm-4"></div>--}}
                {{--@foreach($galleries as $gallery)--}}
                    {{--<div class="item tours gal-item col-mob-12 col-xs-6 col-sm-4">--}}
                        {{--<a class="black-hover" href="">--}}
                            {{--<img class="img-full img-responsive" src="{{asset('images/gallery/'.$gallery->image)}}" alt="{{config('app.name')}}">--}}
                            {{--<div class="tour-layer delay-1"></div>--}}
                            {{--<div class="vertical-align">--}}
                                {{--<h3 class="color-white"><b>{{$gallery->tour->name}}</b></h3>--}}
                                {{--<h5 class="color-white">{{$gallery->tour->country->name}}</h5>--}}
                            {{--</div>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                {{--@endforeach--}}


            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}


    <div class="main-wraper padd-70-70">
        <div class="container">
            <div class="filter style-2">
                <ul class="filter-nav">
                    <li class="selected"><a href="#all" data-filter="*">all</a>
                    </li>
                    @foreach($themes as $theme)
                    <li><a href="#" data-filter=".{{$theme->name}}">{{$theme->name}}</a>
                    </li>

                     @endforeach

                </ul>
            </div>

            <div class="filter-content row">
                <div class="grid-sizer col-mob-12 col-xs-6 col-sm-4"></div>
                @foreach($themes as $theme)
                @foreach($theme->tours as $tour)
                <div class="item col-mob-12 col-xs-6 col-sm-4">
                    <a class="black-hover" href="{{url('/themelist/'.Illuminate\Support\Str::slug($theme->name))}}">
                        <img class="img-full img-responsive" src="{{asset('images/tour/'.$tour->image)}}" alt="{{config('app.name')}}">
                        <div class="delay-1"></div>
                        <div class="vertical-align">
                            <h3 class="color-white"><b style="margin-left: 10px;">{{$tour->theme->name}}</b></h3>
                            <h5 class="color-white">{{$tour->name}}</h5>
                        </div>
                    </a>
                </div>
                @endforeach
                @endforeach

            </div>
        </div>
    </div>

@stop
