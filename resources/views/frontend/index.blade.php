@extends('frontend.baselayout')
@section('main-section')
    <div class="top-baner swiper-animate arrows" style="top:30px">
        <div class="swiper-container main-slider" data-autoplay="5000" data-loop="1" data-speed="900" data-center="0" data-slides-per-view="1">
            <div class="swiper-wrapper">
                @foreach($sliders as $slider)
                    <div class="swiper-slide active" data-val="0">
                        <div class="clip">
                            <div class="bg bg-bg-chrome act" style="background-image:url({{asset('images/slider/'.$slider->image)}})">
                            </div>
                        </div>
                        <div class="vertical-align">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="main-title vert-title">
                                            <p>{{$slider->slider_title1}}</p>
                                            <h1 class="color-white delay-1">{!! $slider->slider_title2 !!}</h1>
                                            <p class="color-white-op delay-2">{!! $slider->slider_desc !!}</p>
                                            {{--<a href="#" class="c-button bg-aqua hv-transparent delay-2"><span>view our tours</span></a>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
            <div class="pagination pagination-hidden poin-style-1"></div>
        </div>
        <div class="arrow-wrapp m-200">
            <div class="cont-1170">
                <div class="swiper-arrow-left sw-arrow"><span class="fa fa-angle-left"></span></div>
                <div class="swiper-arrow-right sw-arrow"><span class="fa fa-angle-right"></span></div>
            </div>
        </div>
        <div class="baner-tabs">
            <div class="tab-content tpl-tabs-cont section-text t-con-style-1">
                <div class="tab-pane active in" id="one">
                    <div class="container">
                        <div class="row" class="pull-center">
                            <form action="{{url('/advance-search')}}" method="get">
                                {{csrf_field()}}
                            <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="form-block clearfix">
                                <div class="form-label color-white">Your Destination</div>
                                    <div class="drop-wrap drop-wrap-s-3" id="dropdown">

                                        <select name="country">

                                            <option value="disabled selected">Choose where do you wanna travel </option>

                                            @foreach($countries as $country)
                                                <option value="{{$country->name}}">{{ucfirst($country->name)}}</option>
                                            @endforeach
                                        </select>


                                    </div>
                                </div>
                            </div>
                                <div class="col-xs-12 col-sm-3">
                                    <div class="form-block clearfix">
                                        <div class="form-label color-white">Themes</div>
                                        <div class="drop-wrap drop-wrap-s-3" id="dropdown">
                                            <select name="theme">
                                                <option value="disabled selected">Choose your appropriate theme</option>

                                                @foreach($themes as $theme)
                                                    <option value="{{$theme->name}}">{{ucfirst($theme->name)}}</option>

                                                 @endforeach

                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-3">
                                    <div class="form-block clearfix">
                                        <div class="form-label color-white"> Price</div>
                                        <div class="drop-wrap drop-wrap-s-3" id="dropdown">
                                            <select name="price">


                                                <option value="disabled selected">Choose Your Apppropriate Price</option>


                                                @foreach($tours as $t)
                                                  <option value="{{$t->price}}">{{ucfirst($t->price)}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                </div>
                            <div class="col-lg-2 col-md-6 col-sm-3 col-xs-12">
                                <button type="submit" class="c-button b-60 bg-aqua hv-transparent" style="margin-top: 40px;"><i class="fa fa-search"></i><span>search now</span></button>
                            </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-wraper padd-90" id="dest">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="second-title">
                        <h2>Best Destination</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($tours->take(6) as $tour)
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="radius-mask tour-block hover-aqua">
                        <div class="clip">
                            <div class="bg bg-bg-chrome act" style="background-image:url({{asset('images/tour/'.$tour->image)}})">
                            </div>
                        </div>
                        <div class="tour-layer delay-1"></div>
                        <div class="tour-caption">
                            <div class="vertical-align">
                                <h3 class="hover-it">{{$tour->country->name}}</h3>

                                <h4>from $<b>{{$tour->price}}</b></h4>
                            </div>
                            <div class="vertical-bottom">
                                <div class="fl">
                                    <div class="tour-info">
                                        <span class="font-style-2 color-grey-4"><strong class="color-white"><h4>{{$tour->duration}} days</h4></strong></span>
                                    </div>
                                </div>
                                <a href="#" class="c-button b-50 bg-aqua hv-transparent fr"><span>view more</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    {{--<div class="main-wraper">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    {{--<div class="second-title">--}}
                        {{--<h2>Special Offers</h2>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row">--}}
                {{--<div class="top-baner arrows">--}}
                    {{--<div class="swiper-container offers-slider" data-autoplay="5000" data-loop="1" data-speed="500" data-slides-per-view="responsive" data-mob-slides="1" data-xs-slides="2" data-sm-slides="2" data-md-slides="3" data-lg-slides="3" data-add-slides="3">--}}
                        {{--<div class="swiper-wrapper">--}}
                            {{--@foreach($tours as $tour)--}}
                            {{--<div class="swiper-slide active" data-val="0">--}}
                                {{--<div class="offers-block radius-mask">--}}
                                    {{--<div class="clip">--}}
                                        {{--<div class="bg bg-bg-chrome act" style="background-image:url({{asset('images/tour/'.$tour->image)}})">--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="tour-layer delay-1"></div>--}}
                                    {{--<div class="vertical-top">--}}

                                        {{--<h3>{{ $tour->country->name }}</h3>--}}
                                    {{--</div>--}}
                                    {{--<div class="vertical-bottom">--}}
                                        {{--<ul class="offers-info">--}}
                                            {{--<li> {{ $tour->name }}</li>--}}
                                            {{--<li>$ {{ $tour->price }}</li>--}}
                                            {{--<li>{{ $tour->duration }}</li>--}}
                                        {{--</ul>--}}
                                        {{--<p style="color:white;">{!!str_limit( $tour->description) !!}</p>--}}
                                        {{--<a href="#" class="c-button bg-aqua hv-aqua-o b-40"><span>view more</span></a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--@endforeach--}}
                        {{--</div>--}}
                        {{--<div class="pagination  poin-style-1 pagination-hidden"></div>--}}
                    {{--</div>--}}
                    {{--<div class="swiper-arrow-left offers-arrow"><span class="fa fa-angle-left"></span></div>--}}
                    {{--<div class="swiper-arrow-right offers-arrow"><span class="fa fa-angle-right"></span></div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="main-wraper padd-90">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    {{--<div class="second-title">--}}
                        {{--<h2>Most Popular Travel Countries</h2>--}}
                        {{--<p class="color-grey">Curabitur nunc erat, consequat in erat ut, congue bibendum nulla.</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row">--}}
                {{--<div class="col-xs-12 col-md-6 col-md-push-6 col-sm-12">--}}
                    {{--<div class="popular-desc text-left">--}}
                        {{--<div class="clip">--}}
                            {{--<div class="bg bg-bg-chrome act bg-contain" style="background-image:url(img/home_1/map_1.png)">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="vertical-align">--}}
                            {{--<h3>italy, europe</h3>--}}
                            {{--<p class="color-grey">Curabitur nunc erat, consequat in erat ut, congue bibendum nulla. Suspendisse id pharetra lacus, et hendrerit mi Praesent at vestibulum tortor. Praesent condimentum efficitur massa, nec congue sem dapibus sed. </p>--}}
                            {{--<h4>best cities</h4>--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                                    {{--<ul>--}}
                                        {{--<li><a href="#">Rome</a></li>--}}
                                        {{--<li><a href="#">Venice</a></li>--}}
                                        {{--<li><a href="#">Pisa</a></li>--}}
                                        {{--<li><a href="#">Naples</a></li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                                    {{--<ul>--}}
                                        {{--<li><a href="#">Bologna</a></li>--}}
                                        {{--<li><a href="#">Florence</a></li>--}}
                                        {{--<li><a href="#">Genoa</a></li>--}}
                                        {{--<li><a href="#">Turin</a></li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                                    {{--<ul>--}}
                                        {{--<li><a href="#">Milan</a></li>--}}
                                        {{--<li><a href="#">Capri</a></li>--}}
                                        {{--<li><a href="#">Matera</a></li>--}}
                                        {{--<li><a href="#">Pompeii</a></li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<a href="#" class="c-button bg-aqua hv-transparent b-50 custom-icon">--}}
                                {{--<img class="img-hide" src="img/flag_icon.png" alt="">--}}
                                {{--<img class="img-hov" src="img/flag_icon_aqua.png" alt="">--}}
                                {{--<span>view all places</span></a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-xs-12 col-md-6 col-md-pull-6 col-sm-12">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-6 col-sm-6 col-xs-12">--}}
                            {{--<div class="radius-mask popular-img">--}}
                                {{--<div class="clip">--}}
                                    {{--<div class="bg bg-bg-chrome act" style="background-image:url(img/home_1/popular_travel_img_1.jpg)">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="tour-layer delay-1"></div>--}}
                                {{--<div class="vertical-bottom">--}}
                                    {{--<h4><a href="#">Maecenas sit amet</a></h4>--}}
                                    {{--<h5><b class="color-aqua">from $235</b> per person</h5>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6 col-sm-6 col-xs-12">--}}
                            {{--<div class="radius-mask popular-img">--}}
                                {{--<div class="clip">--}}
                                    {{--<div class="bg bg-bg-chrome act" style="background-image:url(img/home_1/popular_travel_img_2.jpg)">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="tour-layer delay-1"></div>--}}
                                {{--<div class="vertical-bottom">--}}
                                    {{--<h4><a href="#">Aenean iaculis enim</a></h4>--}}
                                    {{--<h5><b class="color-aqua">from $180</b> per person</h5>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6 col-sm-6 col-xs-12">--}}
                            {{--<div class="radius-mask popular-img">--}}
                                {{--<div class="clip">--}}
                                    {{--<div class="bg bg-bg-chrome act" style="background-image:url(img/home_1/popular_travel_img_3.jpg)">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="tour-layer delay-1"></div>--}}
                                {{--<div class="vertical-bottom">--}}
                                    {{--<h4><a href="#">Pellentesque tempus</a></h4>--}}
                                    {{--<h5><b class="color-aqua">from $195</b> per person</h5>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6 col-sm-6 col-xs-12">--}}
                            {{--<div class="radius-mask popular-img">--}}
                                {{--<div class="clip">--}}
                                    {{--<div class="bg bg-bg-chrome act" style="background-image:url(img/home_1/popular_travel_img_4.jpg)">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="tour-layer delay-1"></div>--}}
                                {{--<div class="vertical-bottom">--}}
                                    {{--<h4><a href="#">Donec id maximus</a></h4>--}}
                                    {{--<h5><b class="color-aqua">from $350</b> per person</h5>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row">--}}
                {{--<div class="col-xs-12 col-md-6 col-sm-12">--}}
                    {{--<div class="popular-desc text-right">--}}
                        {{--<div class="clip">--}}
                            {{--<div class="bg bg-bg-chrome act bg-contain" style="background-image:url(img/home_1/map_2.png)">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="vertical-align">--}}
                            {{--<h3>france, europe</h3>--}}
                            {{--<p class="color-grey">Curabitur nunc erat, consequat in erat ut, congue bibendum nulla. Suspendisse id pharetra lacus, et hendrerit mi Praesent at vestibulum tortor. Praesent condimentum efficitur massa, nec congue sem dapibus sed. </p>--}}
                            {{--<h4>best cities</h4>--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                                    {{--<ul>--}}
                                        {{--<li><a href="#">Rome</a></li>--}}
                                        {{--<li><a href="#">Venice</a></li>--}}
                                        {{--<li><a href="#">Pisa</a></li>--}}
                                        {{--<li><a href="#">Naples</a></li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                                    {{--<ul>--}}
                                        {{--<li><a href="#">Bologna</a></li>--}}
                                        {{--<li><a href="#">Florence</a></li>--}}
                                        {{--<li><a href="#">Genoa</a></li>--}}
                                        {{--<li><a href="#">Turin</a></li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                                    {{--<ul>--}}
                                        {{--<li><a href="#">Milan</a></li>--}}
                                        {{--<li><a href="#">Capri</a></li>--}}
                                        {{--<li><a href="#">Matera</a></li>--}}
                                        {{--<li><a href="#">Pompeii</a></li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<a href="#" class="c-button bg-aqua hv-transparent b-50 custom-icon">--}}
                                {{--<img class="img-hide" src="img/flag_icon.png" alt="">--}}
                                {{--<img class="img-hov" src="img/flag_icon_aqua.png" alt="">--}}
                                {{--<span>view all places</span>--}}
                            {{--</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-xs-12 col-md-6 col-sm-12">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-6 col-sm-6 col-xs-12">--}}
                            {{--<div class="radius-mask popular-img">--}}
                                {{--<div class="clip">--}}
                                    {{--<div class="bg bg-bg-chrome act" style="background-image:url(img/home_1/popular_travel_img_5.jpg)">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="tour-layer delay-1"></div>--}}
                                {{--<div class="vertical-bottom">--}}
                                    {{--<h4><a href="#">Donec id maximus</a></h4>--}}
                                    {{--<h5><b class="color-aqua">from $215</b> per person</h5>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6 col-sm-6 col-xs-12">--}}
                            {{--<div class="radius-mask popular-img">--}}
                                {{--<div class="clip">--}}
                                    {{--<div class="bg bg-bg-chrome act" style="background-image:url(img/home_1/popular_travel_img_6.jpg)">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="tour-layer delay-1"></div>--}}
                                {{--<div class="vertical-bottom">--}}
                                    {{--<h4><a href="#">Pellentesque tempus</a></h4>--}}
                                    {{--<h5><b class="color-aqua">from $175</b> per person</h5>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6 col-sm-6 col-xs-12">--}}
                            {{--<div class="radius-mask popular-img">--}}
                                {{--<div class="clip">--}}
                                    {{--<div class="bg bg-bg-chrome act" style="background-image:url(img/home_1/popular_travel_img_7.jpg)">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="tour-layer delay-1"></div>--}}
                                {{--<div class="vertical-bottom">--}}
                                    {{--<h4><a href="#">Aenean iaculis enim</a></h4>--}}
                                    {{--<h5><b class="color-aqua">from $150</b> per person</h5>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6 col-sm-6 col-xs-12">--}}
                            {{--<div class="radius-mask popular-img">--}}
                                {{--<div class="clip">--}}
                                    {{--<div class="bg bg-bg-chrome act" style="background-image:url(img/home_1/popular_travel_img_8.jpg)">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="tour-layer delay-1"></div>--}}
                                {{--<div class="vertical-bottom">--}}
                                    {{--<h4><a href="#">Maecenas sit amet</a></h4>--}}
                                    {{--<h5><b class="color-aqua">from $290</b> per person</h5>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}



    <div class="main-wraper padd-120">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <div class="second-title">
                        <h2>Latest Articles</h2>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="top-baner arrows">
                    <div class="swiper-container offers-slider" data-autoplay="5000" data-loop="1" data-speed="500" data-slides-per-view="responsive" data-mob-slides="1" data-xs-slides="2" data-sm-slides="2" data-md-slides="3" data-lg-slides="4" data-add-slides="4">
                        <div class="swiper-wrapper">

                            @foreach($articles as $article)
                            <div class="swiper-slide" data-val="0">
                                <div class="offers-block style-2 radius-mask underline-block hover-blue">
                                    <div class="clip">
                                        <div class="bg bg-bg-chrome act" style="background-image:url('images/article/{{$article->image}}')"></div>
                                    </div>
                                    <div class="tour-layer delay-1"></div>
                                    <div class="vertical-top">

                                        <h3 class="hover-it">{{$article->type}}</h3>
                                    </div>
                                    <div class="vertical-bottom">
                                        <div class="tour-info">
                                            <img src="img/calendar_icon.png" alt="">
                                            <span class="font-style-2">{{\Carbon\Carbon::parse($article->date)->format('d-m-Y')}}</span>
                                        </div>
                                        <h4 class="offet-title underline hover-it m94"></h4>
                                        <p>{!! str_limit($article->description) !!}</p>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>
                        <div class="pagination  poin-style-1 pagination-hidden"></div>
                    </div>
                    <div class="swiper-arrow-left offers-arrow color-2"><span class="fa fa-angle-left"></span>
                    </div>
                    <div class="swiper-arrow-right offers-arrow color-2"><span class="fa fa-angle-right"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
