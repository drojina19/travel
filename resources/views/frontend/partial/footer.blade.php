<footer class="bg-dark type-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="footer-block">
                    <img src="{{asset('images/logo.png')}}" alt="" class="logo-footer" style="width: 100%">
                    {{--<div class="f_text color-grey-7">{{}}</div>--}}
                    <div class="footer-share">
                        <a href="#"><span class="fa fa-facebook"></span></a>
                        <a href="#"><span class="fa fa-twitter"></span></a>
                        <a href="#"><span class="fa fa-google-plus"></span></a>
                        <a href="#"><span class="fa fa-pinterest"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-sm-6 no-padding">
                <div class="footer-block">
                    <h6>Travel News</h6>
                    @foreach($articles->take(3) as $article)
                    <div class="f_news clearfix">
                        <a class="f_news-img black-hover" href="{{url('/about/gallery')}}">
                            <img class="img-responsive" src="{{asset('images/article/'.$article->image)}}" alt="">
                            <div class="tour-layer delay-1"></div>
                        </a>
                        <div class="f_news-content">
                            <a class="f_news-tilte color-white link-red" href="{{url('/article/'.Illuminate\Support\Str::slug($article->type))}}">Astoninshing {{$article->type}}</a>

                            <span class="date-f">{{\Carbon\Carbon::parse($article->date)->format('d-m-Y')}}</span>
                            <a href="{{url('/article/'.Illuminate\Support\Str::slug($article->type))}}" class="r-more">read more</a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="footer-block">
                    <h6>Tags</h6>
                    @foreach($themes as $theme)
                        <a href="{{url('/themelist/'.Illuminate\Support\Str::slug($theme->name))}}" class="tags-b">{{ucfirst($theme->name)}}</a>
                    @endforeach

                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="footer-block">
                    <h6>Contact Info</h6>
                    <div class="contact-info">
                        <div class="contact-line color-grey-3"><i class="fa fa-map-marker"></i><span>Sydney, Australia</span></div>
                        <div class="contact-line color-grey-3"><i class="fa fa-phone"></i><a href="">12334455</a></div>
                        <div class="contact-line color-grey-3"><i class="fa fa-envelope-o"></i><a href="#"><span class="__cf_email__" data-cfemail="a8c4cddcdbdcdac9decdc4e8c5c9c1c486cbc7c5">admin@travel.com</span></a></div>
                        <div class="contact-line color-grey-3"><i class="fa fa-globe"></i><a href="#"><span class="__cf_email__" data-cfemail="25567a51574453404965524a5749410b464a48">info@travel.com.au</span></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-link bg-black">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <span>&copy;All rights reserved ,Genius IT Soultion</span>
                    </div>
                    <ul>



                        <li><a class="link-aqua" href="{{url('/')}}">Home</a></li>
                        <li><a class="link-aqua" href="{{url('/about')}}">About Us</a></li>
                        <li><a class="link-aqua" href="{{url('/about/faq')}}">Faq(s)</a></li>
                        <li><a class="link-aqua" href="{{url('/article')}}">Article</a></li>


                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>