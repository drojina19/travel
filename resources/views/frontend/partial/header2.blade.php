
<header class="color-1 hovered menu-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="nav">
                    <a href="{{url('/')}}" class="logo">
                        <img src="img/theme-1/logo_dark.png" alt="{{config('app.name')}}">
                    </a>
                    <div class="nav-menu-icon">
                        <a href="#"><i></i></a>
                    </div>
                    <nav class="menu">
                        <ul>
                            {{--<li class="type-1 active">--}}
                                {{--<a href="{{url('/top-destination')}}">Destination<span class="fa fa-angle-down"></span></a>--}}
                                {{--<ul class="dropmenu">--}}

                                    {{--@foreach($countries as $country)--}}
                                        {{--<li><a href="{{url('/destination/'.\Illuminate\Support\Str::slug($country->name) )}}">{{ucfirst($country->name)}}  </a></li>--}}
                                    {{--@endforeach()--}}

                                {{--</ul>--}}
                            {{--</li>--}}
                            <li class="type-1"><a href="#">Themes<span class="fa fa-angle-down"></span></a>
                                <ul class="dropmenu">
                                    @foreach($themes as $theme)
                                        <li><a href="{{url('/theme/'.\Illuminate\Support\Str::slug($theme->name))}}">{{ucfirst($theme->name)}}  </a></li>
                                    @endforeach

                                </ul>
                            </li>

                            <li class="type-1"><a href="#">Travel Deals<span class="fa fa-angle-down"></span></a>
                                <ul class="dropmenu">
                                    <li><a href="car_list.html">List View</a></li>
                                    <li><a href="car_grid.html">Grid View</a></li>
                                    <li><a href="car_block.html">Block View</a></li>

                                </ul>
                            </li>
                            <li class="type-1"><a href="#">Articles<span class="fa fa-angle-down"></span></a>
                                <ul class="dropmenu">
                                    @foreach($articles as $article)
                                        <li><a href="{{url('/article/'.\Illuminate\Support\Str::slug($article->type))}}">{{ucfirst($article->type)}}  </a></li>
                                    @endforeach

                                </ul>
                            </li>
                            <li class="type-1"><a href="#">Travel Deals<span class="fa fa-angle-down"></span></a>
                                <ul class="dropmenu">
                                    <li><a href="car_list.html">List View</a></li>
                                    <li><a href="car_grid.html">Grid View</a></li>
                                    <li><a href="car_block.html">Block View</a></li>

                                </ul>
                            </li>

                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>