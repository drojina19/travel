@extends('frontend.baselayout')
@section('main-section')


    <div class="inner-banner style-5">
        <img class="center-image" src="{{asset('frontend/img/tour_list/inner_banner_1.jpg')}}" alt="{{config('app.name')}}">
        <div class="vertical-align">
            <div class="container">
                <ul class="banner-breadcrumb color-white clearfix">
                    <li><a class="link-blue-2" href="#">home</a> /</li>
                    <li><a class="link-blue-2" href="#"> search tours</a> /</li>
                    <li><span>List tours </span></li>
                </ul>
            </div>
        </div>
    </div>


    <div>
        <div class="container">
            <ul class="list-breadcrumb clearfix">
                <li><a class="color-grey link-dr-blue" href="">home</a> /</li>
                <li><a class="color-grey link-dr-blue" href="">SEARCH</a> /</li>
                <li><span class="color-dr-blue">TOURS</span>
                </li>
            </ul>
             <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-3">
            <div class="sidebar style-2 clearfix">
                <div >
                    <h4 class="sidebar-title color-dark-2">search</h4>
                    <div>
                        <div class="searchFilter">
                            <div>

                                @foreach($countries as $country)
                                    <input type="radio" name="country" value="{{$country->name}}"
                                        <?php if($country->name==$c['country']) echo "checked"; ?>
                                        >
                                    {{$country->name}}<br>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-block clearfix searchFilter">
                            <div>


                                @foreach($themes as $theme)
                                <input type="text"  name="theme" value="{{$theme->name}}" style="padding-top: 10px"

                                <?php if($theme->name==$c['theme']) echo "checked"; ?>
                                >
                                   {{$theme->name}}
                                @endforeach
                            </div>
                        </div>
                        {{--<div class="form-block clearfix searchFilter">--}}
                            {{--<div class="input-style-1 b-50 color-4">--}}
                                {{--<img src="{{asset('frontend/img/plane.png')}}" alt="">--}}
                                {{--<input type="text" placeholder="Which tour you will prefer?" >--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="form-block clearfix searchFilter">
                            <div class="input-style-1 b-50 color-4">
                                <img src="{{asset('frontend/img/comment_icon_grey.png')}}" alt="">
                                {{--@foreach($tours as $t)--}}

                                    {{--<input type="text" value="{{$t->price}}">--}}
                                {{--@endforeach--}}


                            </div>
                        </div>
                    </div>
                    <input type="submit" class="c-button b-40 bg-dr-blue hv-dr-blue-o" value="search">
                </div>


            </div>
        </div>
        <div class="col-xs-12 col-sm-8 col-md-9">
            {{--<div class="list-header clearfix">--}}
                {{--<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">--}}
                    {{--<div class="drop">--}}
                        {{--<b>Sort by price</b>--}}
                        {{--<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>--}}
								{{--<span>--}}
                            {{--<a href="#">ASC</a>--}}
                                    {{--<a href="#">DESC</a></span>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">--}}
                    {{--<div class="drop">--}}
                        {{--<b>Sort by ranking</b>--}}
                        {{--<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>--}}
								{{--<span>--}}
{{--<a href="#">ASC</a>--}}
{{--<a href="#">DESC</a>--}}
{{--</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="list-view-change">--}}
                    {{--<div class="change-grid color-2 fr"><i class="fa fa-th"></i>--}}
                    {{--</div>--}}
                    {{--<div class="change-list color-2 fr active"><i class="fa fa-bars"></i>--}}
                    {{--</div>--}}
                    {{--<div class="change-to-label fr color-grey-8">View:</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="list-content clearfix">
                @foreach($t as $tour)
                <div class="list-item-entry">
                    <div class="hotel-item style-8 bg-white">
                        <div class="table-view">
                            <div class="radius-top cell-view">
                                <img src="{{asset('images/tour/'.$tour->image)}}" alt="{{config('app.name')}}">
                                <div class="price price-s-3 red tt">hot price</div>
                            </div>
                            <div class="title hotel-middle clearfix cell-view text-justify">
                                <div class="hotel-person color-dark-2 list-hidden">from <span>${{$tour->price}}</span>
                                </div>
                                <div class="rate-wrap">
                                    <div class="rate">
                                        <span class="fa fa-star color-yellow"></span>
                                        <span class="fa fa-star color-yellow"></span>
                                        <span class="fa fa-star color-yellow"></span>
                                        <span class="fa fa-star color-yellow"></span>
                                        <span class="fa fa-star color-yellow"></span>
                                    </div>
                                    <i>485 rewies</i>
                                </div>
                                <h4><b>{{$tour->name}}</b></h4>
                                <span class="f-14 color-dark-2 grid-hidden"></span>
                                <p class="f-14">{!! $tour->description !!}</p>
                                <div class="hotel-icons-block grid-hidden">
                                    <img class="hotel-icon" src="img/tour_list/hotel_icon_1.png" alt="">
                                    <img class="hotel-icon" src="img/tour_list/hotel_icon_2.png" alt="">
                                    <img class="hotel-icon" src="img/tour_list/hotel_icon_3.png" alt="">
                                    <img class="hotel-icon" src="img/tour_list/hotel_icon_4.png" alt="">
                                    <img class="hotel-icon" src="img/tour_list/hotel_icon_5.png" alt="">
                                </div>
                                <a href="#" class="c-button bg-dr-blue hv-dr-blue-o b-40 fl list-hidden">select</a>
                                <a href="#" class="c-button color-dr-blue hv-o b-40 fr list-hidden">
                                    <img src="img/loc_icon_small_drak.png" alt="">view on map</a>
                            </div>
                            <div class="title hotel-right bg-dr-blue clearfix cell-view">
                                <div class="hotel-person color-white">from <span>${{$tour->price}}</span>
                                </div>
                                <a class="c-button b-40 bg-white color-dark-2 hv-dark-2-o grid-hidden" href="#">view more</a>
                            </div>
                        </div>
                    </div>
                </div>
                 @endforeach

            </div>
            <div class="c_pagination clearfix padd-120">
                <a href="#" class="c-button b-40 bg-dr-blue hv-dr-blue-o fl">prev page</a>
                <a href="#" class="c-button b-40 bg-dr-blue hv-dr-blue-o fr">next page</a>
                <ul class="cp_content color-2">
                    <li class="active"><a href="#">1</a>
                    </li>
                    <li><a href="#">2</a>
                    </li>
                    <li><a href="#">3</a>
                    </li>
                    <li><a href="#">4</a>
                    </li>
                    <li><a href="#">...</a>
                    </li>
                    <li><a href="#">10</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
        </div>

    </div>



 @stop