@extends('frontend.baselayout')
@section('main-section')
    <div class="inner-banner style-5">
        <img class="center-image" src="img/inner/bg_6.jpg" alt="">
        <div class="vertical-align">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <ul class="banner-breadcrumb color-white clearfix">
                            <li><a class="link-blue-2" href="#">home</a> /</li>
                            <li><span>blog</span></li>
                        </ul>
                        <h2 class="color-white">blog</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-md-12">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

        </div>
    </div>
    <div class="main-wraper padd-120">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <div class="second-title">
                    </div>
                </div>
            </div>
            <div class="blog-grid row">
                <div class="blog-grid-entry col-mob-12 col-xs-12 col-sm-6 col-md-4">
                    <div class="s_news-entry style-2">
                        <a href="blog_detail.html"><img class="s_news-img img-responsive" src="img/inner/blog_grid_1.jpg" alt=""></a>
                        <h4 class="s_news-title"><a href="blog_detail.html">Lorem ipsum dolor</a></h4>
                        <div class="tour-info-line clearfix">
                            <div class="tour-info fl">
                                <img src="img/calendar_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">03/07/2015</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/people_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">By Emma Stone</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/comment_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">10 comments</span>
                            </div>
                        </div>
                        <div class="s_news-text color-grey-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        <a href="blog_detail.html" class="c-button small bg-dr-blue-2 hv-dr-blue-2-o"><span>view more</span></a>
                    </div>
                </div>
                <div class="blog-grid-entry col-mob-12 col-xs-12 col-sm-6 col-md-4">
                    <div class="s_news-entry style-2">
                        <a href="blog_detail.html"><img class="s_news-img img-responsive" src="img/inner/blog_grid_2.jpg" alt=""></a>
                        <h4 class="s_news-title"><a href="blog_detail.html">Lorem ipsum dolor</a></h4>
                        <div class="tour-info-line clearfix">
                            <div class="tour-info fl">
                                <img src="img/calendar_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">03/07/2015</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/people_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">By Emma Stone</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/comment_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">10 comments</span>
                            </div>
                        </div>
                        <div class="s_news-text color-grey-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        <a href="blog_detail.html" class="c-button small bg-dr-blue-2 hv-dr-blue-2-o"><span>view more</span></a>
                    </div>
                </div>
                <div class="blog-grid-entry col-mob-12 col-xs-12 col-sm-6 col-md-4">
                    <div class="s_news-entry style-2">
                        <a href="blog_detail.html"><img class="s_news-img img-responsive" src="img/inner/blog_grid_3.jpg" alt=""></a>
                        <h4 class="s_news-title"><a href="blog_detail.html">Lorem ipsum dolor</a></h4>
                        <div class="tour-info-line clearfix">
                            <div class="tour-info fl">
                                <img src="img/calendar_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">03/07/2015</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/people_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">By Emma Stone</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/comment_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">10 comments</span>
                            </div>
                        </div>
                        <div class="s_news-text color-grey-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        <a href="blog_detail.html" class="c-button small bg-dr-blue-2 hv-dr-blue-2-o"><span>view more</span></a>
                    </div>
                </div>
                <div class="blog-grid-entry col-mob-12 col-xs-12 col-sm-6 col-md-4">
                    <div class="s_news-entry style-2">
                        <a href="blog_detail.html"><img class="s_news-img img-responsive" src="img/inner/blog_grid_4.jpg" alt=""></a>
                        <h4 class="s_news-title"><a href="blog_detail.html">Lorem ipsum dolor</a></h4>
                        <div class="tour-info-line clearfix">
                            <div class="tour-info fl">
                                <img src="img/calendar_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">03/07/2015</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/people_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">By Emma Stone</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/comment_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">10 comments</span>
                            </div>
                        </div>
                        <div class="s_news-text color-grey-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        <a href="blog_detail.html" class="c-button small bg-dr-blue-2 hv-dr-blue-2-o"><span>view more</span></a>
                    </div>
                </div>
                <div class="blog-grid-entry col-mob-12 col-xs-12 col-sm-6 col-md-4">
                    <div class="s_news-entry style-2">
                        <a href="blog_detail.html"><img class="s_news-img img-responsive" src="img/inner/blog_grid_5.jpg" alt=""></a>
                        <h4 class="s_news-title"><a href="blog_detail.html">Lorem ipsum dolor</a></h4>
                        <div class="tour-info-line clearfix">
                            <div class="tour-info fl">
                                <img src="img/calendar_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">03/07/2015</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/people_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">By Emma Stone</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/comment_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">10 comments</span>
                            </div>
                        </div>
                        <div class="s_news-text color-grey-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        <a href="blog_detail.html" class="c-button small bg-dr-blue-2 hv-dr-blue-2-o"><span>view more</span></a>
                    </div>
                </div>
                <div class="blog-grid-entry col-mob-12 col-xs-12 col-sm-6 col-md-4">
                    <div class="s_news-entry style-2">
                        <a href="blog_detail.html"><img class="s_news-img img-responsive" src="img/inner/blog_grid_6.jpg" alt=""></a>
                        <h4 class="s_news-title"><a href="blog_detail.html">Lorem ipsum dolor</a></h4>
                        <div class="tour-info-line clearfix">
                            <div class="tour-info fl">
                                <img src="img/calendar_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">03/07/2015</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/people_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">By Emma Stone</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/comment_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">10 comments</span>
                            </div>
                        </div>
                        <div class="s_news-text color-grey-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        <a href="blog_detail.html" class="c-button small bg-dr-blue-2 hv-dr-blue-2-o"><span>view more</span></a>
                    </div>
                </div>
                <div class="blog-grid-entry col-mob-12 col-xs-12 col-sm-6 col-md-4">
                    <div class="s_news-entry style-2">
                        <a href="blog_detail.html"><img class="s_news-img img-responsive" src="img/inner/blog_grid_7.jpg" alt=""></a>
                        <h4 class="s_news-title"><a href="blog_detail.html">Lorem ipsum dolor</a></h4>
                        <div class="tour-info-line clearfix">
                            <div class="tour-info fl">
                                <img src="img/calendar_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">03/07/2015</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/people_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">By Emma Stone</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/comment_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">10 comments</span>
                            </div>
                        </div>
                        <div class="s_news-text color-grey-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        <a href="blog_detail.html" class="c-button small bg-dr-blue-2 hv-dr-blue-2-o"><span>view more</span></a>
                    </div>
                </div>
                <div class="blog-grid-entry col-mob-12 col-xs-12 col-sm-6 col-md-4">
                    <div class="s_news-entry style-2">
                        <a href="blog_detail.html"><img class="s_news-img img-responsive" src="img/inner/blog_grid_8.jpg" alt=""></a>
                        <h4 class="s_news-title"><a href="blog_detail.html">Lorem ipsum dolor</a></h4>
                        <div class="tour-info-line clearfix">
                            <div class="tour-info fl">
                                <img src="img/calendar_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">03/07/2015</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/people_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">By Emma Stone</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/comment_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">10 comments</span>
                            </div>
                        </div>
                        <div class="s_news-text color-grey-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        <a href="blog_detail.html" class="c-button small bg-dr-blue-2 hv-dr-blue-2-o"><span>view more</span></a>
                    </div>
                </div>
                <div class="blog-grid-entry col-mob-12 col-xs-12 col-sm-6 col-md-4">
                    <div class="s_news-entry style-2">
                        <a href="blog_detail.html"><img class="s_news-img img-responsive" src="img/inner/blog_grid_9.jpg" alt=""></a>
                        <h4 class="s_news-title"><a href="blog_detail.html">Lorem ipsum dolor</a></h4>
                        <div class="tour-info-line clearfix">
                            <div class="tour-info fl">
                                <img src="img/calendar_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">03/07/2015</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/people_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">By Emma Stone</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/comment_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">10 comments</span>
                            </div>
                        </div>
                        <div class="s_news-text color-grey-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        <a href="blog_detail.html" class="c-button small bg-dr-blue-2 hv-dr-blue-2-o"><span>view more</span></a>
                    </div>
                </div>
                <div class="blog-grid-entry col-mob-12 col-xs-12 col-sm-6 col-md-4">
                    <div class="s_news-entry style-2">
                        <a href="blog_detail.html"><img class="s_news-img img-responsive" src="img/inner/blog_grid_10.jpg" alt=""></a>
                        <h4 class="s_news-title"><a href="blog_detail.html">Lorem ipsum dolor</a></h4>
                        <div class="tour-info-line clearfix">
                            <div class="tour-info fl">
                                <img src="img/calendar_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">03/07/2015</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/people_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">By Emma Stone</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/comment_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">10 comments</span>
                            </div>
                        </div>
                        <div class="s_news-text color-grey-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        <a href="blog_detail.html" class="c-button small bg-dr-blue-2 hv-dr-blue-2-o"><span>view more</span></a>
                    </div>
                </div>
                <div class="blog-grid-entry col-mob-12 col-xs-12 col-sm-6 col-md-4">
                    <div class="s_news-entry style-2">
                        <a href="blog_detail.html"><img class="s_news-img img-responsive" src="img/inner/blog_grid_11.jpg" alt=""></a>
                        <h4 class="s_news-title"><a href="blog_detail.html">Lorem ipsum dolor</a></h4>
                        <div class="tour-info-line clearfix">
                            <div class="tour-info fl">
                                <img src="img/calendar_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">03/07/2015</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/people_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">By Emma Stone</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/comment_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">10 comments</span>
                            </div>
                        </div>
                        <div class="s_news-text color-grey-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        <a href="blog_detail.html" class="c-button small bg-dr-blue-2 hv-dr-blue-2-o"><span>view more</span></a>
                    </div>
                </div>
                <div class="blog-grid-entry col-mob-12 col-xs-12 col-sm-6 col-md-4">
                    <div class="s_news-entry style-2">
                        <a href="blog_detail.html"><img class="s_news-img img-responsive" src="img/inner/blog_grid_12.jpg" alt=""></a>
                        <h4 class="s_news-title"><a href="blog_detail.html">Lorem ipsum dolor</a></h4>
                        <div class="tour-info-line clearfix">
                            <div class="tour-info fl">
                                <img src="img/calendar_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">03/07/2015</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/people_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">By Emma Stone</span>
                            </div>
                            <div class="tour-info fl">
                                <img src="img/comment_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2">10 comments</span>
                            </div>
                        </div>
                        <div class="s_news-text color-grey-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        <a href="blog_detail.html" class="c-button small bg-dr-blue-2 hv-dr-blue-2-o"><span>view more</span></a>
                    </div>
                </div>
            </div>
            <div class="c_pagination clearfix">
                <a href="#" class="c-button b-40 bg-dr-blue-2 hv-dr-blue-2-o fl">prev page</a>
                <a href="#" class="c-button b-40 bg-dr-blue-2 hv-dr-blue-2-o fr">next page</a>
                <ul class="cp_content color-3">
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">...</a></li>
                    <li><a href="#">10</a></li>
                </ul>
            </div>
        </div>
    </div>
@stop