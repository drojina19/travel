
            <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="sidebar bg-white clearfix">
                    <div class="sidebar-block">
                        <h4 class="sidebar-title color-dark-2">search</h4>
                        <div class="search-inputs">
                            <div class="form-block clearfix">
                                <div class="input-style-1 b-50 color-3">
                                    <img src="{{asset('frontend/img/loc_icon_small_grey.png')}}" alt="">
                                    <input type="text" placeholder="Where do you want to go?">
                                </div>
                            </div>
                        </div>
                        <input type="submit" class="c-button b-40 bg-blue-2 hv-blue-2-o" value="search">
                    </div>
                    <div class="sidebar-block">
                        <h4 class="sidebar-title color-dark-2">categories</h4>
                        <ul class="sidebar-category color-1">
                            <li class="active">
                                <a class="cat-drop" href="#">Destination</a>
                                <ul>
                                    @foreach($destination as $dest)
                                    <li><a href="{{url('destination/'.$dest->name)}}">{{ $dest->name }}</a></li>
                                    @endforeach

                                </ul>
                            </li>
                            <li>
                                <a class="cat-drop" href="#">Tour</a>
                                <ul>
                                    @foreach($trs as $t)
                                        <li><a href="{{url('/themelist/'.Illuminate\Support\Str::slug($t->theme->name).'/'.'tourdetail')}}">{{ $t->name }}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <li>
                                <a class="cat-drop" href="#">Themes</a>
                                <ul>
                                    @foreach($themes as $thm)
                                        <li><a href="{{url('/themelist/'.$thm->name )}}">{{ $thm->name }}</a></li>
                                    @endforeach

                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
