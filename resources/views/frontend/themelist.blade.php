@extends('frontend.baselayout')
@section('main-section')
    <div class="container">
        <div class="inner-banner style-6">
            <img class="center-image" src="{{asset('frontend/img/detail/bg_3.jpg')}}" alt="">
            <div class="vertical-align">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-8 col-md-offset-2">
                            <ul class="banner-breadcrumb color-white clearfix">

                                <li><a class="link-blue-2" href="#">home </a> /</li>
                                <li><a class="link-blue-2" href="#">themes</a> /</li>
                                <li><span>{{$theme->name}}</span>
                                </li>
                            </ul>
                            <h2 class="color-white">{{$theme->name}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-wrapper bg-grey-">
            <div class="container">
                <div class="row">

                    @include('frontend.sidesearch')

                    <div class="col-xs-12 col-sm-8 col-md-9">
                        <div class="col-md-12">
                            <h3 class="subtitle color-dr-blue-2 underline">{{$theme->name}}</h3>
                        </div>
                        <hr>
                        <br>
                        <div class="col-md-12">
                            <img class="img-responsive img-full" src="{{url('images/theme/'.$theme->image)}}" alt="{{config('app.name')}}">
                        </div>
                        <br>
                        <br>
                        <div class="col-md-12">
                            <div class="s_news-text color-grey-3">
                                <p class="color-grey">{!! $theme->description !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="grid-content clearfix">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <div class="second-title">
                        <h4 class="subtitle color-dr-blue-2 underline">our proposed tours</h4>
                        <h2></h2>
                    </div>
                </div>
            </div>

            @foreach($tours as $tour)
                <div class="list-item-entry">

                    <div class="hotel-item style-9 bg-white">
                        <div class="table-view">

                            <div class="radius-top cell-view">
                                <img src="{{asset('images/tour/'.$tour->image)}}" alt="">
                                <div class="price price-s-4">$600</div>
                            </div>
                            <div class="title hotel-middle cell-view">
                                <div class="tour-info-line clearfix">
                                    <div class="tour-info fl">
                                        <img src="img/calendar_icon_grey.png" alt="">
                                        <span class="font-style-2 color-grey-3">July <strong>{{$tour->duration}} </strong> days</span>
                                    </div>
                                    <div class="tour-info fl">
                                        <img src="img/loc_icon_small_grey.png" alt="">
                                        <span class="font-style-2 color-grey-3">{{$tour->country->name}}</span>
                                    </div>
                                </div>
                                <h4><b>{{$tour->name}}</b></h4>
                                <div class="rate-wrap list-hidden">
                                    <div class="rate">
                                        <span class="fa fa-star color-yellow"></span>
                                        <span class="fa fa-star color-yellow"></span>
                                        <span class="fa fa-star color-yellow"></span>
                                        <span class="fa fa-star color-yellow"></span>
                                        <span class="fa fa-star color-yellow"></span>
                                    </div>
                                    <i>485 rewies</i>
                                </div>
                                <p class="f-14 color-grey-3">{!! str_limit($tour->description) !!}</p>
                                <div class="buttons-block bg-dr-blue-2">
                                    <a href="{{url('/themelist/'.Illuminate\Support\Str::slug($theme->name).'/'.'tourdetail')}}" class="c-button b-40 bg-grey-3-t hv-grey-3-t b-1">detail</a>
                                    <a href="#" class="c-button b-40 bg-white hv-transparent fr">book now</a>
                                </div>
                            </div>
                            <div class="title hotel-right clearfix cell-view grid-hidden">
                                <div class="rate-wrap">
                                    <i>485 rewies</i>
                                    <div class="rate">
                                        <span class="fa fa-star color-yellow"></span>
                                        <span class="fa fa-star color-yellow"></span>
                                        <span class="fa fa-star color-yellow"></span>
                                        <span class="fa fa-star color-yellow"></span>
                                        <span class="fa fa-star color-yellow"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@stop
