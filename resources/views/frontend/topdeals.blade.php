@extends('frontend.baselayout')
@section('main-section')

<div class="inner-banner style-6">
    <img class="center-image" src="{{asset('frontend/img/detail/bg_3.jpg')}}" alt="">
    <div class="vertical-align">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <ul class="banner-breadcrumb color-white clearfix">
                        <li><a class="link-blue-2" href="{{asset('/')}}">home</a> /</li>
                        <li><a class="link-blue-2" href="{{asset('/tours')}}">tours</a> /</li>
                        <li><span>Top Details</span></li>
                    </ul>
                    <h2 class="color-white">Top deals</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="main-wraper bg-grey-2 padd-90">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="second-title">
                    <h4 class="subtitle color-dr-blue-2 underline">our directions</h4>
                    <h2>we proposed to you</h2>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="container">
    <div class="grid-content clearfix">
        @foreach($tours as $tour)
        <div class="list-item-entry">
            <div class="hotel-item style-9 bg-white">
                <div class="table-view">
                    <div class="radius-top cell-view">
                        <img src="{{asset('images/tour/'.$tour->image)}}" alt="" class="img-responsive">
                        <div class="price price-s-4">${{ $tour->price }}</div>
                    </div>
                    <div class="title hotel-middle cell-view">
                        <div class="tour-info-line clearfix">
                            <div class="tour-info fl">
                                <img src="{{asset('frontend/img/calendar_icon_grey.png')}}" alt="">
                                <span class="font-style-2 color-grey-3"><strong>{{ $tour->duration }}</strong></span>
                            </div>
                            <div class="tour-info fl">
                                <img src="{{asset('frontend/img/loc_icon_small_grey.png')}}" alt="">
                                <span class="font-style-2 color-grey-3">{{ $tour->country->name }}</span>
                            </div>
                        </div>
                        <h4><b>{{ $tour->name }}</b></h4>

                        <p class="f-14 color-grey-3">{!! $tour->description !!}.</p>
                        <div class="buttons-block bg-dr-blue-2">
                            <a href="#" class="c-button b-40 bg-grey-3-t hv-grey-3-t b-1">details</a>
                            <a href="#" class="c-button b-40 bg-white hv-transparent fr">contacct</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        @endforeach

    </div>
    <div class="c_pagination clearfix padd-120">
        <a href="#" class="c-button b-40 bg-dr-blue-2 hv-dr-blue-2-o fl">prev page</a>
        <a href="#" class="c-button b-40 bg-dr-blue-2 hv-dr-blue-2-o fr">next page</a>
        <ul class="cp_content color-3">
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">...</a></li>
            <li><a href="#">10</a></li>
        </ul>
    </div>
</div>

@stop