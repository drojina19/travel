@extends('frontend.baselayout')
@section('main-section')


<div class="inner-banner style-6">
    <img class="center-image" src="{{asset('frontend/img/detail/bg_3.jpg')}}" alt="">
    <div class="vertical-align">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <ul class="banner-breadcrumb color-white clearfix">
                        <li><a class="link-blue-2" href="#">home</a> /</li>
                        <li><a class="link-blue-2" href="#">tours</a> /</li>
                        <li><span>detail</span>
                        </li>
                    </ul>
                    <h2 class="color-white">{{$tour->name}}</h2>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="detail-wrapper">
    <div class="container">
        <div class="detail-header">
            <div class="row">
                <div class="col-xs-12 col-sm-8">
                    <h2 class="detail-title color-dark-2">{{$tour->name}} </h2>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="detail-price color-dark-2">price from <span class="color-dr-blue"> ${{$tour->price}}</span> /person</div>
                </div>
            </div>
        </div>
        <div class="row padd-90" style="margin-top: -32px;">
            <div class="col-xs-12 col-md-8">
                <div class="detail-content">
                    <div class="detail-top">
                        <div class="embed-responsive embed-responsive-16by9">
                           <img src="{{asset('images/tour/'.$tour->image)}}" alt="{{config('app.name')}}" class="img-responsive" style="width: 100%">
                        </div>
                    </div>
                    <div class="detail-content-block">
                        <div class="simple-tab color-1 tab-wrapper">
                            <div class="tab-nav-wrapper">
                                <div class="nav-tab  clearfix">
                                    <div class="nav-tab-item active">Overview</div>
                                    <div class="nav-tab-item">Highlights</div>
                                    <div class="nav-tab-item">Inclusions</div>
                                    <div class="nav-tab-item">Important Notes</div>
                                    <div class="nav-tab-item">Equipment</div>
                                </div>
                            </div>
                            <div class="tabs-content clearfix">
                                <div class="tab-info active">
                                    <h3>General Information About tour</h3>
                                    <p>Pellentesque ac turpis egestas, varius justo et, condimentum augue. Praesent aliquam, nisl feugiat vehicula condimentum, justo tellus scelerisque metus. Pellentesque ac turpis egestas, varius justo et, condimentum augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
                                    <h4>Schedules</h4>

                                        @foreach($tour->schedules as $schedule)

                                            <h6>{{$schedule->title}}</h6>
                                            <p>{!! $schedule->description !!}</p>


                                        @endforeach

                                    <h4>Informations</h4>

                                        @foreach($tour->informations as $infos)


                                               <h6>{{$infos->title}}</h6>
                                                <p>{!! $infos->description !!}</p>

                                        @endforeach


                                </div>
                                <div class="tab-info text-justify">
                                    <h3>Highlights </h3>
                                    @foreach($tour->highlights as $highlight)
                                    <p>{{$highlight->description}}</p>
                                     @endforeach

                                </div>
                                <div class="tab-info text-justify">
                                    <h3>you need to know Inclusions for {{$tour->name}}</h3>
                                    @foreach($tour->inclusions as $inclusion)
                                    <p>{{$inclusion->description}}</p>
                                     @endforeach


                                </div>
                                <div class="tab-info text-justify">

                                        <img class="right-img  col-sm-offset-1 col-md-3 col-md-offset-0" src="{{asset('images/tour/'.$tour->image)}}" alt="{{config('app.name')}}">
                                        <h3>Important Facts About {{$tour->name}}</h3>
                                        @foreach($tour->additionalinfos as $additionalinfo)
                                            <p>{!! $additionalinfo->description !!}</p>
                                        @endforeach


                                </div>

                                <div class="tab-info">
                                    <h3>Equipments</h3>

                                    @foreach($tour->equipments as $equipment)
                                    <p>{{$equipment->title}}</p>
                                     @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="right-sidebar">

                    <div class="detail-block bg-dr-blue-2">
                        <h4 class="color-white">details</h4>
                        <div class="details-desc">

                            <p class="color-blue-5">price: <span class="color-white">${{$tour->price}}</span>
                            </p>
                            <p class="color-blue-5">tourstyle: <span class="color-white">{{$tour->tourstyle->name}}</span>
                            </p>
                            <p class="color-blue-5">duration: <span class="color-white">{{$tour->duration}} days</span>
                            </p>
                            <p class="color-blue-5">theme: <span class="color-white">{{$tour->theme->name}}</span>
                            </p>
                            <p class="color-blue-5">Country : <span class="color-white">{{$tour->country->name}}</span>
                            </p>
                        </div>
                        <div class="details-btn">
                            <a href="#" class="c-button b-40 bg-tr-1 hv-white"><span>view on map</span></a>
                            <a href="" class="c-button b-40 bg-white hv-transparent"><span>enquiry now</span></a>
                        </div>
                    </div>
                    <div class="popular-tours bg-grey-2">
                        <h4 class="color-dark-2"> our other popular tours</h4>
                        @foreach($tours->where('id','!=',$tour->id)->take(6) as $t)

                        <div class="hotel-small style-2 clearfix">
                            <a class="hotel-img black-hover" href="{{url('/themelist/'.Illuminate\Support\Str::slug($t->theme->name).'/tourdetail')}}">
                                <img class="img-responsive radius-0" src="{{asset('images/tour/'.$t->image)}}" alt="{{config('app.name')}}">
                                <div class="tour-layer delay-1"></div>
                            </a>
                            <div class="hotel-desc">

                                <h4>{{$t->name}}</h4>
                                <h5><span class="color-dark-2"><strong>${{$t->price}}</strong>/ person</span></h5>

                                <div class="hotel-loc tt"><strong>{{$t->duration}}</strong> days</div>
                            </div>
                        </div>
                         @endforeach

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>




 @stop