@extends('frontend.baselayout')
@section('main-section')



<div class="inner-banner style-6">
    <img class="center-image" src="{{asset('frontend/img/inner/bg_5.jpg')}}" alt="{{config('app.name')}}">
    <div class="vertical-align">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <ul class="banner-breadcrumb color-white clearfix">
                        <li><a class="link-blue-2" href="{{asset('/')}}">home</a> /</li>
                        <li><a class="link-blue-2" href="{{asset('/about')}}">about</a> /</li>
                        <li><span>travel styles</span></li>
                    </ul>
                    <h2 class="color-white">Travel Style</h2>
                </div>
            </div>
        </div>
    </div>
</div>

{{--<div class="main-wraper padd-90">--}}
    {{--<div class="container">--}}
        {{--<div class="row">--}}
            {{--<div class="col-xs-12 col-sm-8 col-sm-offset-2">--}}
                {{--<div class="second-title">--}}
                    {{--<h4 class="subtitle color-dr-blue-2 underline">our proposed</h4>--}}
                    {{--<h2>why choose us</h2>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row">--}}
            {{--<div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">--}}
                {{--<div class="simple-text">--}}
                    {{--<h3>What We Do?</h3>--}}
                    {{--<ul class="color-grey dot-blue-2">--}}
                        {{--<li>First Class Flights</li>--}}
                        {{--<li>5 Star Accommodations</li>--}}
                        {{--<li>Inclusive Packages</li>--}}
                        {{--<li>Latest Model Vehicles</li>--}}
                        {{--<li>Best Price Guarantee</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-mob-12 col-xs-12 col-sm-6 col-lg-4">--}}
                {{--<img class="img-responsive img-full radius-5" src="img/inner/about_2.jpg" alt="">--}}
            {{--</div>--}}
            {{--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">--}}
                {{--<div class="simple-text">--}}
                    {{--<h3>Who We Are?</h3>--}}
                    {{--<p class="color-grey">Pellentesque ac turpis egestas, varius justo et, condimentum augue. Praesent aliquam, nisl feugiat vehicula condimentum, justo tellus scelerisque metus. Pellentesque ac turpis egestas, varius justo et, condimentum augue.</p>--}}
                    {{--<h4>interesting for you</h4>--}}
                    {{--<p class="color-grey">Pellentesque ac turpis egestas, varius justo et, condimentum augue. Praesent aliquam, nisl feugiat vehicula condimentum, justo tellus scelerisque metus. Pellentesque varius justo et, condimentum augue.</p>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

<div class="main-wraper bg-grey-2 padd-90">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="second-title">
                    {{--<h4 class="subtitle color-dr-blue-2 underline"></h4>--}}
                    <h2>we proposed to you</h2>
                </div>
            </div>
        </div>
        <div class="tour-item-grid row">
            @foreach($tourstyles as $tourstyle)
            <div class="col-mob-12 col-xs-6 col-sm-6 col-md-4">
                <div class="tour-item style-5">
                    <div class="radius-top">
                        <img src="{{asset('images/tourstyle/'.$tourstyle->image)}}" alt="{{config('app.name')}}">
                    </div>
                    <div class="tour-desc bg-white">
                        <h4><a class="tour-title color-dark-2 link-dr-blue-2" href="#">{{$tourstyle->name}}</a></h4>
                        <div class="tour-text color-grey-3">{!! $tourstyle->description !!}</div>
                        <a href="#" class="c-button b-40 bg-dr-blue-2 hv-dr-blue-2-o"><span>view more</span></a>
                    </div>
                </div>
            </div>
             @endforeach

        </div>
    </div>
</div>

<div class="main-wraper bg-dr-blue-2 color-2 padd-90">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="second-title style-2">
                    <h4 class="color-white underline">our directions</h4>
                    <h2 class="color-white">why we are the best</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-mob-12 col-xs-6 col-sm-4">
                <div class="icon-block">
                    <img class="icon-img" src="img/inner/c_icon_1.png" alt="">
                    <h5 class="icon-title color-white">happy clients</h5>
                    <div class="icon-text color-white-light">Sed sit amet leo orci. Fusce tincidunt accumsan pretium. Donec fermentum, ex non placerat feugiat.</div>
                    <a href="#" class="c-button b-40 bg-white color-dark-2 hv-white-o"><span>view more</span></a>
                </div>
            </div>
            <div class="col-mob-12 col-xs-6 col-sm-4">
                <div class="icon-block">
                    <img class="icon-img" src="img/inner/c_icon_2.png" alt="">
                    <h5 class="icon-title color-white">amazing tour</h5>
                    <div class="icon-text color-white-light">Sed sit amet leo orci. Fusce tincidunt accumsan pretium. Donec fermentum, ex non placerat feugiat.</div>
                    <a href="#" class="c-button b-40 bg-white color-dark-2 hv-white-o"><span>view more</span></a>
                </div>
            </div>
            <div class="col-mob-12 col-xs-6 col-sm-4">
                <div class="icon-block">
                    <img class="icon-img" src="img/inner/c_icon_3.png" alt="">
                    <h5 class="icon-title color-white">support cases</h5>
                    <div class="icon-text color-white-light">Sed sit amet leo orci. Fusce tincidunt accumsan pretium. Donec fermentum, ex non placerat feugiat.</div>
                    <a href="#" class="c-button b-40 bg-white color-dark-2 hv-white-o"><span>view more</span></a>
                </div>
            </div>

        </div>
    </div>
</div>
{{--<div class="main-wraper bg-grey-2 padd-90">--}}
    {{--<div class="container">--}}
        {{--<div class="row">--}}
            {{--<div class="col-xs-12 col-sm-8 col-sm-offset-2">--}}
                {{--<div class="second-title">--}}
                    {{--<h4 class="subtitle color-dr-blue-2 underline">pricing</h4>--}}
                    {{--<h2>olso include</h2>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row">--}}
            {{--<div class="col-xs-12 col-sm-4">--}}
                {{--<div class="tariff">--}}
                    {{--<div class="tariff-header bg-dr-blue-2">--}}
                        {{--<div class="tariff-title color-white">premium</div>--}}
                        {{--<div class="tariff-trial color-white-light">30 days free trial</div>--}}
                    {{--</div>--}}
                    {{--<img class="tafiff-img img-responsive img-full" src="img/inner/tariff_1.jpg" alt="">--}}
                    {{--<div class="tariff-content bg-white">--}}
                        {{--<div class="tariff-price color-dark-2"><span>$20</span>/mon</div>--}}
                        {{--<div class="tariff-line color-grey">15 projects</div>--}}
                        {{--<div class="tariff-line color-grey">Unlimited Users</div>--}}
                        {{--<div class="tariff-line color-grey">No Time Tracking</div>--}}
                        {{--<div class="tariff-line color-grey">Enhanced Security</div>--}}
                        {{--<div class="tariff-line color-grey">5 GB Storage</div>--}}
                        {{--<a href="#" class="c-button b-50 bg-dr-blue-2 hv-dr-blue-2-o"><span>view more</span></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-xs-12 col-sm-4">--}}
                {{--<div class="tariff">--}}
                    {{--<div class="tariff-header bg-dr-blue-2">--}}
                        {{--<div class="tariff-title color-white">premium</div>--}}
                        {{--<div class="tariff-trial color-white-light">30 days free trial</div>--}}
                    {{--</div>--}}
                    {{--<img class="tafiff-img img-responsive img-full" src="img/inner/tariff_2.jpg" alt="">--}}
                    {{--<div class="tariff-content bg-white">--}}
                        {{--<div class="tariff-price color-dark-2"><span>$20</span>/mon</div>--}}
                        {{--<div class="tariff-line color-grey">15 projects</div>--}}
                        {{--<div class="tariff-line color-grey">Unlimited Users</div>--}}
                        {{--<div class="tariff-line color-grey">No Time Tracking</div>--}}
                        {{--<div class="tariff-line color-grey">Enhanced Security</div>--}}
                        {{--<div class="tariff-line color-grey">5 GB Storage</div>--}}
                        {{--<a href="#" class="c-button b-50 bg-dr-blue-2 hv-dr-blue-2-o"><span>view more</span></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-xs-12 col-sm-4">--}}
                {{--<div class="tariff">--}}
                    {{--<div class="tariff-header bg-dr-blue-2">--}}
                        {{--<div class="tariff-title color-white">premium</div>--}}
                        {{--<div class="tariff-trial color-white-light">30 days free trial</div>--}}
                    {{--</div>--}}
                    {{--<img class="tafiff-img img-responsive img-full" src="img/inner/tariff_3.jpg" alt="">--}}
                    {{--<div class="tariff-content bg-white">--}}
                        {{--<div class="tariff-price color-dark-2"><span>$20</span>/mon</div>--}}
                        {{--<div class="tariff-line color-grey">15 projects</div>--}}
                        {{--<div class="tariff-line color-grey">Unlimited Users</div>--}}
                        {{--<div class="tariff-line color-grey">No Time Tracking</div>--}}
                        {{--<div class="tariff-line color-grey">Enhanced Security</div>--}}
                        {{--<div class="tariff-line color-grey">5 GB Storage</div>--}}
                        {{--<a href="#" class="c-button b-50 bg-dr-blue-2 hv-dr-blue-2-o"><span>view more</span></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

<div class="main-wraper padd-120">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="second-title">
                    <h2>what our client say about us</h2>
                    <p class="color-grey">Curabitur nunc erat, consequat in erat ut, congue bibendum nulla.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="top-baner arrows">
                <div class="swiper-container offers-slider" data-autoplay="5000" data-loop="1" data-speed="500" data-slides-per-view="responsive" data-mob-slides="1" data-xs-slides="2" data-sm-slides="2" data-md-slides="3" data-lg-slides="4" data-add-slides="4">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide" data-val="0">
                            <div class="offers-block style-2 radius-mask underline-block hover-blue">
                                <div class="clip">
                                    <div class="bg bg-bg-chrome act" style="background-image:url(img/home_4/offers-block_1.jpg)">
                                    </div>
                                </div>
                                <div class="tour-layer delay-1"></div>
                                <div class="vertical-top">
                                    <div class="rate">
                                        <span class="fa fa-heart color-dr-blue-2 hover-it"></span>
                                        <span class="fa fa-heart color-dr-blue-2 hover-it"></span>
                                        <span class="fa fa-heart color-dr-blue-2 hover-it"></span>
                                        <span class="fa fa-heart color-dr-blue-2 hover-it"></span>
                                        <span class="fa fa-heart color-dr-blue-2 hover-it"></span>
                                    </div>
                                    <h3 class="hover-it">sea tour</h3>
                                </div>
                                <div class="vertical-bottom">
                                    <div class="tour-info">
                                        <img src="img/calendar_icon.png" alt="">
                                        <span class="font-style-2">03/07/2015</span>
                                    </div>
                                    <h4 class="offet-title underline hover-it m94">inna, david lunoe</h4>
                                    <p>Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam.</p>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide" data-val="1">
                            <div class="offers-block style-2 radius-mask underline-block hover-blue">
                                <div class="clip">
                                    <div class="bg bg-bg-chrome act" style="background-image:url(img/home_4/offers-block_2.jpg)">
                                    </div>
                                </div>
                                <div class="tour-layer delay-1"></div>
                                <div class="vertical-top">
                                    <div class="rate">
                                        <span class="fa fa-heart color-dr-blue-2 hover-it"></span>
                                        <span class="fa fa-heart color-dr-blue-2 hover-it"></span>
                                        <span class="fa fa-heart color-dr-blue-2 hover-it"></span>
                                        <span class="fa fa-heart color-dr-blue-2 hover-it"></span>
                                        <span class="fa fa-heart color-dr-blue-2 hover-it"></span>
                                    </div>
                                    <h3 class="hover-it">cruise tour</h3>
                                </div>
                                <div class="vertical-bottom">
                                    <div class="tour-info">
                                        <img src="img/calendar_icon.png" alt="">
                                        <span class="font-style-2">03/07/2015</span>
                                    </div>
                                    <h4 class="offet-title underline hover-it m94">anna, mark</h4>
                                    <p>Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam.</p>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide" data-val="2">
                            <div class="offers-block style-2 radius-mask underline-block hover-blue">
                                <div class="clip">
                                    <div class="bg bg-bg-chrome act" style="background-image:url(img/home_4/offers-block_3.jpg)">
                                    </div>
                                </div>
                                <div class="tour-layer delay-1"></div>
                                <div class="vertical-top">
                                    <div class="rate">
                                        <span class="fa fa-heart color-dr-blue-2 hover-it"></span>
                                        <span class="fa fa-heart color-dr-blue-2 hover-it"></span>
                                        <span class="fa fa-heart color-dr-blue-2 hover-it"></span>
                                        <span class="fa fa-heart color-dr-blue-2 hover-it"></span>
                                        <span class="fa fa-heart color-dr-blue-2 hover-it"></span>
                                    </div>
                                    <h3 class="hover-it">food tour</h3>
                                </div>
                                <div class="vertical-bottom">
                                    <div class="tour-info">
                                        <img src="img/calendar_icon.png" alt="">
                                        <span class="font-style-2">03/07/2015</span>
                                    </div>
                                    <h4 class="offet-title underline hover-it m94">elison lloyds</h4>
                                    <p>Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam.</p>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide" data-val="4">
                            <div class="offers-block style-2 radius-mask underline-block hover-blue">
                                <div class="clip">
                                    <div class="bg bg-bg-chrome act" style="background-image:url(img/home_4/offers-block_4.jpg)">
                                    </div>
                                </div>
                                <div class="tour-layer delay-1"></div>
                                <div class="vertical-top">
                                    <div class="rate">
                                        <span class="fa fa-heart color-dr-blue-2 hover-ite"></span>
                                        <span class="fa fa-heart color-dr-blue-2 hover-it"></span>
                                        <span class="fa fa-heart color-dr-blue-2 hover-it"></span>
                                        <span class="fa fa-heart color-dr-blue-2 hover-it"></span>
                                        <span class="fa fa-heart color-dr-blue-2 hover-it"></span>
                                    </div>
                                    <h3 class="hover-it">sea tour</h3>
                                </div>
                                <div class="vertical-bottom">
                                    <div class="tour-info">
                                        <img src="img/calendar_icon.png" alt="">
                                        <span class="font-style-2">03/07/2015</span>
                                    </div>
                                    <h4 class="offet-title underline hover-it m94">elison lloyds</h4>
                                    <p>Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pagination  poin-style-1 pagination-hidden"></div>
                </div>
                <div class="swiper-arrow-left offers-arrow color-2"><span class="fa fa-angle-left"></span></div>
                <div class="swiper-arrow-right offers-arrow color-2"><span class="fa fa-angle-right"></span></div>
            </div>
        </div>
    </div>
</div>



@stop