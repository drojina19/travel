<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/admin', function () {
    return view('admin.dashboard');
})->middleware('auth');


Route::group(['middleware'=>'auth'],function(){

    Route::group(['prefix'=>'admin'],function(){
        Route::resource('/slider','Admin\SliderController');
        Route::resource('/testemonial','Admin\TestemonialController');
        Route::resource('/country','Admin\CountryController');
        Route::resource('/component','Admin\ComponentController');
        Route::resource('/theme','Admin\ThemeController');
        Route::resource('/tour','Admin\TourController');
        Route::resource('/gallery','Admin\GalleryController');
        Route::resource('/article','Admin\ArticleController');
        Route::resource('/faqs','Admin\FaqController');
        Route::resource('/tourstyle','Admin\TourStyleController');
        Route::resource('/blog','Admin\BlogController');
        Route::resource('traveldeal','Admin\TravelDealController');
        Route::resource('/landmark','Admin\LandmarkController');
        Route::get('c/landmark/add/{countryId}','Admin\LandmarkController@addLandmark');
        Route::get('c/landmark/{countryId}','Admin\LandmarkController@showLandmarks');
    });
});



Route::group(['prefix' => 'api'], function() {
    
    Route::resource('/tour','Admin\TourController',array('only'=>'show'));
    Route::resource('/highlight','Api\HighlightController');
    Route::resource('/schedule','Api\ScheduleController');
    Route::resource('/inclusion','Api\InclusionController');
    Route::resource('/information','Api\InformationController');
    Route::resource('/additionalinfo','Api\AdditionalInfoController');
    Route::resource('/price','Api\PriceController');
    Route::resource('/equipment','Api\EquipmentController');

});




Route::get('/','Frontend\FrontendController@index');
Route::get('/topdeals','Frontend\FrontendController@topdeals');
Route::get('/themelist/{theme}','Frontend\FrontendController@themelist');

Route::get('/themelist/{name}/detail','Frontend\FrontendController@themeDetails');
Route::get('/about','Frontend\FrontendController@about');
Route::get('/about/faq','Frontend\FrontendController@faqs');
Route::get('/services','Frontend\FrontendController@services');
Route::get('/about/contact','Frontend\FrontendController@contact');
Route::get('/about/gallery','Frontend\FrontendController@gallery');
Route::get('/destination/{country}','Frontend\FrontendController@destination');

Route::get('/article/{type}','Frontend\FrontendController@article');
Route::get('/about/travelstyle/{travel}','Frontend\FrontendController@travelStyle');
Route::get('/themelist/{theme}/tourdetail','Frontend\FrontendController@tourdetail');


/**Searching tour**/

Route::get('/advance-search','Frontend\FrontendController@searchTour');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
